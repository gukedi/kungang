<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <source>KunGang is a non-linear video editor. This software is free and protected by the GNU GPL. Its development based on Olive(https://www.olivevideoeditor.org)</source>
        <translation type="vanished">昆冈是免费的视频编辑软件，基于GNU通用公共许可证（GNU GPL）条款发布。基于Olive(https://www.olivevideoeditor.org)</translation>
    </message>
    <message>
        <location filename="../dialogs/aboutdialog.cpp" line="33"/>
        <source>About KunGang</source>
        <translation>关于昆冈剪辑</translation>
    </message>
    <message>
        <location filename="../dialogs/aboutdialog.cpp" line="46"/>
        <source>KunGang is a simple video editor. This software is free and protected by the GNU GPL. Its was originally developed in Feb 2020, based on Olive(https://www.olivevideoeditor.org)</source>
        <translation>昆冈剪辑是开源的视频编辑软件，基于GNU通用公共许可证（GNU GPL）条款发布。2020年2月基于Olive(https://www.olivevideoeditor.org)开发</translation>
    </message>
    <message>
        <location filename="../dialogs/aboutdialog.cpp" line="48"/>
        <source>KunGang Team is obliged to inform users that KunGang source code is available for download from its website.</source>
        <translation>昆冈剪辑团队有义务告知用户可以从官网下载源代码。</translation>
    </message>
</context>
<context>
    <name>ActionSearch</name>
    <message>
        <location filename="../dialogs/actionsearch.cpp" line="57"/>
        <source>Search for action...</source>
        <translation>功能搜索...</translation>
    </message>
</context>
<context>
    <name>AdvancedVideoDialog</name>
    <message>
        <location filename="../dialogs/advancedvideodialog.cpp" line="41"/>
        <source>Advanced Video Settings</source>
        <translation>高级视频设置</translation>
    </message>
    <message>
        <location filename="../dialogs/advancedvideodialog.cpp" line="53"/>
        <source>Pixel Format:</source>
        <translation>视频格式:</translation>
    </message>
    <message>
        <location filename="../dialogs/advancedvideodialog.cpp" line="77"/>
        <source>Threads:</source>
        <translation>线程数量:</translation>
    </message>
</context>
<context>
    <name>Audio</name>
    <message>
        <source>%1 Audio</source>
        <translatorcomment>音频渲染</translatorcomment>
        <translation type="vanished">%1 音频</translation>
    </message>
    <message>
        <location filename="../rendering/audio.cpp" line="347"/>
        <source>Recording %1</source>
        <translation>录音 %1</translation>
    </message>
</context>
<context>
    <name>AudioNoiseEffect</name>
    <message>
        <location filename="../effects/internal/audionoiseeffect.cpp" line="24"/>
        <source>Amount</source>
        <translation>质量</translation>
    </message>
    <message>
        <location filename="../effects/internal/audionoiseeffect.cpp" line="30"/>
        <source>Mix</source>
        <translation>混合</translation>
    </message>
</context>
<context>
    <name>AutoCutSilenceDialog</name>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="38"/>
        <source>Cut Silence</source>
        <translation>静噪分离</translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="44"/>
        <source>Attack Threshold:</source>
        <translation>触发阀值:</translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="49"/>
        <source>Attack Time:</source>
        <translation>触发时间:</translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="54"/>
        <source>Release Threshold:</source>
        <translation>释放阀值:</translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="59"/>
        <source>Release Time:</source>
        <translation>释放时间:</translation>
    </message>
</context>
<context>
    <name>Cacher</name>
    <message>
        <location filename="../rendering/cacher.cpp" line="922"/>
        <location filename="../rendering/cacher.cpp" line="931"/>
        <source>Could not open %1 - %2</source>
        <translation>无法打开 %1 - %2</translation>
    </message>
</context>
<context>
    <name>ChannelLayoutName</name>
    <message>
        <location filename="../project/media.cpp" line="53"/>
        <source>Invalid</source>
        <translatorcomment>媒体文件损坏或者无效</translatorcomment>
        <translation>媒体无效</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="54"/>
        <source>Mono</source>
        <translation>单声道</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="55"/>
        <source>Stereo</source>
        <translation>立体声</translation>
    </message>
</context>
<context>
    <name>ClipPropertiesDialog</name>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="14"/>
        <source>&quot;%1&quot; Properties</source>
        <translation>处理中 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="15"/>
        <source>Multiple Clip Properties</source>
        <translation>多个片段属性</translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="24"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="33"/>
        <source>Duration:</source>
        <translation>片段长度:</translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="71"/>
        <source>(multiple)</source>
        <translatorcomment>多个特效</translatorcomment>
        <translation>(多个)</translation>
    </message>
</context>
<context>
    <name>CollapsibleWidget</name>
    <message>
        <location filename="../ui/collapsiblewidget.cpp" line="56"/>
        <source>Save current effect</source>
        <translation>保存当前特效</translation>
    </message>
    <message>
        <location filename="../ui/collapsiblewidget.cpp" line="59"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../ui/collapsiblewidget.cpp" line="62"/>
        <source>&lt;untitled&gt;</source>
        <translation>&lt;无标题&gt;</translation>
    </message>
</context>
<context>
    <name>ColorButton</name>
    <message>
        <location filename="../ui/colorbutton.cpp" line="47"/>
        <source>Set Color</source>
        <translation>选择颜色</translation>
    </message>
</context>
<context>
    <name>CornerPinEffect</name>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="30"/>
        <source>Top Left</source>
        <translation>左上角</translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="34"/>
        <source>Top Right</source>
        <translation>右上角</translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="38"/>
        <source>Bottom Left</source>
        <translation>左下角</translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="42"/>
        <source>Bottom Right</source>
        <translation>右下角</translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="46"/>
        <source>Perspective</source>
        <translation>透视图</translation>
    </message>
</context>
<context>
    <name>DebugDialog</name>
    <message>
        <location filename="../dialogs/debugdialog.cpp" line="44"/>
        <source>Debug Log</source>
        <translation>调试日志</translation>
    </message>
</context>
<context>
    <name>DemoNotice</name>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="30"/>
        <location filename="../dialogs/demonotice.cpp" line="45"/>
        <source>Welcome to Olive!</source>
        <translation>欢迎来到Olive！</translation>
    </message>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="47"/>
        <source>Olive is a free open-source video editor released under the GNU GPL. If you have paid for this software, you have been scammed.</source>
        <translation>Olive是一个自由开源的视频编辑器.基于GNU通用公共许可证（GNU GPL）条款发布.如果你购买了这个软件,你就被蒙骗了.</translation>
    </message>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="49"/>
        <source>This software is currently in ALPHA which means it is unstable and very likely to crash, have bugs, and have missing features. We offer no warranty so use at your own risk. Please report any bugs or feature requests at %1</source>
        <translation>这个软件目前处于ALPHA版本的开发阶段,意味着功能尚未稳定并且有漏洞以至于崩溃,功能并不完善.我们不会承担任何责任,所有风险皆自行承担.若发现不足的地方请向此处报告: %1</translation>
    </message>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="51"/>
        <source>Thank you for trying Olive and we hope you enjoy it!</source>
        <translation>谢谢您选择Olive,尽情享受吧！</translation>
    </message>
</context>
<context>
    <name>Effect</name>
    <message>
        <location filename="../effects/effect.cpp" line="102"/>
        <source>Invalid effect</source>
        <translation>无效的特效</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="103"/>
        <source>No candidate for effect &apos;%1&apos;. This effect may be corrupt. Try reinstalling it or Olive.</source>
        <translation>特效无法使用 &apos;%1&apos;. Ц此特效可能已经损坏. 请尝试重新安装Olive.</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="575"/>
        <source>Save effect</source>
        <translation>保存特效</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="576"/>
        <source>Enter effect name:</source>
        <translation>输入名称</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="596"/>
        <source>Save Effect Settings</source>
        <translation>保存特效设置</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="598"/>
        <location filename="../effects/effect.cpp" line="608"/>
        <source>Effect XML Settings %1</source>
        <translation>特效设定中 %1</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="471"/>
        <source>Save Settings Failed</source>
        <translation>保存设定失败</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="472"/>
        <source>Failed to open &quot;%1&quot; for writing.</source>
        <translation>无法打开 &quot;%1&quot; 用于写入.</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="606"/>
        <source>Load Effect Settings</source>
        <translation>加载特效设置</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="562"/>
        <location filename="../effects/effect.cpp" line="622"/>
        <location filename="../effects/effect.cpp" line="814"/>
        <source>Load Settings Failed</source>
        <translation>加载设置失败</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="563"/>
        <location filename="../effects/effect.cpp" line="623"/>
        <source>Failed to open &quot;%1&quot; for reading.</source>
        <translation>无法打开 &quot;%1&quot; 用于读取.</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="815"/>
        <source>This settings file doesn&apos;t match this effect.</source>
        <translation>该设置不匹配于此特效</translation>
    </message>
</context>
<context>
    <name>EffectControls</name>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="335"/>
        <source>(none)</source>
        <translation>(无)</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="522"/>
        <source>Effects: </source>
        <translation>特效: </translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="524"/>
        <source>Add Video Effect</source>
        <translation>添加视频效果</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="525"/>
        <source>VIDEO EFFECTS</source>
        <translation>视频特效</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="526"/>
        <source>Add Video Transition</source>
        <translation>添加视频转场效果</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="527"/>
        <source>Add Audio Effect</source>
        <translation>添加音频效果</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="528"/>
        <source>AUDIO EFFECTS</source>
        <translation>音频效果</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="529"/>
        <source>Add Audio Transition</source>
        <translation>添加音频转场效果</translation>
    </message>
</context>
<context>
    <name>EffectMgrDialog</name>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="50"/>
        <location filename="../dialogs/effectmgr.cpp" line="170"/>
        <source>Effect Manager</source>
        <translation>特效管理器</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="59"/>
        <source>input code to download</source>
        <translation>输入特效码下载</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="61"/>
        <location filename="../dialogs/effectmgr.cpp" line="75"/>
        <source>Get Effect</source>
        <translation>下载特效</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="77"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="93"/>
        <source>downloading...</source>
        <translation>下载中...</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="109"/>
        <location filename="../dialogs/effectmgr.cpp" line="118"/>
        <source>Network error</source>
        <translation>网络错误</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="130"/>
        <source>No network</source>
        <translation>无网络</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="138"/>
        <location filename="../dialogs/effectmgr.cpp" line="149"/>
        <source>Write file error</source>
        <translation>写文件错误</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="154"/>
        <source>extracting...</source>
        <translation>提取中...</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="161"/>
        <source>well done</source>
        <translation>下载完成</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="170"/>
        <source>Effect has been downloaded successfully</source>
        <translation>特效成功下载</translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="174"/>
        <source>invalid zip file</source>
        <translation>提取失败</translation>
    </message>
</context>
<context>
    <name>EffectRow</name>
    <message>
        <location filename="../effects/effectrow.cpp" line="107"/>
        <source>Disable Keyframes</source>
        <translation>禁用关键帧/动画补间</translation>
    </message>
    <message>
        <location filename="../effects/effectrow.cpp" line="108"/>
        <source>Disabling keyframes will delete all current keyframes. Are you sure you want to do this?</source>
        <translation>所有关键帧/动画补间将会被删除. 确认这么做？</translation>
    </message>
</context>
<context>
    <name>EffectUI</name>
    <message>
        <location filename="../ui/effectui.cpp" line="55"/>
        <source>%1 (Opening)</source>
        <translatorcomment>打开特效</translatorcomment>
        <translation>%1 (正在打开)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="57"/>
        <source>%1 (Closing)</source>
        <translatorcomment>关闭特效</translatorcomment>
        <translation>%1 (正在关闭)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="160"/>
        <source>%1 (multiple)</source>
        <translatorcomment>多个特效</translatorcomment>
        <translation>%1 (多个)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="386"/>
        <source>Cu&amp;t</source>
        <translation>剪切(&amp;T)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="389"/>
        <source>&amp;Copy</source>
        <translation>复制(&amp;C)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="400"/>
        <source>Move &amp;Up</source>
        <translation>向上移动(&amp;U)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="404"/>
        <source>Move &amp;Down</source>
        <translation>向下移动(&amp;D)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="409"/>
        <source>D&amp;elete</source>
        <translation>删除(&amp;E)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="426"/>
        <source>Load Settings From File</source>
        <translation>从文件加载设置</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="428"/>
        <source>Save Settings to File</source>
        <translation>保存设置到文件</translation>
    </message>
</context>
<context>
    <name>EmbeddedFileChooser</name>
    <message>
        <location filename="../ui/embeddedfilechooser.cpp" line="52"/>
        <source>File:</source>
        <translation>文件:</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="75"/>
        <source>Export &quot;%1&quot;</source>
        <translation>导出 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="128"/>
        <source>Unknown codec name %1</source>
        <translation>未知的编解码器 %1</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="342"/>
        <source>Export Failed</source>
        <translation>导出失败</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="343"/>
        <source>Export failed - %1</source>
        <translation>导出失败 - %1</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="373"/>
        <source>Export Successfully</source>
        <translation>导出成功</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="374"/>
        <source>Would you like to open containing folder?</source>
        <translation>要打开视频所在文件夹吗？</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="393"/>
        <source>Invalid dimensions</source>
        <translation>无效的大小</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="394"/>
        <source>Export width and height must both be even numbers/divisible by 2.</source>
        <translation>导出宽度和高度必须都是偶数/能被2整除.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="450"/>
        <source>Invalid codec</source>
        <translation>无效的编解码器</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="451"/>
        <source>Couldn&apos;t determine output parameters for the selected codec. This is a bug, please contact the developers.</source>
        <translation>无法确定所选编解码器的输出参数.这是一个bug,请联系开发人员.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="520"/>
        <source>Invalid format</source>
        <translation>无效的格式</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="521"/>
        <source>Couldn&apos;t determine output format. This is a bug, please contact the developers.</source>
        <translation>无法确定输出格式.这是一个bug,请联系开发人员.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="528"/>
        <source>Export Media</source>
        <translation>输出媒体</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="615"/>
        <source>%p% (Total: %1:%2:%3)</source>
        <translatorcomment>总量</translatorcomment>
        <translation>%p% (总计: %1:%2:%3)</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="620"/>
        <source>%p% (ETA: %1:%2:%3)</source>
        <translation>%p% (估计所需时间: %1:%2:%3)</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="635"/>
        <source>Quality-based (Constant Rate Factor)</source>
        <translatorcomment>速率</translatorcomment>
        <translation>质量(恒定速率因子)</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="639"/>
        <source>Constant Bitrate</source>
        <translation>恒定比特率</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="648"/>
        <location filename="../dialogs/exportdialog.cpp" line="654"/>
        <source>Invalid Codec</source>
        <translation>无效的编解码器</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="649"/>
        <source>Failed to find a suitable encoder for this codec. Export will likely fail.</source>
        <translation>无法为此格式匹配编解码器.输出有可能会失败.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="655"/>
        <source>Failed to find pixel format for this encoder. Export will likely fail.</source>
        <translation>未能找到此编码器的像素格式.输出有可能会失败.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="668"/>
        <source>Bitrate (Mbps):</source>
        <translation>比特率 (Mbp/s):</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="672"/>
        <source>Quality (CRF):</source>
        <translation>质量 (CRF):</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="675"/>
        <source>Quality Factor:

0 = lossless
17-18 = visually lossless (compressed, but unnoticeable)
23 = high quality
51 = lowest quality possible</source>
        <translation>质量因素:

0 = 无损耗
17-18 = 无法察觉的损耗 (压缩,但不明显)
23 = 高品质
51 = 最低品质</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="678"/>
        <source>Target File Size (MB):</source>
        <translation>输出文件大小 (MB):</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="714"/>
        <source>Format:</source>
        <translation>格式:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="723"/>
        <source>Range:</source>
        <translation>范围:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="726"/>
        <source>Entire Sequence</source>
        <translation>整个片段</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="727"/>
        <source>In to Out</source>
        <translation>已选择的时间段</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="734"/>
        <source>Video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="740"/>
        <location filename="../dialogs/exportdialog.cpp" line="783"/>
        <source>Codec:</source>
        <translation>编解码器:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="744"/>
        <source>Width:</source>
        <translation>宽度:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="749"/>
        <source>Height:</source>
        <translation>高度:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="754"/>
        <source>Frame Rate:</source>
        <translation>帧率:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="760"/>
        <source>Compression Type:</source>
        <translation>压缩类型:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="771"/>
        <source>Advanced</source>
        <translation>高级</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="778"/>
        <source>Audio</source>
        <translation>音频</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="787"/>
        <source>Sampling Rate:</source>
        <translation>采样率:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="793"/>
        <source>Bitrate (Kbps/CBR):</source>
        <translation>比特率 ((Kbps/CBR):</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="819"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="825"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>ExportThread</name>
    <message>
        <location filename="../rendering/exportthread.cpp" line="79"/>
        <source>failed to send frame to encoder (%1)</source>
        <translation>发送帧到编码器失败 (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="90"/>
        <source>failed to receive packet from encoder (%1)</source>
        <translation>无法从编码器接收数据包 (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="113"/>
        <source>could not video encoder for %1</source>
        <translation>无视频编解码器 %1</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="122"/>
        <source>could not allocate video stream</source>
        <translation>无法分配视频流</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="131"/>
        <source>could not allocate video encoding context</source>
        <translation>无法分配视频编码上下文</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="180"/>
        <source>could not open output video encoder (%1)</source>
        <translation>无法打开输出视频编码器 (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="188"/>
        <source>could not copy video encoder parameters to output stream (%1)</source>
        <translation>无法将视频编码器参数复制到输出流 (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="227"/>
        <source>could not audio encoder for %1</source>
        <translation>не вдалося знайти кодувальник аудіо для %1</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="235"/>
        <source>could not allocate audio stream</source>
        <translation>无法分配音频流</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="249"/>
        <source>could not allocate audio encoding context</source>
        <translation>无法分配音频编码上下文</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="274"/>
        <source>could not open output audio encoder (%1)</source>
        <translation>无法打开输出音频编码器 (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="282"/>
        <source>could not copy audio encoder parameters to output stream (%1)</source>
        <translation>无法将音频编码器参数复制到输出流 (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="319"/>
        <source>could not allocate audio buffer (%1)</source>
        <translation>无法分配音频缓冲区 (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="350"/>
        <source>could not create output format context</source>
        <translation>无法分配音频缓冲区</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="360"/>
        <source>could not open output file (%1)</source>
        <translation>无法打开输出文件 (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="396"/>
        <source>could not write output file header (%1)</source>
        <translation>无法写入输出文件标题 (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="600"/>
        <source>could not write output file trailer (%1)</source>
        <translatorcomment>无法写入输出文件</translatorcomment>
        <translation>无法写入输出文件预告片 (%1)</translation>
    </message>
</context>
<context>
    <name>FillLeftRightEffect</name>
    <message>
        <location filename="../effects/internal/fillleftrighteffect.cpp" line="27"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../effects/internal/fillleftrighteffect.cpp" line="29"/>
        <source>Fill Left with Right</source>
        <translation>从右到左</translation>
    </message>
    <message>
        <location filename="../effects/internal/fillleftrighteffect.cpp" line="30"/>
        <source>Fill Right with Left</source>
        <translation>从左到右</translation>
    </message>
</context>
<context>
    <name>Frei0rEffect</name>
    <message>
        <location filename="../effects/internal/frei0reffect.cpp" line="55"/>
        <source>Failed to load Frei0r plugin &quot;%1&quot;: %2</source>
        <translation>无法加载 плагін 插件 &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <source>NOTE: You can&apos;t load 32-bit Frei0r plugins into a 64-bit build of Olive. Please find a 64-bit version of this plugin or switch to a 32-bit build of Olive.</source>
        <translation type="vanished">警告:您不能将32位的Frei0r插件加载到64位的Olive构建中.请找到这个插件的64位版本或切换到32位的Olive构建版本.</translation>
    </message>
    <message>
        <source>NOTE: You can&apos;t load 64-bit Frei0r plugins into a 32-bit build of Olive. Please find a 32-bit version of this plugin or switch to a 64-bit build of Olive.</source>
        <translation type="vanished">警告:您不能将64位的Frei0r插件加载到32位的Olive构建中.请找到这个插件的32位版本或切换到64位构建的Olive.</translation>
    </message>
    <message>
        <location filename="../effects/internal/frei0reffect.cpp" line="54"/>
        <source>Error loading Frei0r plugin</source>
        <translation>加载Frei0插件时发生错误</translation>
    </message>
</context>
<context>
    <name>GraphEditor</name>
    <message>
        <location filename="../panels/grapheditor.cpp" line="140"/>
        <source>Graph Editor</source>
        <translation>图形编辑器</translation>
    </message>
    <message>
        <location filename="../panels/grapheditor.cpp" line="141"/>
        <source>Linear</source>
        <translation>线性</translation>
    </message>
    <message>
        <location filename="../panels/grapheditor.cpp" line="142"/>
        <source>Bezier</source>
        <translation>贝塞尔曲线</translation>
    </message>
    <message>
        <location filename="../panels/grapheditor.cpp" line="143"/>
        <source>Hold</source>
        <translation>保留</translation>
    </message>
</context>
<context>
    <name>GraphView</name>
    <message>
        <location filename="../ui/graphview.cpp" line="80"/>
        <source>Zoom to Selection</source>
        <translation>缩放选择</translation>
    </message>
    <message>
        <location filename="../ui/graphview.cpp" line="87"/>
        <source>Zoom to Show All</source>
        <translation>放大显示所有</translation>
    </message>
    <message>
        <location filename="../ui/graphview.cpp" line="96"/>
        <source>Reset View</source>
        <translation>重置视图</translation>
    </message>
</context>
<context>
    <name>InterlacingName</name>
    <message>
        <location filename="../project/media.cpp" line="44"/>
        <source>None (Progressive)</source>
        <translation>无 (进度)</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="45"/>
        <source>Top Field First</source>
        <translation>顶端区域优先</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="46"/>
        <source>Bottom Field First</source>
        <translation>底部区域优先</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="47"/>
        <source>Invalid</source>
        <translation>无效</translation>
    </message>
</context>
<context>
    <name>KeyframeNavigator</name>
    <message>
        <location filename="../ui/keyframenavigator.cpp" line="77"/>
        <source>Enable Keyframes</source>
        <translation>开启关键帧/动画补间</translation>
    </message>
</context>
<context>
    <name>KeyframeView</name>
    <message>
        <location filename="../ui/keyframeview.cpp" line="74"/>
        <source>Linear</source>
        <translation>线性</translation>
    </message>
    <message>
        <location filename="../ui/keyframeview.cpp" line="76"/>
        <source>Bezier</source>
        <translation>贝塞尔曲线</translation>
    </message>
    <message>
        <location filename="../ui/keyframeview.cpp" line="78"/>
        <source>Hold</source>
        <translation>保留</translation>
    </message>
    <message>
        <location filename="../ui/keyframeview.cpp" line="81"/>
        <source>Graph Editor</source>
        <translation>图形编辑器</translation>
    </message>
</context>
<context>
    <name>LabelSlider</name>
    <message>
        <location filename="../ui/labelslider.cpp" line="271"/>
        <source>&amp;Edit</source>
        <translation>输入值(&amp;E)</translation>
    </message>
    <message>
        <location filename="../ui/labelslider.cpp" line="275"/>
        <source>&amp;Reset to Default</source>
        <translation>重置为默认(&amp;R)</translation>
    </message>
    <message>
        <location filename="../ui/labelslider.cpp" line="306"/>
        <location filename="../ui/labelslider.cpp" line="345"/>
        <source>Set Value</source>
        <translation>设定值</translation>
    </message>
    <message>
        <location filename="../ui/labelslider.cpp" line="307"/>
        <location filename="../ui/labelslider.cpp" line="346"/>
        <source>New value:</source>
        <translation>新值:</translation>
    </message>
</context>
<context>
    <name>LoadDialog</name>
    <message>
        <location filename="../dialogs/loaddialog.cpp" line="37"/>
        <source>Loading...</source>
        <translation>加载中...</translation>
    </message>
    <message>
        <location filename="../dialogs/loaddialog.cpp" line="42"/>
        <source>Loading &apos;%1&apos;...</source>
        <translation>加载中 &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../dialogs/loaddialog.cpp" line="48"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>LoadThread</name>
    <message>
        <location filename="../project/loadthread.cpp" line="249"/>
        <source>Version Mismatch</source>
        <translation>版本不匹配</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="250"/>
        <source>This project was saved in a different version of Olive and may not be fully compatible with this version. Would you like to attempt loading it anyway?</source>
        <translation>此项目用Olive的另一个版本保存,可能与此版本不完全兼容.无论如何,您想尝试加载它吗？</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="573"/>
        <source>Invalid Clip Link</source>
        <translation>无效的视频链接</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="574"/>
        <source>This project contains an invalid clip link. It may be corrupt. Would you like to continue loading it?</source>
        <translation>此项目包含无效的剪辑链接.可能已经损坏.您要继续加载吗?</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="697"/>
        <source>%1 - Line: %2 Col: %3</source>
        <translation>%1 - 行: %2 列: %3</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="724"/>
        <source>User aborted loading</source>
        <translation>用户终止加载</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="755"/>
        <source>XML Parsing Error</source>
        <translation>XML解析错误</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="756"/>
        <source>Couldn&apos;t load &apos;%1&apos;. %2</source>
        <translation>无法加载%1&apos;. %2</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="760"/>
        <source>Project Load Error</source>
        <translation>项目加载错误</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="761"/>
        <source>Error loading project: %1</source>
        <translation>加载项目是发生错误: %1</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="303"/>
        <source>Welcome to %1</source>
        <translation>欢迎来到 %1</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="862"/>
        <source>&amp;File</source>
        <translation>文件(&amp;F)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="863"/>
        <source>&amp;New</source>
        <translation>新建(&amp;N)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="864"/>
        <source>&amp;Open Project</source>
        <translation>打开项目(&amp;O)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="865"/>
        <source>Clear Recent List</source>
        <translation>清除最近的列表</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="866"/>
        <source>Open Recent</source>
        <translation>打开最近的列表</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="867"/>
        <source>&amp;Save Project</source>
        <translation>保存项目(&amp;S)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="868"/>
        <source>Save Project &amp;As</source>
        <translation>保存项目为(&amp;A)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="870"/>
        <source>&amp;Import...</source>
        <translation>导入媒体(&amp;I)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="871"/>
        <source>&amp;Export...</source>
        <translation>导出(&amp;E)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="872"/>
        <source>E&amp;xit</source>
        <translation>退出(&amp;I)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="874"/>
        <source>&amp;Edit</source>
        <translation>编辑(&amp;E)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="876"/>
        <source>&amp;Undo</source>
        <translation>撤销(&amp;U)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="877"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="879"/>
        <source>Select &amp;All</source>
        <translation>选择全部(&amp;A)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="880"/>
        <source>Deselect All</source>
        <translation>取消选择所有</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="882"/>
        <source>Ripple to In Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="883"/>
        <source>Ripple to Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="884"/>
        <source>Edit to In Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="885"/>
        <source>Edit to Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="886"/>
        <source>Delete In/Out Point</source>
        <translation>删除标记的区域</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="887"/>
        <source>Ripple Delete In/Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set/Edit Marker</source>
        <translation type="vanished">设置/编辑标记</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="891"/>
        <source>&amp;View</source>
        <translation>视图(&amp;V)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="892"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="893"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="894"/>
        <source>Increase Track Height</source>
        <translation>增加轨道高度</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="895"/>
        <source>Decrease Track Height</source>
        <translation>降低轨道高度</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="896"/>
        <source>Toggle Show All</source>
        <translation>轨道全部显示</translation>
    </message>
    <message>
        <source>Track Lines</source>
        <translation type="vanished">轨道线</translation>
    </message>
    <message>
        <source>Rectified Waveforms</source>
        <translation type="vanished">整流波形</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="899"/>
        <source>Frames</source>
        <translation>帧</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="900"/>
        <source>Drop Frame</source>
        <translation>丢帧补偿</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="901"/>
        <source>Non-Drop Frame</source>
        <translation>精确</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="902"/>
        <source>Milliseconds</source>
        <translation>毫秒</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="904"/>
        <source>Title/Action Safe Area</source>
        <translation>字幕/行动安全区域</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="905"/>
        <source>Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="906"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="907"/>
        <source>4:3</source>
        <translation>4:3</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="908"/>
        <source>16:9</source>
        <translation>16:9</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="909"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="911"/>
        <source>Full Screen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="912"/>
        <source>Full Screen Viewer</source>
        <translation>全屏预览</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="914"/>
        <source>&amp;Playback</source>
        <translation>回放(&amp;P)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="915"/>
        <source>Go to Start</source>
        <translation>回到起始帧</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="916"/>
        <source>Previous Frame</source>
        <translation>前一帧</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="917"/>
        <source>Play/Pause</source>
        <translation>播放/暂停</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="918"/>
        <source>Play In to Out</source>
        <translation>播放已标记的区域</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="919"/>
        <source>Next Frame</source>
        <translation>下一帧</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="920"/>
        <source>Go to End</source>
        <translation>转到结束帧</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="922"/>
        <source>Go to Previous Cut</source>
        <translation>切换到之前的位置</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="923"/>
        <source>Go to Next Cut</source>
        <translation>转到下一个位置</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="924"/>
        <source>Go to In Point</source>
        <translation>转到时间的起始标记处</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="925"/>
        <source>Go to Out Point</source>
        <translation>转到时间的结束标记处</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="927"/>
        <source>Shuttle Left</source>
        <translation>向左播放</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="928"/>
        <source>Shuttle Stop</source>
        <translation>停止播放</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="929"/>
        <source>Shuttle Right</source>
        <translation>向右播放</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="931"/>
        <source>Loop</source>
        <translation>循环播放</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="933"/>
        <source>&amp;Window</source>
        <translation>窗口(&amp;W)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="935"/>
        <source>Project</source>
        <translation>项目</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="936"/>
        <source>Effect Controls</source>
        <translation>特效</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="937"/>
        <source>Timeline</source>
        <translation>时间轴</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="939"/>
        <source>Graph Editor</source>
        <translation>图形编辑器</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="941"/>
        <source>Media Viewer</source>
        <translation>媒体预览</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="942"/>
        <source>Sequence Viewer</source>
        <translation>片段预览</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="944"/>
        <source>Maximize Panel</source>
        <translation>最大化面板</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="945"/>
        <source>Lock Panels</source>
        <translation>锁定面板</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="946"/>
        <source>Reset to Default Layout</source>
        <translation>重置为默认布局</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="948"/>
        <source>&amp;Tools</source>
        <translation>工具(&amp;T)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="949"/>
        <source>Pointer Tool</source>
        <translation>选择/移动/默认</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="950"/>
        <source>Edit Tool</source>
        <translation>选择部分</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="951"/>
        <source>Ripple Tool</source>
        <translation>涟漪的工具</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="952"/>
        <source>Razor Tool</source>
        <translation>剪刀</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="953"/>
        <source>Slip Tool</source>
        <translation>滑动工具</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="954"/>
        <source>Slide Tool</source>
        <translation>幻灯片工具</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="955"/>
        <source>Hand Tool</source>
        <translation>移动时间轴</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="956"/>
        <source>Transition Tool</source>
        <translation>转场/过渡效果</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="957"/>
        <source>Enable Snapping</source>
        <translation>开启边缘吸合/自动对齐</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="958"/>
        <source>Auto-Cut Silence</source>
        <translation>噪声分离</translation>
    </message>
    <message>
        <source>Hold CTRL to toggle this setting</source>
        <translation type="vanished">按住CTRL切换设置</translation>
    </message>
    <message>
        <source>Invert Timeline Scroll Axes</source>
        <translation type="vanished">反转时间轴滚动轴</translation>
    </message>
    <message>
        <source>Enable Drag Files to Timeline</source>
        <translation type="vanished">启用拖动文件到时间轴</translation>
    </message>
    <message>
        <source>Auto-Scale By Default</source>
        <translation type="vanished">默认情况下自动缩放</translation>
    </message>
    <message>
        <source>Audio Scrubbing</source>
        <translation type="vanished">拖动音频同时播放</translation>
    </message>
    <message>
        <source>Enable Drop on Media to Replace</source>
        <translation type="vanished">开启拖动到媒体上面后替换该媒体</translation>
    </message>
    <message>
        <source>Enable Hover Focus</source>
        <translation type="vanished">启用悬停焦点</translation>
    </message>
    <message>
        <source>Ask For Name When Setting Marker</source>
        <translation type="vanished">设置标记时询问名称</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="960"/>
        <source>No Auto-Scroll</source>
        <translation>关闭时间轴自动滚动</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="961"/>
        <source>Page Auto-Scroll</source>
        <translation>页面时间轴自动滚动</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="962"/>
        <source>Smooth Auto-Scroll</source>
        <translation>时间轴自动平滑滚动</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="869"/>
        <location filename="../ui/mainwindow.cpp" line="964"/>
        <source>Preferences</source>
        <translation>首选项</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="966"/>
        <source>Clear Undo</source>
        <translation>清除撤消</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="970"/>
        <source>&amp;Help</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="972"/>
        <source>A&amp;ction Search</source>
        <translation>功能查找(&amp;C)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="974"/>
        <source>Debug Log</source>
        <translation>调试日志</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="976"/>
        <source>Effect Manager</source>
        <translation>特效管理器</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="977"/>
        <source>Check Update</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="978"/>
        <source>Official Site</source>
        <translation>访问官网</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="979"/>
        <source>&amp;About...</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="980"/>
        <source>User Guide</source>
        <translation>使用指南</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="999"/>
        <source>&lt;untitled&gt;</source>
        <translation>&lt;无标题&gt;</translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="../timeline/marker.cpp" line="64"/>
        <source>Set Marker</source>
        <translation>设置标记</translation>
    </message>
    <message>
        <location filename="../timeline/marker.cpp" line="66"/>
        <source>Set clip marker name:</source>
        <translation>设置该剪辑标记的名称:</translation>
    </message>
    <message>
        <location filename="../timeline/marker.cpp" line="67"/>
        <source>Set sequence marker name:</source>
        <translation>设置序列标记名称:</translation>
    </message>
</context>
<context>
    <name>Media</name>
    <message>
        <location filename="../project/media.cpp" line="95"/>
        <source>New Folder</source>
        <translation>新建文件夹</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="120"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="120"/>
        <source>Filename:</source>
        <translation>文件名:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="124"/>
        <source>Video Dimensions:</source>
        <translation>视频分辨率:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="134"/>
        <source>Frame Rate:</source>
        <translation>帧率:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="144"/>
        <source>%1 field(s) (%2 frame(s))</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="153"/>
        <source>Interlacing:</source>
        <translation>隔行扫描</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="165"/>
        <source>Audio Frequency:</source>
        <translation>音频频率:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="174"/>
        <source>Audio Channels:</source>
        <translation>音频通道:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="192"/>
        <source>Name: %1
Video Dimensions: %2x%3
Frame Rate: %4
Audio Frequency: %5
Audio Layout: %6</source>
        <translation>名称: %1
视频|分辨率: %2x%3
视频|帧率:: %4
音频|频率: %5
音频|布局: %6</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="322"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="324"/>
        <source>Duration</source>
        <translation>持续时间</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="328"/>
        <source>Rate</source>
        <translation>速率</translation>
    </message>
</context>
<context>
    <name>MediaPropertiesDialog</name>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="44"/>
        <source>&quot;%1&quot; Properties</source>
        <translation>属性 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="53"/>
        <source>Tracks:</source>
        <translation>轨道:</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="61"/>
        <source>Video %1: %2x%3 %4FPS</source>
        <translation>视频 %1: %2x%3 %4FPS</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="77"/>
        <source>Audio %1: %2Hz %3</source>
        <translation>音频 %1: %2Hz %3</translation>
    </message>
    <message numerus="yes">
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="80"/>
        <source>%n channel(s)</source>
        <translation>
            <numerusform>%n 通道</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="95"/>
        <source>Conform to Frame Rate:</source>
        <translation>符合帧率:</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="105"/>
        <source>Alpha is Premultiplied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="114"/>
        <source>Auto (%1)</source>
        <translation>自动 (%1)</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="127"/>
        <source>Interlacing:</source>
        <translation>隔行扫描</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="134"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
</context>
<context>
    <name>MenuHelper</name>
    <message>
        <location filename="../ui/menuhelper.cpp" line="169"/>
        <source>&amp;Import</source>
        <translation>导入媒体</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="170"/>
        <source>&amp;Project</source>
        <translation>项目(&amp;P)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="171"/>
        <source>&amp;Sequence</source>
        <translation>片段(&amp;S)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="172"/>
        <source>&amp;Folder</source>
        <translation>目录(&amp;F)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="173"/>
        <source>Set In Point</source>
        <translation>设置时间的起始标记</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="174"/>
        <source>Set Out Point</source>
        <translation>设置时间的结束标记</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="175"/>
        <source>Reset In Point</source>
        <translation>重置时间的起始标记</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="176"/>
        <source>Reset Out Point</source>
        <translation>重置时间的结束标记</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="177"/>
        <source>Clear In/Out Point</source>
        <translation>清除时间标记</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="178"/>
        <source>Add Default Transition</source>
        <translation>添加默认的转场效果</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="179"/>
        <source>Link/Unlink</source>
        <translation>组合/分离音频视频</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="180"/>
        <source>Enable/Disable</source>
        <translation>启用/禁用</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="181"/>
        <source>Enable/Disable track</source>
        <translation>启用/禁用轨道</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="182"/>
        <source>Nest</source>
        <translation>嵌套</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="183"/>
        <source>Cu&amp;t</source>
        <translation>剪切(&amp;T)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="184"/>
        <source>Cop&amp;y</source>
        <translation>复制(&amp;Y)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="185"/>
        <location filename="../ui/menuhelper.cpp" line="280"/>
        <source>&amp;Paste</source>
        <translation>粘帖(&amp;P)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="186"/>
        <source>Paste Insert</source>
        <translation>插入式粘贴</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="187"/>
        <source>Duplicate</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="188"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="189"/>
        <source>Ripple Delete</source>
        <translation>抽出片段并删除</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="190"/>
        <source>Split</source>
        <translation>切断</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="233"/>
        <source>Invalid aspect ratio</source>
        <translation>无效的长宽比</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="233"/>
        <source>The aspect ratio &apos;%1&apos; is invalid. Please try again.</source>
        <translation>长宽比无效 &apos;%1&apos;, 请再试一次.</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="236"/>
        <source>Enter custom aspect ratio</source>
        <translation>输入自定义纵横比</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="236"/>
        <source>Enter the aspect ratio to use for the title/action safe area (e.g. 16:9):</source>
        <translation>输入字幕/动作安全区使用的纵横比 (例子, 16:9):</translation>
    </message>
</context>
<context>
    <name>NewProjDialog</name>
    <message>
        <location filename="../dialogs/newproj.cpp" line="60"/>
        <source>Recent projects</source>
        <translation>最近项目</translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="102"/>
        <source>open other project</source>
        <translation>打开其他项目</translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="49"/>
        <location filename="../dialogs/newproj.cpp" line="107"/>
        <source>create new project</source>
        <translation>创建新项目</translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="47"/>
        <source>Project</source>
        <translation>项目</translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="93"/>
        <source>none</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="138"/>
        <source>project name</source>
        <translation>项目名</translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="138"/>
        <source>Project name cannot be empty</source>
        <translation>项目名成不能为空</translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="161"/>
        <source>Project Name:</source>
        <translation>项目名称:</translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="168"/>
        <source>customize</source>
        <translation>自定义</translation>
    </message>
</context>
<context>
    <name>NewSequenceDialog</name>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="62"/>
        <source>Editing &quot;%1&quot;</source>
        <translation>编辑中 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="82"/>
        <source>New Sequence</source>
        <translation>新片段</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="209"/>
        <source>Preset:</source>
        <translation>预置:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="213"/>
        <source>Film 4K</source>
        <translation>4k电影</translation>
    </message>
    <message>
        <source>TV 4K (Ultra HD/2160p)</source>
        <translation type="vanished">4K电视 (Ultra HD/2160p)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="214"/>
        <source>1080p</source>
        <translation>1080p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="215"/>
        <source>V720p</source>
        <translation>短视频</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="216"/>
        <source>720p</source>
        <translation>720p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="217"/>
        <source>480p</source>
        <translation>480p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="218"/>
        <source>360p</source>
        <translation>360p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="219"/>
        <source>240p</source>
        <translation>240p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="220"/>
        <source>144p</source>
        <translation>144p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="221"/>
        <source>NTSC (480i)</source>
        <translation>NTSC (480i)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="222"/>
        <source>PAL (576i)</source>
        <translation>PAL (576i)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="223"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="231"/>
        <source>Video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="235"/>
        <source>Width:</source>
        <translation>宽度:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="241"/>
        <source>Height:</source>
        <translation>高度:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="246"/>
        <source>Frame Rate:</source>
        <translation>帧速率:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="266"/>
        <source>Pixel Aspect Ratio:</source>
        <translation>像素长宽比</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="268"/>
        <source>Square Pixels (1.0)</source>
        <translation>像素长宽比 (1.0)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="271"/>
        <source>Interlacing:</source>
        <translation>隔行扫描</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="273"/>
        <source>None (Progressive)</source>
        <translation>无 (渐进式)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="281"/>
        <source>Audio</source>
        <translation>音频</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="285"/>
        <source>Sample Rate: </source>
        <translation>采样率:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="303"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
</context>
<context>
    <name>OliveGlobal</name>
    <message>
        <location filename="../global/global.cpp" line="109"/>
        <source>Auto-recovery</source>
        <translation>自动恢复</translation>
    </message>
    <message>
        <source>KunGang %1 %2</source>
        <translation type="vanished">昆冈剪辑 %1 %2</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="69"/>
        <source>KunGang %1%2</source>
        <translation>昆冈剪辑 %1%2</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="72"/>
        <location filename="../global/global.cpp" line="100"/>
        <source>KunGang Project %1</source>
        <translation>昆冈剪辑项目 %1</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="99"/>
        <source>KunGang %1.%2</source>
        <translation>昆冈剪辑 %1.%2</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="110"/>
        <source>KunGang didn&apos;t close properly and an autorecovery file was detected. Would you like to open it?</source>
        <translation>昆冈剪辑上次没有正常关闭，自动恢复打开上次文件?</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="265"/>
        <source>Open Project...</source>
        <translation>打开项目...</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="278"/>
        <source>Missing recent project</source>
        <translation>缺少最近的项目</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="279"/>
        <source>The project &apos;%1&apos; no longer exists. Would you like to remove it from the recent projects list?</source>
        <translation>这个项目 &apos;%1&apos; 已经不存在了。您想把它从最近的项目列表中删除吗?</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="290"/>
        <source>Save Project As...</source>
        <translation>保存项目为...</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="315"/>
        <source>replace project</source>
        <translation>项目存在</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="316"/>
        <source>file is existing, do you want to overwrite it? </source>
        <translation>文件已经存在，要覆盖它吗？</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="347"/>
        <source>Unsaved Project</source>
        <translation>未保存的项目</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="348"/>
        <source>This project has changed since it was last saved. Would you like to save it before closing?</source>
        <translation>这个项目自从上次保存以来已经发生了变化,您想在关闭前保存吗?</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="444"/>
        <source>No active sequence</source>
        <translation>没有已激活的片段</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="445"/>
        <source>Please open the sequence to perform this action.</source>
        <translation>请打开片段以执行这个功能.</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="510"/>
        <source>No clips selected</source>
        <translation>没有剪辑被选择</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="511"/>
        <source>Select the clips you wish to auto-cut</source>
        <translation>选择剪辑以自动剪裁</translation>
    </message>
    <message>
        <source>Please open the sequence you wish to export.</source>
        <translation type="vanished">请打开要输出的片段.</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="379"/>
        <source>Missing Project File</source>
        <translation>丢失的项目文件</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="380"/>
        <source>Specified project &apos;%1&apos; does not exist.</source>
        <translation>指定的项目 &apos;%1&apos; 未找到.</translation>
    </message>
</context>
<context>
    <name>PanEffect</name>
    <message>
        <location filename="../effects/internal/paneffect.cpp" line="32"/>
        <source>Pan</source>
        <translation>左右平衡/平移</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="84"/>
        <source>Preferences</source>
        <translation>首选项</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="91"/>
        <source>Default Sequence</source>
        <translation>默认片段</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="196"/>
        <source>Invalid CSS File</source>
        <translation>无效的CSS文件</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="197"/>
        <source>CSS file &apos;%1&apos; does not exist.</source>
        <translation>CSS文件 &apos;%1&apos; 不存在.</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="227"/>
        <source>Restart Required</source>
        <translation>重启程序</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="228"/>
        <source>Would you like to restart Kungang to make some settings take effect now?</source>
        <translation>你想要重新启动程序，让设置生效吗？</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="365"/>
        <source>Confirm Reset All Shortcuts</source>
        <translation>确认重置所有快捷键</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="366"/>
        <source>Are you sure you wish to reset all keyboard shortcuts to their defaults?</source>
        <translation>您确定要将所有键盘快捷键重置为默认值吗?</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="416"/>
        <source>Import Keyboard Shortcuts</source>
        <translation>导入键盘快捷键配置</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="440"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="464"/>
        <source>Error saving shortcuts</source>
        <translation>保存键盘快捷键是发生错误</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="441"/>
        <source>Failed to open file for reading</source>
        <translation>无法读取文件</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="448"/>
        <source>Export Keyboard Shortcuts</source>
        <translation>导出键盘快捷键配置</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="462"/>
        <source>Export Shortcuts</source>
        <translation>导出快捷键</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="462"/>
        <source>Shortcuts exported successfully</source>
        <translation>快捷键成功导出</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="464"/>
        <source>Failed to open file for writing</source>
        <translation>无法写入文件</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="470"/>
        <source>Browse for CSS file</source>
        <translation>浏览CSS文件</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="478"/>
        <source>Delete All Previews</source>
        <translation>删除所有预览</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="479"/>
        <source>Are you sure you want to delete all previews?</source>
        <translation>您确定要删除所有预览吗?</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="483"/>
        <source>Previews Deleted</source>
        <translation>预览成功删除</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="484"/>
        <source>All previews deleted successfully. You may have to re-open your current project for changes to take effect.</source>
        <translation>所有预览成功删除,重新打开当前项目以生效.</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="508"/>
        <source>Language:</source>
        <translation>语言:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="544"/>
        <source>Image sequence formats:</source>
        <translation>图形片段格式:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="554"/>
        <source>Thumbnail Resolution:</source>
        <translation>缩略图分辨率:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="562"/>
        <source>Waveform Resolution:</source>
        <translation>音频波形分辨率:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="570"/>
        <source>Delete Previews</source>
        <translation>删除预览</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="575"/>
        <source>Default frames for image:</source>
        <translatorcomment>插入图片到片段时，图片持续的帧数</translatorcomment>
        <translation>图片默认帧数</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="585"/>
        <source>Use Software Fallbacks When Possible</source>
        <translation>尽量用软件回放</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="592"/>
        <source>Default Sequence Settings</source>
        <translation>默认的片段设置</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="596"/>
        <source>General</source>
        <translation>通用</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="600"/>
        <source>Behavior</source>
        <translation>行为</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="604"/>
        <source>Add Default Effects to New Clips</source>
        <translation>添加默认效果到新的剪辑</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="608"/>
        <source>Automatically Seek to the Beginning When Playing at the End of a Sequence</source>
        <translation>当播放结束后自动回到开始位置</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="612"/>
        <source>Selecting Also Seeks</source>
        <translation>选择并任意预览</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="616"/>
        <source>Edit Tool Also Seeks</source>
        <translation>编辑工具也任意预览</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="620"/>
        <source>Edit Tool Selects Links</source>
        <translation>编辑工具选中链接</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="624"/>
        <source>Seek Also Selects</source>
        <translation>预览同时选中</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="628"/>
        <source>Seek to the End of Pastes</source>
        <translation>预览到粘贴内容结尾</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="632"/>
        <source>Scroll Wheel Zooms</source>
        <translation>滚轮缩放</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="633"/>
        <source>Hold CTRL to toggle this setting</source>
        <translation>CTRL键和滚轮同时使用实现同样的效果</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="637"/>
        <source>Invert Timeline Scroll Axes</source>
        <translation>反转时间轴滚动轴</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="641"/>
        <source>Enable Drag Files to Timeline</source>
        <translation>开启拖放文件到时间轴</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="645"/>
        <source>Auto-Scale By Default</source>
        <translation>默认情况下自动缩放</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="649"/>
        <source>Auto-Seek to Imported Clips</source>
        <translation>自动寻找并导入剪辑</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="653"/>
        <source>Audio Scrubbing</source>
        <translation>拖动音频同时播放</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="657"/>
        <source>Drop Files on Media to Replace</source>
        <translation>拖放文件以代替媒体</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="661"/>
        <source>Enable Hover Focus</source>
        <translation>启用悬停焦点</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="665"/>
        <source>Ask For Name When Setting Marker</source>
        <translation>设置标记时询问名称</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="671"/>
        <source>Appearance</source>
        <translation>外观</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="678"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="681"/>
        <source>Kungang Dark (Default)</source>
        <translation>昆冈剪辑暗色主题（默认）</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="682"/>
        <source>Kungang Light</source>
        <translation>昆冈剪辑亮色主题</translation>
    </message>
    <message>
        <source>Olive Dark (Default)</source>
        <translation type="vanished">Olive 暗色 (默认)</translation>
    </message>
    <message>
        <source>Olive Light</source>
        <translation type="vanished">Olive 明亮</translation>
    </message>
    <message>
        <source>Native</source>
        <translation type="vanished">原生</translation>
    </message>
    <message>
        <source>Native (Light Icons)</source>
        <translation type="vanished">原生 (明亮图标)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="693"/>
        <source>Use Native Menu Styling</source>
        <translation>使用原生菜单风格</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="701"/>
        <source>Custom CSS:</source>
        <translation>自定义 CSS:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="707"/>
        <source>Browse</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="714"/>
        <source>Effect Textbox Lines:</source>
        <translation>文本框线效果:</translation>
    </message>
    <message>
        <source>Seeking</source>
        <translation type="vanished">查找中</translation>
    </message>
    <message>
        <source>Accurate Seeking
Always show the correct frame (visual may pause briefly as correct frame is retrieved)</source>
        <translation type="vanished">精准查找
总是显示当前按的帧 (视觉可能会在检索到正确的帧时暂停)</translation>
    </message>
    <message>
        <source>Fast Seeking
Seek quickly (may briefly show inaccurate frames when seeking - doesn&apos;t affect playback/export)</source>
        <translation type="vanished">快速查找
查找得更快 (搜索时可能会短暂显示不准确的帧—不影响回放/导出)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="729"/>
        <source>Memory Usage</source>
        <translation>内存使用</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="731"/>
        <source>Upcoming Frame Queue:</source>
        <translation>即将到来的帧队列:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="736"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="745"/>
        <source>frames</source>
        <translation>帧</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="737"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="746"/>
        <source>seconds</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="740"/>
        <source>Previous Frame Queue:</source>
        <translation>前一帧队列:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="751"/>
        <source>Playback</source>
        <translation>回放</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="762"/>
        <source>Output Device:</source>
        <translation>输出设备:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="765"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="788"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="785"/>
        <source>Input Device:</source>
        <translation>输入设备:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="808"/>
        <source>Sample Rate:</source>
        <translation>采样率:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="824"/>
        <source>Audio Recording:</source>
        <translation>音频录制:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="827"/>
        <source>Mono</source>
        <translation>单声道</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="828"/>
        <source>Stereo</source>
        <translation>立体声</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="834"/>
        <source>Audio</source>
        <translation>音频</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="842"/>
        <source>Search for action or shortcut</source>
        <translation>搜索功能或者快捷键</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="849"/>
        <source>Action</source>
        <translation>功能</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="850"/>
        <source>Shortcut</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="855"/>
        <source>Import</source>
        <translation>导入媒体</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="859"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="865"/>
        <source>Reset Selected</source>
        <translation>重新选择</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="869"/>
        <source>Reset All</source>
        <translation>全部重设</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="875"/>
        <source>Keyboard</source>
        <translation>键盘</translation>
    </message>
</context>
<context>
    <name>PreviewGenerator</name>
    <message>
        <location filename="../project/previewgenerator.cpp" line="205"/>
        <source>Failed to find any valid video/audio streams</source>
        <translation>未能找到任何有效的视频/音频流</translation>
    </message>
    <message>
        <location filename="../project/previewgenerator.cpp" line="561"/>
        <source>Could not open file - %1</source>
        <translation>无法打开文件 — %1</translation>
    </message>
    <message>
        <location filename="../project/previewgenerator.cpp" line="568"/>
        <source>Could not find stream information - %1</source>
        <translation>无法找到流信息 — %1</translation>
    </message>
</context>
<context>
    <name>Project</name>
    <message>
        <location filename="../panels/project.cpp" line="99"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="105"/>
        <source>Open Project</source>
        <translation>打开项目</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="111"/>
        <source>Save Project</source>
        <translation>保存项目</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="117"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="123"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="134"/>
        <source>Tree View</source>
        <translation>树视图</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="140"/>
        <source>Icon View</source>
        <translation>缩略图</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="146"/>
        <source>List View</source>
        <translation>列表视图</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="224"/>
        <source>Search media, markers, etc.</source>
        <translation>搜索媒体,标记等.</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="225"/>
        <source>Project</source>
        <translation>项目</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="229"/>
        <source>Sequence</source>
        <translation>片段</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="351"/>
        <source>Replace &apos;%1&apos;</source>
        <translation>替换 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="353"/>
        <source>All Files</source>
        <translation>全部文件</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="364"/>
        <location filename="../panels/project.cpp" line="923"/>
        <source>No active sequence</source>
        <translation>没有活动片段</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="365"/>
        <source>No sequence is active, please open the sequence you want to replace clips from.</source>
        <translation>没有片段处于活动状态,请打开要代替剪辑的片段.</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="373"/>
        <source>Active sequence selected</source>
        <translation>使活选择的片段</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="374"/>
        <source>You cannot insert a sequence into itself, so no clips of this media would be in this sequence.</source>
        <translation>无法插入自己的片段,所以这个媒体的剪辑不会在这个片段中.</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="405"/>
        <source>Rename &apos;%1&apos;</source>
        <translation>重命名 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="406"/>
        <source>Enter new name:</source>
        <translation>输入新的名称:</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="577"/>
        <source>Delete media in use?</source>
        <translation>删除使用中的媒体?</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="578"/>
        <source>The media &apos;%1&apos; is currently used in &apos;%2&apos;. Deleting it will remove all instances in the sequence. Are you sure you want to do this?</source>
        <translation>此媒体 &apos;%1&apos; 正在被使用于 &apos;%2&apos;. 删除它将删除片段中的所有实例. 你确定你要这么做吗?</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="581"/>
        <source>Skip</source>
        <translation>跳过</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="750"/>
        <source>Import a Project</source>
        <translation>导入一个项目</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="751"/>
        <source>&quot;%1&quot; is an KunGang project file. It will merge with this project. Do you wish to continue?</source>
        <translation>% 是项目文件，其内容将合并到当前项目，继续？</translation>
    </message>
    <message>
        <source>&quot;%1&quot; is an Olive project file. It will merge with this project. Do you wish to continue?</source>
        <translation type="vanished">&quot;%1&quot; 是Olive项目文件. 它将与这个项目合并. 你想继续吗?</translation>
    </message>
    <message>
        <source>Image sequence detected</source>
        <translation type="vanished">图像片段检测</translation>
    </message>
    <message>
        <source>The file &apos;%1&apos; appears to be part of an image sequence. Would you like to import it as such?</source>
        <translation type="vanished">该文件 &apos;%1&apos; 似乎是图像片段中的一部分. 您要按原样代替吗?</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="911"/>
        <source>Import media...</source>
        <translation>导入媒体...</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="911"/>
        <source>Media Files</source>
        <translation>媒体文件</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="924"/>
        <source>No sequence is active, please open the sequence you want to delete clips from.</source>
        <translation>没有片段处于活动状态,请打开要从中删除剪辑的片段.</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="41"/>
        <source>Create Proxy</source>
        <translation>创建衍生</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="44"/>
        <source>Proxy</source>
        <translation>衍生</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="50"/>
        <source>Dimensions:</source>
        <translation>大小:</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="53"/>
        <source>Same Size as Source</source>
        <translation>使用与来源相同的大小</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="54"/>
        <source>Half Resolution (1/2)</source>
        <translation>一半的分辨率 (1/2)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="55"/>
        <source>Quarter Resolution (1/4)</source>
        <translation>四分之一的分辨率 (1/4)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="56"/>
        <source>Eighth Resolution (1/8)</source>
        <translation>八分之一的分辨率 (1/8)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="57"/>
        <source>Sixteenth Resolution (1/16)</source>
        <translation>十六分之一的分辨率 (1/16)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="61"/>
        <source>Format:</source>
        <translation>格式:</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="64"/>
        <source>ProRes HQ</source>
        <translation>ProRes HQ</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="72"/>
        <source>Location:</source>
        <translation>位置:</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="75"/>
        <source>Same as Source (in &quot;%1&quot; folder)</source>
        <translation>使用与来源相同的大小 (在 &quot;%1&quot; 目录)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="128"/>
        <source>Proxy file exists</source>
        <translation>衍生文件存在</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="129"/>
        <source>The file &quot;%1&quot; already exists. Do you wish to replace it?</source>
        <translation>该文件 &quot;%1&quot; 已经存在. 你想代替它吗?</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="182"/>
        <source>Custom Location</source>
        <translation>自定义路径</translation>
    </message>
</context>
<context>
    <name>ProxyGenerator</name>
    <message>
        <location filename="../project/proxygenerator.cpp" line="332"/>
        <source>Finished generating proxy for &quot;%1&quot;</source>
        <translation>完成生成衍生 &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../effects/effectloaders.cpp" line="56"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="61"/>
        <source>Pan</source>
        <translation>平移</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="70"/>
        <source>Tone</source>
        <translation>音质</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="75"/>
        <source>Noise</source>
        <translation>噪音</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="80"/>
        <source>Fill Left/Right</source>
        <translation>左右声道填充</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="88"/>
        <source>Transform</source>
        <translation>变形</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="89"/>
        <source>Distort</source>
        <translation>扭曲</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="94"/>
        <source>Corner Pin</source>
        <translation>别角</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="103"/>
        <source>Shake</source>
        <translation>抖动</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="109"/>
        <source>Text</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="110"/>
        <location filename="../effects/effectloaders.cpp" line="117"/>
        <source>Render</source>
        <translation>渲染</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="116"/>
        <source>Rich Text</source>
        <translation>富文本</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="122"/>
        <source>Timecode</source>
        <translation>时间码</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="127"/>
        <source>Solid</source>
        <translation>单色</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="136"/>
        <source>Cross Dissolve</source>
        <translation>淡入淡出</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="143"/>
        <source>Linear Fade</source>
        <translation>线性过渡</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="148"/>
        <source>Exponential Fade</source>
        <translation>指数过渡</translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="153"/>
        <source>Logarithmic Fade</source>
        <translation>对数过渡</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="436"/>
        <location filename="../global/config.cpp" line="481"/>
        <source>Radius</source>
        <translation>放射</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="437"/>
        <location filename="../global/config.cpp" line="479"/>
        <source>Horizontal</source>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="438"/>
        <location filename="../global/config.cpp" line="480"/>
        <location filename="../global/config.cpp" line="525"/>
        <source>Vertical</source>
        <translation>垂直</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="439"/>
        <source>Amount</source>
        <translation>量</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="440"/>
        <location filename="../global/config.cpp" line="497"/>
        <location filename="../global/config.cpp" line="504"/>
        <location filename="../global/config.cpp" line="510"/>
        <source>Center</source>
        <translation>中心</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="441"/>
        <source>Mode</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="442"/>
        <source>Key Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="443"/>
        <source>Lower Tolerance</source>
        <translation>下限值</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="444"/>
        <source>Upper Tolerance</source>
        <translation>上限</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="445"/>
        <source>Red Amount</source>
        <translation>红值</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="446"/>
        <source>Green Amount</source>
        <translation>绿值</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="447"/>
        <source>Blue Amount</source>
        <translation>蓝值</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="448"/>
        <source>Temperature</source>
        <translation>温度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="449"/>
        <source>Tint</source>
        <translation>色调</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="450"/>
        <source>Exposure</source>
        <translation>曝光</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="451"/>
        <location filename="../global/config.cpp" line="474"/>
        <source>Contrast</source>
        <translation>对比度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="452"/>
        <source>Highlights</source>
        <translation>高亮</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="453"/>
        <source>Shadows</source>
        <translation>阴影</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="454"/>
        <source>Whites (Brightness)</source>
        <translation>白（亮）</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="455"/>
        <source>Blacks (Darkness)</source>
        <translation>黑（暗)</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="456"/>
        <location filename="../global/config.cpp" line="484"/>
        <location filename="../global/config.cpp" line="532"/>
        <source>Saturation</source>
        <translation>饱和度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="457"/>
        <source>Find by</source>
        <translation>根据</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="458"/>
        <location filename="../global/config.cpp" line="487"/>
        <source>Lower Limit</source>
        <translation>下限</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="459"/>
        <location filename="../global/config.cpp" line="488"/>
        <source>Upper Limit</source>
        <translation>上限</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="460"/>
        <location filename="../global/config.cpp" line="466"/>
        <location filename="../global/config.cpp" line="468"/>
        <location filename="../global/config.cpp" line="477"/>
        <location filename="../global/config.cpp" line="489"/>
        <location filename="../global/config.cpp" line="521"/>
        <source>Invert</source>
        <translation>反选</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="461"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="462"/>
        <source>Top</source>
        <translation>顶部</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="463"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="464"/>
        <source>Bottom</source>
        <translation>底下</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="465"/>
        <source>Feather</source>
        <translation>羽化</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="467"/>
        <location filename="../global/config.cpp" line="478"/>
        <location filename="../global/config.cpp" line="518"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="469"/>
        <source>Channel</source>
        <translation>通道</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="470"/>
        <source>Factor</source>
        <translation>因子</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="471"/>
        <source>Balance</source>
        <translation>平衡</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="472"/>
        <source>Length</source>
        <translation>长度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="473"/>
        <location filename="../global/config.cpp" line="508"/>
        <source>Angle</source>
        <translation>角度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="475"/>
        <source>Resolution</source>
        <translation>分辨率</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="476"/>
        <source>HSL mode</source>
        <translation>HSL模式</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="482"/>
        <source>Sigma</source>
        <translation type="unfinished">西格玛</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="483"/>
        <location filename="../global/config.cpp" line="531"/>
        <source>Hue</source>
        <translation>色度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="485"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="490"/>
        <source>Color Noise</source>
        <translation>颜色噪点</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="491"/>
        <source>Blend</source>
        <translation>混合</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="492"/>
        <source>Horizontal Pixels</source>
        <translation>水平像素</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="493"/>
        <source>Vertical Pixels</source>
        <translation>垂直像素</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="494"/>
        <source>Bypass</source>
        <translation>绕过</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="495"/>
        <location filename="../global/config.cpp" line="513"/>
        <source>Colors</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="496"/>
        <source>Gamma</source>
        <translation type="unfinished">伽马</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="498"/>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="499"/>
        <location filename="../global/config.cpp" line="523"/>
        <source>Intensity</source>
        <translation>强度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="500"/>
        <location filename="../global/config.cpp" line="522"/>
        <source>Frequency</source>
        <translation>频率</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="501"/>
        <source>Reverse</source>
        <translation>反向</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="502"/>
        <location filename="../global/config.cpp" line="507"/>
        <source>Stretch</source>
        <translation>拉长</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="505"/>
        <source>Tile</source>
        <translation>平铺</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="506"/>
        <source>Hide Edges</source>
        <translation>藏边</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="509"/>
        <source>Scale</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="511"/>
        <source>Mirror X</source>
        <translation>水平镜像</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="512"/>
        <source>Mirror Y</source>
        <translation>垂直镜像</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="514"/>
        <source>Saturation Levels</source>
        <translation>饱和度等级</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="515"/>
        <source>Brightness Levels</source>
        <translation>亮度等级</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="516"/>
        <source>Edge Tolerance</source>
        <translation>边缘容限</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="517"/>
        <source>Edge Strength</source>
        <translation>边缘强度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="519"/>
        <source>Softness</source>
        <translation>柔化</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="520"/>
        <source>Circular</source>
        <translation>圆形</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="524"/>
        <source>Evolution</source>
        <translation>进化</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="526"/>
        <source>Composite</source>
        <translation>合成</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="527"/>
        <source>Alpha</source>
        <translation type="unfinished">阿尔法</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="528"/>
        <source>Original</source>
        <translation>本原</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="529"/>
        <source>Luminance</source>
        <translation>亮度</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="530"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="533"/>
        <source>Red</source>
        <translation>红色</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="534"/>
        <source>Green</source>
        <translation>绿色</translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="535"/>
        <source>Blue</source>
        <translation>蓝色</translation>
    </message>
</context>
<context>
    <name>ReplaceClipMediaDialog</name>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="37"/>
        <source>Replace clips using &quot;%1&quot;</source>
        <translation>取代剪辑使用 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="43"/>
        <source>Select which media you want to replace this media&apos;s clips with:</source>
        <translation>选择要替换此媒体的媒体:</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="49"/>
        <source>Keep the same media in-points</source>
        <translation>保持相同的媒体插入点</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="57"/>
        <source>Replace</source>
        <translation>取代</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="61"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="77"/>
        <source>No media selected</source>
        <translation>没有已选择的媒体</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="78"/>
        <source>Please select a media to replace with or click &apos;Cancel&apos;.</source>
        <translation>请选择一个媒体替代或取消.</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="86"/>
        <source>Same media selected</source>
        <translation>相同的媒体被选择</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="87"/>
        <source>You selected the same media that you&apos;re replacing. Please select a different one or click &apos;Cancel&apos;.</source>
        <translation>你选择了相同的媒体代替.请选择其他或者取消.</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="93"/>
        <source>Folder selected</source>
        <translation>目录选择</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="94"/>
        <source>You cannot replace footage with a folder.</source>
        <translation>您无法用文件夹替换素材.</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="101"/>
        <source>Active sequence selected</source>
        <translation>活动的片段已经被选择</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="102"/>
        <source>You cannot insert a sequence into itself.</source>
        <translation>无法插入自身的片段.</translation>
    </message>
</context>
<context>
    <name>RichTextEffect</name>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="22"/>
        <source>Text</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="26"/>
        <source>Padding</source>
        <translation>填充</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="30"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="34"/>
        <source>Vertical Align:</source>
        <translation>垂直对齐:</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="36"/>
        <source>Top</source>
        <translation>顶部</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="37"/>
        <source>Center</source>
        <translation>中心点</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="38"/>
        <source>Bottom</source>
        <translation>底下</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="42"/>
        <source>Auto-Scroll</source>
        <translation>自动滚动</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="44"/>
        <source>Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="45"/>
        <source>Up</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="46"/>
        <source>Down</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="47"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="48"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="51"/>
        <source>Shadow</source>
        <translation>阴影</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="55"/>
        <source>Shadow Color</source>
        <translation>阴影颜色</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="59"/>
        <source>Shadow Angle</source>
        <translation>阴影角度</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="63"/>
        <source>Shadow Distance</source>
        <translation>阴影距离</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="68"/>
        <source>Shadow Softness</source>
        <translation>阴影柔化</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="73"/>
        <source>Shadow Opacity</source>
        <translation>阴影透明度</translation>
    </message>
</context>
<context>
    <name>Sequence</name>
    <message>
        <location filename="../timeline/sequence.cpp" line="41"/>
        <source>%1 (copy)</source>
        <translation>%1 (复制)</translation>
    </message>
</context>
<context>
    <name>ShakeEffect</name>
    <message>
        <location filename="../effects/internal/shakeeffect.cpp" line="38"/>
        <source>Intensity</source>
        <translation>强度</translation>
    </message>
    <message>
        <location filename="../effects/internal/shakeeffect.cpp" line="43"/>
        <source>Rotation</source>
        <translation>旋转</translation>
    </message>
    <message>
        <location filename="../effects/internal/shakeeffect.cpp" line="48"/>
        <source>Frequency</source>
        <translation>频率</translation>
    </message>
</context>
<context>
    <name>SolidEffect</name>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="43"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="45"/>
        <source>Solid Color</source>
        <translation>纯色</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="46"/>
        <source>SMPTE Bars</source>
        <translation>无内容条形背景</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="47"/>
        <source>Checkerboard</source>
        <translation>检测板</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="49"/>
        <source>Opacity</source>
        <translation>透明度</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="55"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="59"/>
        <source>Checkerboard Size</source>
        <translation>检测板尺寸</translation>
    </message>
</context>
<context>
    <name>SourcesCommon</name>
    <message>
        <location filename="../project/sourcescommon.cpp" line="83"/>
        <source>Import...</source>
        <translation>导入媒体...</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="86"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="90"/>
        <source>View</source>
        <translation>视图</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="93"/>
        <source>Tree View</source>
        <translation>树视图</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="96"/>
        <source>Icon View</source>
        <translation>图标视图</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="99"/>
        <source>Show Toolbar</source>
        <translation>显示工具栏</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="104"/>
        <source>Show Sequences</source>
        <translation>显示片段</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="116"/>
        <source>Replace/Relink Media</source>
        <translation>替换/重新链接媒体</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="120"/>
        <source>Reveal in Explorer</source>
        <translation>打开所在文件夹</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="122"/>
        <source>Reveal in Finder</source>
        <translation>在查找当中预览</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="124"/>
        <source>Reveal in File Manager</source>
        <translation>在文件管理器中预览</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="129"/>
        <source>Replace Clips Using This Media</source>
        <translation>使用此媒体替换剪辑</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="152"/>
        <source>Create Sequence With This Media</source>
        <translation>使用此媒体创建片段</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="158"/>
        <source>Duplicate</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="164"/>
        <source>Delete All Clips Using This Media</source>
        <translation>删除所有使用此媒体的剪辑</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="167"/>
        <source>Proxy</source>
        <translation>衍生</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="174"/>
        <source>Generating proxy: %1% complete</source>
        <translation>生成衍生: %1% 完成</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="198"/>
        <source>Create/Modify Proxy</source>
        <translation>创建/修改衍生</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="201"/>
        <source>Create Proxy</source>
        <translation>创建衍生</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="212"/>
        <source>Modify Proxy</source>
        <translation>修改衍生</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="215"/>
        <source>Restore Original</source>
        <translation>还原为原始尺寸 </translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="221"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="228"/>
        <source>Preview in Media Viewer</source>
        <translation>在媒体浏览器中预览</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="234"/>
        <source>Properties...</source>
        <translation>属性...</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="291"/>
        <source>Replace Media</source>
        <translation>取代媒体</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="292"/>
        <source>You dropped a file onto &apos;%1&apos;. Would you like to replace it with the dropped file?</source>
        <translation>你拖放了一个文件到 &apos;%1&apos;. 你要取代它吗?</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="422"/>
        <source>Delete proxy</source>
        <translation>删除衍生</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="423"/>
        <source>Would you like to delete the proxy file &quot;%1&quot; as well?</source>
        <translation>您要删除衍生文件吗 &quot;%1&quot;?</translation>
    </message>
</context>
<context>
    <name>SpeedDialog</name>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="40"/>
        <source>Speed/Duration</source>
        <translation>速度/持续时间</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="49"/>
        <source>Speed:</source>
        <translation>速度:</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="56"/>
        <source>Frame Rate:</source>
        <translation>帧速率:</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="61"/>
        <source>Duration:</source>
        <translation>持续时间:</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="69"/>
        <source>Reverse</source>
        <translation>反向</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="70"/>
        <source>Maintain Audio Pitch</source>
        <translation>保持音频音调</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="71"/>
        <source>Ripple Changes</source>
        <translation>波纹变化</translation>
    </message>
</context>
<context>
    <name>TextEditDialog</name>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="35"/>
        <source>Edit Text</source>
        <translation>编辑文本格式</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="68"/>
        <source>Thin</source>
        <translation>细</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="69"/>
        <source>Extra Light</source>
        <translation>加亮</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="70"/>
        <source>Light</source>
        <translation>亮</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="71"/>
        <source>Normal</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="72"/>
        <source>Medium</source>
        <translation>中等</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="73"/>
        <source>Demi Bold</source>
        <translation>亚粗</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="74"/>
        <source>Bold</source>
        <translation>粗体</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="75"/>
        <source>Extra Bold</source>
        <translation>加粗</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="76"/>
        <source>Black</source>
        <translation>黑</translation>
    </message>
</context>
<context>
    <name>TextEditEx</name>
    <message>
        <location filename="../ui/texteditex.cpp" line="43"/>
        <source>Edit Text</source>
        <translation>编辑文本</translation>
    </message>
    <message>
        <location filename="../ui/texteditex.cpp" line="91"/>
        <source>&amp;Edit Text</source>
        <translation>编辑文本(&amp;E)</translation>
    </message>
</context>
<context>
    <name>TextEffect</name>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="51"/>
        <source>Text</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="55"/>
        <source>Font</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="59"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="64"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="68"/>
        <source>Alignment</source>
        <translation>校准</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="70"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="71"/>
        <location filename="../effects/internal/texteffect.cpp" line="77"/>
        <source>Center</source>
        <translation>中心</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="72"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="73"/>
        <source>Justify</source>
        <translation>整理版面</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="76"/>
        <source>Top</source>
        <translation>顶部</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="78"/>
        <source>Bottom</source>
        <translation>底下</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="80"/>
        <source>Word Wrap</source>
        <translation>自动换行</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="84"/>
        <source>Padding</source>
        <translation>填充</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="88"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="92"/>
        <source>Outline</source>
        <translation>轮廓</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="96"/>
        <source>Outline Color</source>
        <translation>轮廓颜色</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="100"/>
        <source>Outline Width</source>
        <translation>轮廓宽</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="105"/>
        <source>Shadow</source>
        <translation>阴影</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="109"/>
        <source>Shadow Color</source>
        <translation>阴影颜色</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="113"/>
        <source>Shadow Angle</source>
        <translation>阴影角度</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="117"/>
        <source>Shadow Distance</source>
        <translation>阴影距离</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="122"/>
        <source>Shadow Softness</source>
        <translation>阴影柔软化</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="127"/>
        <source>Shadow Opacity</source>
        <translation>阴影透明度</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="134"/>
        <source>Sample Text</source>
        <translation>文字样本</translation>
    </message>
</context>
<context>
    <name>TimecodeEffect</name>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="51"/>
        <source>Timecode</source>
        <translation>时间码</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="53"/>
        <source>Sequence</source>
        <translation>片段</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="54"/>
        <source>Media</source>
        <translation>媒体</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="57"/>
        <source>Scale</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="64"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="69"/>
        <source>Background Color</source>
        <translation>背景颜色</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="74"/>
        <source>Background Opacity</source>
        <translation>背景透明度</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="81"/>
        <source>Offset</source>
        <translation>补偿</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="85"/>
        <source>Prepend</source>
        <translation>前置</translation>
    </message>
</context>
<context>
    <name>Timeline</name>
    <message>
        <location filename="../panels/timeline.cpp" line="126"/>
        <source>Pointer Tool</source>
        <translation>选择/移动/默认</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="131"/>
        <source>Edit Tool</source>
        <translation>选择部分</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="132"/>
        <source>Ripple Tool</source>
        <translation>涟漪的工具</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="127"/>
        <location filename="../panels/timeline.cpp" line="133"/>
        <source>Razor Tool</source>
        <translation>剪刀</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="128"/>
        <source>CC</source>
        <translation>文字</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="129"/>
        <source>Solid Color</source>
        <translation>单色</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="134"/>
        <source>Slip Tool</source>
        <translation>滑动工具</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="135"/>
        <source>Slide Tool</source>
        <translation>幻灯片工具</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="136"/>
        <source>Hand Tool</source>
        <translation>手形工具</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="138"/>
        <source>Transition Tool</source>
        <translation>过度/转场效果</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="139"/>
        <source>Snapping</source>
        <translation>边缘吸合/自动对齐</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="140"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="141"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="142"/>
        <source>Record audio</source>
        <translation>录制声音</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="143"/>
        <source>Add title, solid, bars, etc.</source>
        <translation>添加文本,单色,条状图等.</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="497"/>
        <source>Nested Sequence</source>
        <translation>嵌套的片段</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1393"/>
        <source>Effect already exists</source>
        <translation>特效已经存在</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1394"/>
        <source>Clip &apos;%1&apos; already contains a &apos;%2&apos; effect. Would you like to replace it with the pasted one or add it as a separate effect?</source>
        <translation>剪辑 &apos;%1&apos; 已经包含了 &apos;%2&apos;效果. 您是想替换它,还是作为单独的效果加入?</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1399"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1400"/>
        <source>Replace</source>
        <translation>取代</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1401"/>
        <source>Skip</source>
        <translation>跳过</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1403"/>
        <source>Do this for all conflicts found</source>
        <translation>对所有发现的冲突都这样做吗</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1866"/>
        <source>Title...</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1871"/>
        <source>Solid Color...</source>
        <translation>单色图</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1876"/>
        <source>Bars...</source>
        <translation>条状图</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1883"/>
        <source>Tone...</source>
        <translation>音色</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1888"/>
        <source>Noise...</source>
        <translation>随机噪音</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1922"/>
        <source>Unsaved Project</source>
        <translation>未保存的项目</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1923"/>
        <source>You must save this project before you can record audio in it.</source>
        <translation>必须先保存此项目,才能在其中录制音频.</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1929"/>
        <source>Click on the timeline where you want to start recording (drag to limit the recording to a certain timeframe)</source>
        <translation>单击要开始录制的时间轴(拖动可将录制限制在某个时间段)</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1990"/>
        <source>Timeline: </source>
        <translation>时间轴: </translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1992"/>
        <source>(none)</source>
        <translation>(无)</translation>
    </message>
</context>
<context>
    <name>TimelineHeader</name>
    <message>
        <location filename="../ui/timelineheader.cpp" line="486"/>
        <source>Center Timecodes</source>
        <translation>以时间区间/点显示</translation>
    </message>
</context>
<context>
    <name>TimelineWidget</name>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="90"/>
        <source>&amp;Undo</source>
        <translation>撤销(&amp;U)</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="91"/>
        <source>&amp;Redo</source>
        <translation>重做(&amp;R)</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="111"/>
        <source>R&amp;ipple Delete Empty Space</source>
        <translation>连接片段/去除空白空间(&amp;I)</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="115"/>
        <source>Sequence Settings</source>
        <translation>片段设置</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="134"/>
        <source>&amp;Speed/Duration</source>
        <translation>速度/持续时间(&amp;S)</translation>
    </message>
    <message>
        <source>Auto-s&amp;cale</source>
        <translation type="vanished">自动缩放(&amp;C)</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="137"/>
        <source>Auto-Cut Silence</source>
        <translation>噪声分离</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="140"/>
        <source>Auto-S&amp;cale</source>
        <translation>自动缩放(&amp;C)</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="175"/>
        <source>&amp;Reveal in Project</source>
        <translation>在项目库中显示(&amp;R)</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="179"/>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="207"/>
        <source>%1
Start: %2
End: %3
Duration: %4</source>
        <translation>%1
起点: %2
终止: %3
持续时间: %4</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="233"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="233"/>
        <source>Couldn&apos;t locate media wrapper for sequence.</source>
        <translation>无法找到片段的媒体包装器.</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="531"/>
        <source>New Sequence</source>
        <translation>新片段</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="532"/>
        <source>No sequence has been created yet. Would you like to make one based on this footage or set custom parameters?</source>
        <translation>必须先创建一个片段。使用默认配置还是自定义配置创建一个片段？</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="534"/>
        <source>Use Footage Parameters</source>
        <translation>使用默认配置</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="535"/>
        <source>Custom Parameters</source>
        <translation>自定义配置</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1076"/>
        <source>Title</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1080"/>
        <source>Solid Color</source>
        <translation>单色</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1085"/>
        <source>Bars</source>
        <translation>条状图</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1096"/>
        <source>Tone</source>
        <translation>音质</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1100"/>
        <source>Noise</source>
        <translation>噪音</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="2029"/>
        <source>Duration:</source>
        <translation>持续时间:</translation>
    </message>
</context>
<context>
    <name>ToneEffect</name>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="31"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="33"/>
        <source>Sine</source>
        <translation>正弦</translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="35"/>
        <source>Frequency</source>
        <translation>频率</translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="41"/>
        <source>Amount</source>
        <translation>数量</translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="47"/>
        <source>Mix</source>
        <translation>混合</translation>
    </message>
</context>
<context>
    <name>TransformEffect</name>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="49"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="54"/>
        <source>Scale</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="64"/>
        <source>Uniform Scale</source>
        <translation>保持比例</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="68"/>
        <source>Rotation</source>
        <translation>旋转</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="72"/>
        <source>Anchor Point</source>
        <translation>锚点</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="77"/>
        <source>Opacity</source>
        <translation>透明度</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="84"/>
        <source>Blend Mode</source>
        <translation>混合模式</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="89"/>
        <source>Normal</source>
        <translation>标准</translation>
    </message>
</context>
<context>
    <name>Transition</name>
    <message>
        <location filename="../effects/transition.cpp" line="48"/>
        <source>Length</source>
        <translation>长度</translation>
    </message>
</context>
<context>
    <name>UpdateNotification</name>
    <message>
        <source>An update is available from the Olive website. Visit www.olivevideoeditor.org to download it.</source>
        <translation type="vanished">发现新版本.请访问www.olivevideoeditor.org下载.</translation>
    </message>
    <message>
        <source>An update is available from the KunGang website. Visit http://www.sumoon.com/kungang to download it.</source>
        <translation type="vanished">有新的版本更新，请访问官网下载更新。</translation>
    </message>
    <message>
        <location filename="../ui/updatenotification.cpp" line="160"/>
        <source>There is new version:%1.%2 for download</source>
        <translation>新版本%1.%2 可供下载</translation>
    </message>
    <message>
        <location filename="../ui/updatenotification.cpp" line="165"/>
        <source>Latest version</source>
        <translation>已是最新版本</translation>
    </message>
</context>
<context>
    <name>VSTHost</name>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="130"/>
        <location filename="../effects/internal/vsthost.cpp" line="146"/>
        <source>Error loading VST plugin</source>
        <translation>加载VST插件按时发生错误</translation>
    </message>
    <message>
        <source>Failed to create VST reference</source>
        <translation type="vanished">无法创建VST参考</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="131"/>
        <source>Failed to load VST plugin &quot;%1&quot;: %2</source>
        <translation>无法加载VST插件 &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <source>NOTE: You can&apos;t load 32-bit VST plugins into a 64-bit build of Olive. Please find a 64-bit version of this plugin or switch to a 32-bit build of Olive.</source>
        <translation type="vanished">警告: 您不能将32位VST插件加载到64位Olive构建中。请找到这个插件的64位版本或切换到32位的Olive构建版本.</translation>
    </message>
    <message>
        <source>NOTE: You can&apos;t load 64-bit VST plugins into a 32-bit build of Olive. Please find a 32-bit version of this plugin or switch to a 64-bit build of Olive.</source>
        <translation type="vanished">警告: 您不能将64位VST插件加载到32位Olive构建中。请找到这个插件的32位版本或切换到64位的Olive构建版本.</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="147"/>
        <source>Failed to locate entry point for dynamic library.</source>
        <translation>未能找到动态库的入口点.</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="172"/>
        <source>VST Error</source>
        <translation>VST发生错误</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="172"/>
        <source>Plugin&apos;s magic number is invalid</source>
        <translation>插件的幻数无效</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="228"/>
        <source>VST Plugin</source>
        <translation>VST插件</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="254"/>
        <source>Plugin</source>
        <translation>插件</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="258"/>
        <source>Interface</source>
        <translation>用户界面</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="260"/>
        <source>Show</source>
        <translation>显示</translation>
    </message>
</context>
<context>
    <name>Viewer</name>
    <message>
        <location filename="../panels/viewer.cpp" line="603"/>
        <source>(none)</source>
        <translation>(无)</translation>
    </message>
    <message>
        <location filename="../panels/viewer.cpp" line="739"/>
        <source>Drag video only</source>
        <translation>只拖放视频</translation>
    </message>
    <message>
        <location filename="../panels/viewer.cpp" line="746"/>
        <source>Drag audio only</source>
        <translation>只拖放音频</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="981"/>
        <source>Sequence Viewer</source>
        <translation>片段预览</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="982"/>
        <source>Media Viewer</source>
        <translation>媒体预览</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="984"/>
        <source>Notice Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewerWidget</name>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="113"/>
        <source>Save Frame as Image...</source>
        <translation>保存帧为图像...</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="116"/>
        <source>Show Fullscreen</source>
        <translation>全屏模式</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="120"/>
        <source>Disable</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="123"/>
        <source>Screen %1: %2x%3</source>
        <translation>放映 %1: %2x%3</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="131"/>
        <source>Zoom</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="132"/>
        <source>Fit</source>
        <translation>适合</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="142"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="148"/>
        <source>Close Media</source>
        <translation>关闭媒体</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="158"/>
        <source>Save Frame</source>
        <translation>保存帧</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="192"/>
        <source>Viewer Zoom</source>
        <translation>预览缩放</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="193"/>
        <source>Set Custom Zoom Value:</source>
        <translation>设置自己定义缩放:</translation>
    </message>
</context>
<context>
    <name>ViewerWindow</name>
    <message>
        <location filename="../ui/viewerwindow.cpp" line="170"/>
        <source>Exit Fullscreen</source>
        <translation>退出全屏</translation>
    </message>
</context>
<context>
    <name>VoidEffect</name>
    <message>
        <location filename="../effects/internal/voideffect.cpp" line="33"/>
        <source>(unknown)</source>
        <translation>(未知)</translation>
    </message>
    <message>
        <location filename="../effects/internal/voideffect.cpp" line="37"/>
        <source>Missing Effect</source>
        <translation>缺失特效</translation>
    </message>
</context>
<context>
    <name>VolumeEffect</name>
    <message>
        <location filename="../effects/internal/volumeeffect.cpp" line="32"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
</context>
<context>
    <name>transition</name>
    <message>
        <location filename="../effects/transition.cpp" line="117"/>
        <source>Invalid transition</source>
        <translation>无效的转场效果</translation>
    </message>
    <message>
        <location filename="../effects/transition.cpp" line="118"/>
        <source>No candidate for transition &apos;%1&apos;. This transition may be corrupt. Try reinstalling it or Olive.</source>
        <translation>没有适合做转场效果的条件 &apos;%1&apos;. 该效果的插件可能已经损坏. 请尝试重新安装它或者Olive.</translation>
    </message>
</context>
</TS>
