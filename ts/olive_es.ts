<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Olive is a non-linear video editor. This software is free and protected by the GNU GPL.</source>
        <translation type="vanished">Olive es un editor de vídeo no lineal. Esta aplicación es gratuita y está protegida bajo la licencia GNU GPL.</translation>
    </message>
    <message>
        <source>Olive Team is obliged to inform users that Olive source code is available for download from its website.</source>
        <translation type="vanished">El equipo de Olive está obligado a informar a los usuarios que el código fuente de la aplicación esta disponible para su descarga desde su sitio web.</translation>
    </message>
    <message>
        <location filename="../dialogs/aboutdialog.cpp" line="49"/>
        <source>KunGang is a non-linear video editor. This software is free and protected by the GNU GPL. Its development based on Olive(https://www.olivevideoeditor.org)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/aboutdialog.cpp" line="51"/>
        <source>KunGang Team is obliged to inform users that KunGang source code is available for download from its website.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ActionSearch</name>
    <message>
        <location filename="../dialogs/actionsearch.cpp" line="57"/>
        <source>Search for action...</source>
        <translation>Búsqueda de acción...</translation>
    </message>
</context>
<context>
    <name>AdvancedVideoDialog</name>
    <message>
        <location filename="../dialogs/advancedvideodialog.cpp" line="41"/>
        <source>Advanced Video Settings</source>
        <translation>Configuraciones Avanzadas de Vídeo</translation>
    </message>
    <message>
        <location filename="../dialogs/advancedvideodialog.cpp" line="53"/>
        <source>Pixel Format:</source>
        <translation>Formato de Píxel:</translation>
    </message>
    <message>
        <location filename="../dialogs/advancedvideodialog.cpp" line="77"/>
        <source>Threads:</source>
        <translation>Hilos (Threads):</translation>
    </message>
</context>
<context>
    <name>Audio</name>
    <message>
        <location filename="../rendering/audio.cpp" line="333"/>
        <source>%1 Audio</source>
        <translation>%1 Audio</translation>
    </message>
    <message>
        <location filename="../rendering/audio.cpp" line="346"/>
        <source>Recording %1</source>
        <translation>Grabación %1</translation>
    </message>
</context>
<context>
    <name>AudioNoiseEffect</name>
    <message>
        <location filename="../effects/internal/audionoiseeffect.cpp" line="24"/>
        <source>Amount</source>
        <translation>Cantidad</translation>
    </message>
    <message>
        <location filename="../effects/internal/audionoiseeffect.cpp" line="30"/>
        <source>Mix</source>
        <translation>Mezclar</translation>
    </message>
    <message>
        <source>Noise</source>
        <translation type="vanished">Ruido</translation>
    </message>
    <message>
        <source>Generate audio noise that can be mixed with this clip.</source>
        <translation type="vanished">Generar ruido de audio que se puede mezclar con este clip.</translation>
    </message>
</context>
<context>
    <name>AutoCutSilenceDialog</name>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="38"/>
        <source>Cut Silence</source>
        <translation>Corte de silencio</translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="44"/>
        <source>Attack Threshold:</source>
        <translation>Umbral de ataque:</translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="49"/>
        <source>Attack Time:</source>
        <translation>Tiempo de ataque:</translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="54"/>
        <source>Release Threshold:</source>
        <translation>Umbral de liberación:</translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="59"/>
        <source>Release Time:</source>
        <translation>Tiempo de liberación:</translation>
    </message>
</context>
<context>
    <name>Cacher</name>
    <message>
        <location filename="../rendering/cacher.cpp" line="922"/>
        <location filename="../rendering/cacher.cpp" line="931"/>
        <source>Could not open %1 - %2</source>
        <translation>No se pudo abrir %1 - %2</translation>
    </message>
</context>
<context>
    <name>ChannelLayoutName</name>
    <message>
        <location filename="../project/media.cpp" line="53"/>
        <source>Invalid</source>
        <translation>Inválido</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="54"/>
        <source>Mono</source>
        <translation>Monoaural</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="55"/>
        <source>Stereo</source>
        <translation>Estéreo</translation>
    </message>
</context>
<context>
    <name>ClipPropertiesDialog</name>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="14"/>
        <source>&quot;%1&quot; Properties</source>
        <translation>&quot;%1&quot; Propiedades</translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="15"/>
        <source>Multiple Clip Properties</source>
        <translation>Propiedades de múltiples clips</translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="24"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="33"/>
        <source>Duration:</source>
        <translation>Duración:</translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="71"/>
        <source>(multiple)</source>
        <translation>(múltiple)</translation>
    </message>
</context>
<context>
    <name>CollapsibleWidget</name>
    <message>
        <location filename="../ui/collapsiblewidget.cpp" line="54"/>
        <source>&lt;untitled&gt;</source>
        <translation>&lt;SinTítulo&gt;</translation>
    </message>
</context>
<context>
    <name>ColorButton</name>
    <message>
        <location filename="../ui/colorbutton.cpp" line="47"/>
        <source>Set Color</source>
        <translation>Establecer color</translation>
    </message>
</context>
<context>
    <name>CornerPinEffect</name>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="30"/>
        <source>Top Left</source>
        <translation>Arriba Izquierda</translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="34"/>
        <source>Top Right</source>
        <translation>Arriba Derecha</translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="38"/>
        <source>Bottom Left</source>
        <translation>Abajo Izquierda</translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="42"/>
        <source>Bottom Right</source>
        <translation>Abajo Derecha</translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="46"/>
        <source>Perspective</source>
        <translation>Perspectiva</translation>
    </message>
    <message>
        <source>Corner Pin</source>
        <translation type="vanished">Fijar Esquinas Para Deformar (Corner Pin)</translation>
    </message>
    <message>
        <source>Distort</source>
        <translation type="vanished">Distorsionar</translation>
    </message>
    <message>
        <source>Distort/warp this clip by pinning each of its four corners.</source>
        <translation type="vanished">Distorsionar/deformar este clip fijando cada una de sus cuatro esquinas.</translation>
    </message>
</context>
<context>
    <name>CrashDialog</name>
    <message>
        <source>We&apos;re very sorry, Olive has crashed. Please send the following data to developers:</source>
        <translation type="vanished">Lo sentimos mucho, se ha producido un error en la aplicación. Por favor envíe los siguientes datos a los desarrolladores de Olive para falcilitar la resolución del problema, gracias:</translation>
    </message>
</context>
<context>
    <name>CrossDissolveTransition</name>
    <message>
        <source>Cross Dissolve</source>
        <translation type="vanished">Fundido Cruzado</translation>
    </message>
    <message>
        <source>Dissolves</source>
        <translation type="vanished">Fundido</translation>
    </message>
    <message>
        <source>Dissolve clips evenly.</source>
        <translation type="vanished">Fundir uniformemente los clips.</translation>
    </message>
</context>
<context>
    <name>DebugDialog</name>
    <message>
        <location filename="../dialogs/debugdialog.cpp" line="44"/>
        <source>Debug Log</source>
        <translation>Registro de depuración</translation>
    </message>
</context>
<context>
    <name>DemoNotice</name>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="30"/>
        <location filename="../dialogs/demonotice.cpp" line="45"/>
        <source>Welcome to Olive!</source>
        <translation>¡Bienvenido a Olive Video Editor!</translation>
    </message>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="47"/>
        <source>Olive is a free open-source video editor released under the GNU GPL. If you have paid for this software, you have been scammed.</source>
        <translation>Olive es un editor de video gratuito de código abierto lanzado bajo la licencia GPL de GNU. Si ha pagado por este software, ha sido estafado.</translation>
    </message>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="49"/>
        <source>This software is currently in ALPHA which means it is unstable and very likely to crash, have bugs, and have missing features. We offer no warranty so use at your own risk. Please report any bugs or feature requests at %1</source>
        <translation>Este software se encuentra actualmente en desarrollo y es una versión ALPHA, lo que significa que es inestable y es muy probable que se bloquee, tenga errores y carezca de algunas características. No podemos ofrecerle ninguna garantía, así que úselo bajo su propia responsabilidad. Por favor, informe de cualquier error y no dude en notificarnos características que le gustaría que se incluyan en la aplicación en la siguiente web  %1</translation>
    </message>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="51"/>
        <source>Thank you for trying Olive and we hope you enjoy it!</source>
        <translation>¡Gracias por probar Olive Vídeo Editor, esperamos que lo disfrutes!</translation>
    </message>
</context>
<context>
    <name>Effect</name>
    <message>
        <location filename="../effects/effect.cpp" line="100"/>
        <source>Invalid effect</source>
        <translation>Efecto no válido</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="101"/>
        <source>No candidate for effect &apos;%1&apos;. This effect may be corrupt. Try reinstalling it or Olive.</source>
        <translation>Ningún candidato para el efecto &apos;%1&apos;. Este efecto parece estar corrupto. Pruebe a reinstalarlo o reinstale Olive.</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="448"/>
        <source>Save Effect Settings</source>
        <translation>Guardar los ajustes del efecto</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="450"/>
        <location filename="../effects/effect.cpp" line="480"/>
        <source>Effect XML Settings %1</source>
        <translation>Ajustes XML del efecto %1</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="468"/>
        <source>Save Settings Failed</source>
        <translation>Falló guardar los ajustes</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="469"/>
        <source>Failed to open &quot;%1&quot; for writing.</source>
        <translation>Falló la apertura &quot;%1&quot; para escritura.</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="478"/>
        <source>Load Effect Settings</source>
        <translation>Cargar los ajuste del efecto</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="494"/>
        <location filename="../effects/effect.cpp" line="682"/>
        <source>Load Settings Failed</source>
        <translation>Falló la carga de los ajustes</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="495"/>
        <source>Failed to open &quot;%1&quot; for reading.</source>
        <translation>Falló la apertura &quot;%1&quot; para lectura.</translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="683"/>
        <source>This settings file doesn&apos;t match this effect.</source>
        <translation>Estos ajustes no son para este efecto.</translation>
    </message>
</context>
<context>
    <name>EffectControls</name>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="509"/>
        <source>Effects: </source>
        <translation>Efectos:</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="326"/>
        <source>(none)</source>
        <translation>(ninguno)</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="511"/>
        <source>Add Video Effect</source>
        <translation>Añadir Efecto de Vídeo</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="512"/>
        <source>VIDEO EFFECTS</source>
        <translation>EFECTOS DE VÍDEO</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="513"/>
        <source>Add Video Transition</source>
        <translation>Añadir Transición de Vídeo</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="514"/>
        <source>Add Audio Effect</source>
        <translation>Añadir Efecto de Audio</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="515"/>
        <source>AUDIO EFFECTS</source>
        <translation>EFECTOS DE AUDIO</translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="516"/>
        <source>Add Audio Transition</source>
        <translation>Añadir Transición de Audio</translation>
    </message>
</context>
<context>
    <name>EffectRow</name>
    <message>
        <location filename="../effects/effectrow.cpp" line="104"/>
        <source>Disable Keyframes</source>
        <translation>Desactivar fotogramas clave</translation>
    </message>
    <message>
        <location filename="../effects/effectrow.cpp" line="105"/>
        <source>Disabling keyframes will delete all current keyframes. Are you sure you want to do this?</source>
        <translation>¡Desconectar los fotogramas clave los eliminará! ¿Relamente los quieres eliminar?</translation>
    </message>
</context>
<context>
    <name>EffectUI</name>
    <message>
        <location filename="../ui/effectui.cpp" line="54"/>
        <source>%1 (Opening)</source>
        <translation>%1 (Abriendo)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="56"/>
        <source>%1 (Closing)</source>
        <translation>%1 (Cerrando)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="158"/>
        <source>%1 (multiple)</source>
        <translation>%1 (múltiple)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="286"/>
        <source>Cu&amp;t</source>
        <translation>Cor&amp;tar</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="289"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copiar</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="300"/>
        <source>Move &amp;Up</source>
        <translation>Mover Arriba (&amp;Up)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="304"/>
        <source>Move &amp;Down</source>
        <translation>Mover Abajo (&amp;Down)</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="309"/>
        <source>D&amp;elete</source>
        <translation>&amp;Eliminar</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="326"/>
        <source>Load Settings From File</source>
        <translation>Cargar configuración predefinida desde un archivo</translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="328"/>
        <source>Save Settings to File</source>
        <translation>Guardar configuración predefinida en un archivo</translation>
    </message>
</context>
<context>
    <name>EmbeddedFileChooser</name>
    <message>
        <location filename="../ui/embeddedfilechooser.cpp" line="52"/>
        <source>File:</source>
        <translation>Archivo:</translation>
    </message>
</context>
<context>
    <name>ExponentialFadeTransition</name>
    <message>
        <source>Exponential Fade</source>
        <translation type="vanished">Desvanecimiento exponencial</translation>
    </message>
    <message>
        <source>An exponential audio fade that starts slow and ends fast.</source>
        <translation type="vanished">Desvanecimiento de audio exponencial, comienza lento y termina rápido.</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="74"/>
        <source>Export &quot;%1&quot;</source>
        <translation>Exportar &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="127"/>
        <source>Unknown codec name %1</source>
        <translation>Nombre de códec desconocido %1</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="341"/>
        <source>Export Failed</source>
        <translation>La exportación ha fallado</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="342"/>
        <source>Export failed - %1</source>
        <translation>La exportación ha fallado - %1</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="384"/>
        <source>Invalid dimensions</source>
        <translation>Dimensiones no válidas</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="385"/>
        <source>Export width and height must both be even numbers/divisible by 2.</source>
        <translation>El ancho y el alto de la exportación deben ser números pares/divisibles entre 2.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="441"/>
        <source>Invalid codec</source>
        <translation>Códec no valido</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="442"/>
        <source>Couldn&apos;t determine output parameters for the selected codec. This is a bug, please contact the developers.</source>
        <translation>No se pudieron determinar los parámetros de salida para el códec seleccionado. Si esto es un error, por favor, póngase en contacto con los desarrolladores.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="511"/>
        <source>Invalid format</source>
        <translation>Formato no válido</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="512"/>
        <source>Couldn&apos;t determine output format. This is a bug, please contact the developers.</source>
        <translation>No se pudo determinar el formato de salida. Si esto es un error, por favor, póngase en contacto con los desarrolladores.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="519"/>
        <source>Export Media</source>
        <translation>Exportar Medios</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="603"/>
        <source>%p% (Total: %1:%2:%3)</source>
        <translation>%p% (Total: %1:%2:%3)</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="608"/>
        <source>%p% (ETA: %1:%2:%3)</source>
        <translation>%p% (ETA: %1:%2:%3)</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="623"/>
        <source>Quality-based (Constant Rate Factor)</source>
        <translation>Basado en la Calidad de Factor de Ratio Constante</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="627"/>
        <source>Constant Bitrate</source>
        <translation>Velocidad de bits constante</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="636"/>
        <location filename="../dialogs/exportdialog.cpp" line="642"/>
        <source>Invalid Codec</source>
        <translation>Códec no valido</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="637"/>
        <source>Failed to find a suitable encoder for this codec. Export will likely fail.</source>
        <translation>Error al encontrar un codificador adecuado para este códec. La exportación probablemente fallará.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="643"/>
        <source>Failed to find pixel format for this encoder. Export will likely fail.</source>
        <translation>Error al encontrar el formato de píxel adecuado para este codificador. La exportación probablemente fallará.</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="656"/>
        <source>Bitrate (Mbps):</source>
        <translation>Velocidad de Bits (Mbps):</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="660"/>
        <source>Quality (CRF):</source>
        <translation>Calidad (CRF):</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="663"/>
        <source>Quality Factor:

0 = lossless
17-18 = visually lossless (compressed, but unnoticeable)
23 = high quality
51 = lowest quality possible</source>
        <translation>Factor de Calidad:

0 = Sin pérdida, sin compresión. (La mejor calidad pero mayor tamaño de archivo)
17-18 = Muy alta calidad, sin pérdida visual. (RECOMENDADO) (Comprimido, pero de manera imperceptible.)
23 = Alta calidad (Recomendado en la mayoría de casos para mantener una buena relación calidad tamaño)
51 = La peor calidad posible (No se recomienda salvo excepciónes donde sea más importante el menor tamaño de archivo que la calidad del vídeo)</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="666"/>
        <source>Target File Size (MB):</source>
        <translation>Tamaño del archivo de destino (MB):</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="682"/>
        <source>Format:</source>
        <translation>Formato:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="691"/>
        <source>Range:</source>
        <translation>Rango:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="694"/>
        <source>Entire Sequence</source>
        <translation>Secuencia entera</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="695"/>
        <source>In to Out</source>
        <translation>De entrada a salida</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="702"/>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="708"/>
        <location filename="../dialogs/exportdialog.cpp" line="751"/>
        <source>Codec:</source>
        <translation>Codificación (Códec):</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="712"/>
        <source>Width:</source>
        <translation>Ancho:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="717"/>
        <source>Height:</source>
        <translation>Alto:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="722"/>
        <source>Frame Rate:</source>
        <translation>Fotogramas por segundo:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="728"/>
        <source>Compression Type:</source>
        <translation>Tipo de Compresión:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="739"/>
        <source>Advanced</source>
        <translation>Avanzado</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="746"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="755"/>
        <source>Sampling Rate:</source>
        <translation>Tasa de muestreo:</translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="761"/>
        <source>Bitrate (Kbps/CBR):</source>
        <translation>Velocidad de bits (Kbps / CBR):</translation>
    </message>
</context>
<context>
    <name>ExportThread</name>
    <message>
        <location filename="../rendering/exportthread.cpp" line="79"/>
        <source>failed to send frame to encoder (%1)</source>
        <translation>Error al enviar los fotogramas al codificador (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="90"/>
        <source>failed to receive packet from encoder (%1)</source>
        <translation>Error al recibir el paquete del codificador (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="113"/>
        <source>could not video encoder for %1</source>
        <translation>No se ha podido codificar el vídeo para %1</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="122"/>
        <source>could not allocate video stream</source>
        <translation>no se pudo asignar el flujo de video</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="131"/>
        <source>could not allocate video encoding context</source>
        <translation>no se pudo asignar el flujo de video</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="180"/>
        <source>could not open output video encoder (%1)</source>
        <translation>no se pudo abrir el codificador para el vídeo de salida (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="188"/>
        <source>could not copy video encoder parameters to output stream (%1)</source>
        <translation>no se pudieron copiar los parámetros del codificador de video para este flujo de salida (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="227"/>
        <source>could not audio encoder for %1</source>
        <translation>no se pudo codificar el audio para %1</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="235"/>
        <source>could not allocate audio stream</source>
        <translation>no se pudo asignar el flujo de audio</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="249"/>
        <source>could not allocate audio encoding context</source>
        <translation>no se pudo asignar el contexto de codificación de audio</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="274"/>
        <source>could not open output audio encoder (%1)</source>
        <translation>no se pudo abrir el codificador de audio de salida (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="282"/>
        <source>could not copy audio encoder parameters to output stream (%1)</source>
        <translation>no se pudieron copiar los parámetros del codificador de audio al flujo de salida (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="319"/>
        <source>could not allocate audio buffer (%1)</source>
        <translation>no se pudo asignar el búfer de audio (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="350"/>
        <source>could not create output format context</source>
        <translation>no se pudo crear el contexto del formato de salida</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="360"/>
        <source>could not open output file (%1)</source>
        <translation>no se pudo abrir el archvo de salida (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="396"/>
        <source>could not write output file header (%1)</source>
        <translation>no se pudo escribir la cabecera del archivo de salida (%1)</translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="600"/>
        <source>could not write output file trailer (%1)</source>
        <translation>no se pudo escribir el final del archivo de salida (%1)</translation>
    </message>
</context>
<context>
    <name>FFmpegDecoder</name>
    <message>
        <source>Failed to find appropriate decoder for this codec (%1 :: %2)</source>
        <translation type="vanished">No se pudo encontrar un decodificador adecuado para este códec (%1 :: %2)</translation>
    </message>
    <message>
        <source>Failed to allocate codec context (%1 :: %2)</source>
        <translation type="vanished">Error al asignar el contexto del códec (%1 :: %2)</translation>
    </message>
    <message>
        <source>Error decoding %1 - %2 %3</source>
        <translation type="vanished">Error al decodificar %1 - %2 %3</translation>
    </message>
</context>
<context>
    <name>FillLeftRightEffect</name>
    <message>
        <location filename="../effects/internal/fillleftrighteffect.cpp" line="27"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../effects/internal/fillleftrighteffect.cpp" line="29"/>
        <source>Fill Left with Right</source>
        <translation>Rellena a la izquierda con la derecha</translation>
    </message>
    <message>
        <location filename="../effects/internal/fillleftrighteffect.cpp" line="30"/>
        <source>Fill Right with Left</source>
        <translation>Rellena a la derecha con la izquierda</translation>
    </message>
    <message>
        <source>Fill Left/Right</source>
        <translation type="vanished">Rellenar Izquierda/Derecha</translation>
    </message>
    <message>
        <source>Replaces either the left or right channel with the other</source>
        <translation type="vanished">Reemplaza el canal izquierdo o derecho con el otro</translation>
    </message>
</context>
<context>
    <name>Frei0rEffect</name>
    <message>
        <location filename="../effects/internal/frei0reffect.cpp" line="55"/>
        <source>Failed to load Frei0r plugin &quot;%1&quot;: %2</source>
        <translation>Falló la carga del plugin Frei0r &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location filename="../effects/internal/frei0reffect.cpp" line="54"/>
        <source>Error loading Frei0r plugin</source>
        <translation>Error al cargar el plugin Frei0r</translation>
    </message>
</context>
<context>
    <name>GraphEditor</name>
    <message>
        <location filename="../panels/grapheditor.cpp" line="140"/>
        <source>Graph Editor</source>
        <translation>Editor Gráfico</translation>
    </message>
    <message>
        <location filename="../panels/grapheditor.cpp" line="141"/>
        <source>Linear</source>
        <translation>Lineal</translation>
    </message>
    <message>
        <location filename="../panels/grapheditor.cpp" line="142"/>
        <source>Bezier</source>
        <translation>Bézier</translation>
    </message>
    <message>
        <location filename="../panels/grapheditor.cpp" line="143"/>
        <source>Hold</source>
        <translation>Mantener</translation>
    </message>
</context>
<context>
    <name>GraphView</name>
    <message>
        <location filename="../ui/graphview.cpp" line="80"/>
        <source>Zoom to Selection</source>
        <translation>Ampliar a la selección</translation>
    </message>
    <message>
        <location filename="../ui/graphview.cpp" line="87"/>
        <source>Zoom to Show All</source>
        <translation>Mostrar todo</translation>
    </message>
    <message>
        <location filename="../ui/graphview.cpp" line="96"/>
        <source>Reset View</source>
        <translation>Resetear vista</translation>
    </message>
</context>
<context>
    <name>InterlacingName</name>
    <message>
        <location filename="../project/media.cpp" line="44"/>
        <source>None (Progressive)</source>
        <translation>Ninguno (Progresivo)</translation>
    </message>
    <message>
        <source>Upper Field First</source>
        <translation type="vanished">Campo superior primero</translation>
    </message>
    <message>
        <source>Lower Field First</source>
        <translation type="vanished">Campo inferior primero</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="47"/>
        <source>Invalid</source>
        <translation>No válido</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="45"/>
        <source>Top Field First</source>
        <translation>Campo de arriba primero</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="46"/>
        <source>Bottom Field First</source>
        <translation>Campo de abajo primero</translation>
    </message>
</context>
<context>
    <name>KeyframeNavigator</name>
    <message>
        <location filename="../ui/keyframenavigator.cpp" line="77"/>
        <source>Enable Keyframes</source>
        <translation>Habilitar fotogramas clave</translation>
    </message>
</context>
<context>
    <name>KeyframeView</name>
    <message>
        <location filename="../ui/keyframeview.cpp" line="74"/>
        <source>Linear</source>
        <translation>Lineal</translation>
    </message>
    <message>
        <location filename="../ui/keyframeview.cpp" line="76"/>
        <source>Bezier</source>
        <translation>Bézier</translation>
    </message>
    <message>
        <location filename="../ui/keyframeview.cpp" line="78"/>
        <source>Hold</source>
        <translation>Mantener</translation>
    </message>
</context>
<context>
    <name>LabelSlider</name>
    <message>
        <location filename="../ui/labelslider.cpp" line="271"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="../ui/labelslider.cpp" line="275"/>
        <source>&amp;Reset to Default</source>
        <translation>&amp;Restablecer a Predeterminados</translation>
    </message>
    <message>
        <location filename="../ui/labelslider.cpp" line="306"/>
        <location filename="../ui/labelslider.cpp" line="345"/>
        <source>Set Value</source>
        <translation>Establecer Valor</translation>
    </message>
    <message>
        <location filename="../ui/labelslider.cpp" line="307"/>
        <location filename="../ui/labelslider.cpp" line="346"/>
        <source>New value:</source>
        <translation>Nuevo Valor:</translation>
    </message>
</context>
<context>
    <name>LinearFadeTransition</name>
    <message>
        <source>Linear Fade</source>
        <translation type="vanished">Fundido Lineal</translation>
    </message>
    <message>
        <source>An linear audio fade that fades evenly at a constant rate.</source>
        <translation type="vanished">Desvanecimiento lineal del audio a una velocidad constante.</translation>
    </message>
</context>
<context>
    <name>LoadDialog</name>
    <message>
        <location filename="../dialogs/loaddialog.cpp" line="37"/>
        <source>Loading...</source>
        <translation>Cargando...</translation>
    </message>
    <message>
        <location filename="../dialogs/loaddialog.cpp" line="42"/>
        <source>Loading &apos;%1&apos;...</source>
        <translation>Cargando &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../dialogs/loaddialog.cpp" line="48"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>LoadThread</name>
    <message>
        <location filename="../project/loadthread.cpp" line="246"/>
        <source>Version Mismatch</source>
        <translation>La versión no coincide</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="247"/>
        <source>This project was saved in a different version of Olive and may not be fully compatible with this version. Would you like to attempt loading it anyway?</source>
        <translation>Este proyecto se guardó en una versión diferente de Olive y puede que no sea totalmente compatible con esta versión ¿Deseas intentar abrirlo de todos modos?</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="694"/>
        <source>%1 - Line: %2 Col: %3</source>
        <translation>%1 - Línea: %2 Col: %3</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="721"/>
        <source>User aborted loading</source>
        <translation>Carga cancelada por el usuario</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="752"/>
        <source>XML Parsing Error</source>
        <translation>Error de análisis XML</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="753"/>
        <source>Couldn&apos;t load &apos;%1&apos;. %2</source>
        <translation>No se pudo cargar &apos;%1&apos;. %2</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="757"/>
        <source>Project Load Error</source>
        <translation>La carga del proyecto falló</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="758"/>
        <source>Error loading project: %1</source>
        <translation>Error al cargar el proyecto: %1</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="570"/>
        <source>Invalid Clip Link</source>
        <translation>Enlace al clip inválido</translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="571"/>
        <source>This project contains an invalid clip link. It may be corrupt. Would you like to continue loading it?</source>
        <translation>Este proyecto contiene un enlace de clip no válido. Puede haberse movido o estar corrupto. ¿Te gustaría seguir cargándolo?</translation>
    </message>
</context>
<context>
    <name>LogarithmicFadeTransition</name>
    <message>
        <source>Logarithmic Fade</source>
        <translation type="vanished">Desvanecimiento logarítmico</translation>
    </message>
    <message>
        <source>An logarithmic audio fade that starts fast and ends slow.</source>
        <translation type="vanished">Un desvanecimiento de audio logarítmico que comienza rápido y termina lentamente.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="277"/>
        <source>Welcome to %1</source>
        <translation>Bienvenido a %1</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="825"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="826"/>
        <source>&amp;New</source>
        <translation>&amp;Nuevo</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="827"/>
        <source>&amp;Open Project</source>
        <translation>&amp;Abrir Proyecto</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="828"/>
        <source>Clear Recent List</source>
        <translation>Limpiar lista de recientes</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="829"/>
        <source>Open Recent</source>
        <translation>Abrir Recientes</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="830"/>
        <source>&amp;Save Project</source>
        <translation>&amp;Guardar Proyecto</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="831"/>
        <source>Save Project &amp;As</source>
        <translation>G&amp;uardar Proyecto Como</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="832"/>
        <source>&amp;Import...</source>
        <translation>&amp;Importar...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="833"/>
        <source>&amp;Export...</source>
        <translation>&amp;Exportar...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="834"/>
        <source>E&amp;xit</source>
        <translation>&amp;Cerrar la aplicación</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="836"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="838"/>
        <source>&amp;Undo</source>
        <translation>Deshacer Cambios (&amp;Undo)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="839"/>
        <source>Redo</source>
        <translation>Rehacer Cambios</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="841"/>
        <source>Select &amp;All</source>
        <translation>Seleccion&amp;ar Todo</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="842"/>
        <source>Deselect All</source>
        <translation>Deseleccionar Todo</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="844"/>
        <source>Ripple to In Point</source>
        <translation>Extraer desde el punto de inicio del clip</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="845"/>
        <source>Ripple to Out Point</source>
        <translation>Extraer desde el punto final del clip</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="846"/>
        <source>Edit to In Point</source>
        <translation>Eliminar desde el punto de inicio del clip</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="847"/>
        <source>Edit to Out Point</source>
        <translation>Eliminar desde el punto final del clip</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="848"/>
        <source>Delete In/Out Point</source>
        <translation>Eliminar lo comprendido entre los puntos de Entrada/Salida</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="859"/>
        <source>Track Lines</source>
        <translation>Ver líneas de las pistas</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="849"/>
        <source>Ripple Delete In/Out Point</source>
        <translation>Extraer lo comprendido entre los puntos de Entrada/Salida</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="851"/>
        <source>Set/Edit Marker</source>
        <translation>Establecer/Editar Marcador</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="853"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="854"/>
        <source>Zoom In</source>
        <translation>Ampliar</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="855"/>
        <source>Zoom Out</source>
        <translation>Reducir</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="856"/>
        <source>Increase Track Height</source>
        <translation>Aumentar la altura de la pista</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="857"/>
        <source>Decrease Track Height</source>
        <translation>Disminuir la altura de la pista</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="858"/>
        <source>Toggle Show All</source>
        <translation>Alternar Mostrar Todo</translation>
    </message>
    <message>
        <source>OpenColorIO Config Error</source>
        <translation type="vanished">Error de configuración de OpenColorIO</translation>
    </message>
    <message>
        <source>Failed to set OpenColorIO configuration: %1</source>
        <translation type="vanished">Error al establecer la configuración de OpenColorIO: %1</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="860"/>
        <source>Rectified Waveforms</source>
        <translation>Ondas de audio recortadas</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="861"/>
        <source>Frames</source>
        <translation>Fotogramas</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="862"/>
        <source>Drop Frame</source>
        <translation>Descartar fotograma (Drop Frame)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="863"/>
        <source>Non-Drop Frame</source>
        <translation>No descartar fotograma (Non-Drop Frame)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="864"/>
        <source>Milliseconds</source>
        <translation>Milisegundos</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="866"/>
        <source>Title/Action Safe Area</source>
        <translation>Area segura para Titulos y Acción</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="867"/>
        <source>Off</source>
        <translation>Apagado</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="868"/>
        <source>Default</source>
        <translation>Por defecto</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="869"/>
        <source>4:3</source>
        <translation>4:3</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="870"/>
        <source>16:9</source>
        <translation>16:9</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="871"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="873"/>
        <source>Full Screen</source>
        <translation>Pantalla completa</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="874"/>
        <source>Full Screen Viewer</source>
        <translation>Visor a pantalla completa</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="876"/>
        <source>&amp;Playback</source>
        <translation>&amp;Reproducción</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="877"/>
        <source>Go to Start</source>
        <translation>Ir al inicio</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="878"/>
        <source>Previous Frame</source>
        <translation>Fotograma anterior</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="879"/>
        <source>Play/Pause</source>
        <translation>Reproducir/Pausar</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="880"/>
        <source>Play In to Out</source>
        <translation>Reproducir desde la marca de entrada a la marca de salida</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="881"/>
        <source>Next Frame</source>
        <translation>Siguiente fotograma</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="882"/>
        <source>Go to End</source>
        <translation>Ir al final</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="884"/>
        <source>Go to Previous Cut</source>
        <translation>Ir al corte anterior</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="885"/>
        <source>Go to Next Cut</source>
        <translation>Ir al siguiente corte</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="886"/>
        <source>Go to In Point</source>
        <translation>Ir al punto de entrada</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="887"/>
        <source>Go to Out Point</source>
        <translation>Ir al punto de salida</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="889"/>
        <source>Shuttle Left</source>
        <translation>Reproducir hacia la Izquierda (Inversa)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="890"/>
        <source>Shuttle Stop</source>
        <translation>Parar la Reproducción</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="891"/>
        <source>Shuttle Right</source>
        <translation>Reproducir hacia la Derecha (Normal)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="893"/>
        <source>Loop</source>
        <translation>Bucle (Loop)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="895"/>
        <source>&amp;Window</source>
        <translation>Ve&amp;ntana</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="897"/>
        <source>Project</source>
        <translation>Proyecto</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="898"/>
        <source>Effect Controls</source>
        <translation>Controles de efectos</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="899"/>
        <source>Timeline</source>
        <translation>Línea de Tiempo</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="900"/>
        <source>Graph Editor</source>
        <translation>Editor Gráfico</translation>
    </message>
    <message>
        <source>Node Editor</source>
        <translation type="vanished">Editor de Nodos</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="901"/>
        <source>Media Viewer</source>
        <translation>Visor de Medios</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="902"/>
        <source>Sequence Viewer</source>
        <translation>Visor de Secuencias</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="904"/>
        <source>Maximize Panel</source>
        <translation>Maximizar Panel</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="905"/>
        <source>Lock Panels</source>
        <translation>Bloquear Paneles</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="906"/>
        <source>Reset to Default Layout</source>
        <translation>Restaurar valores por defecto de la interfaz</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="908"/>
        <source>&amp;Tools</source>
        <translation>&amp;Herramientas</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="910"/>
        <source>Pointer Tool</source>
        <translation>Puntero de Selección/Edición/Mover Clips</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="911"/>
        <source>Edit Tool</source>
        <translation>Herramienta de Selección</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="912"/>
        <source>Ripple Tool</source>
        <translation>Herramienta para Enrrollar/Desenrrollar</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="913"/>
        <source>Razor Tool</source>
        <translation>Herramienta de Corte</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="914"/>
        <source>Slip Tool</source>
        <translation>Deslizar clip sin desplazar</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="915"/>
        <source>Slide Tool</source>
        <translation>Desplazar clip afectando a los clips contiguos</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="916"/>
        <source>Hand Tool</source>
        <translation>Mano para ajustar la vista (No afecta a la edición)</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="917"/>
        <source>Transition Tool</source>
        <translation>Herramienta para Inserción de Transiciones</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="918"/>
        <source>Enable Snapping</source>
        <translation>Habilitar Imán de Ajuste</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="919"/>
        <source>Auto-Cut Silence</source>
        <translation>Auto Cortar en los Silencios</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="921"/>
        <source>No Auto-Scroll</source>
        <translation>Sin desplazamiento automático</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="922"/>
        <source>Page Auto-Scroll</source>
        <translation>Desplazamiento automático de páginas</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="923"/>
        <source>Smooth Auto-Scroll</source>
        <translation>Desplazamiento automático suave</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="925"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="927"/>
        <source>Clear Undo</source>
        <translation>Limpiar historial de Deshacer</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="930"/>
        <source>&amp;Help</source>
        <translation>A&amp;yuda</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="932"/>
        <source>A&amp;ction Search</source>
        <translation>&amp;Buscar</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="933"/>
        <source>Debug Log</source>
        <translation>Registro de depuración</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="934"/>
        <source>&amp;About...</source>
        <translation>&amp;Acerca de...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="952"/>
        <source>&lt;untitled&gt;</source>
        <translation>&lt;SinTítulo&gt;</translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="../timeline/marker.cpp" line="64"/>
        <source>Set Marker</source>
        <translation>Establecer Marca</translation>
    </message>
    <message>
        <location filename="../timeline/marker.cpp" line="66"/>
        <source>Set clip marker name:</source>
        <translation>Establecer el nombre del marcador de clip:</translation>
    </message>
    <message>
        <location filename="../timeline/marker.cpp" line="67"/>
        <source>Set sequence marker name:</source>
        <translation>Establecer el nombre del marcador de secuencia:</translation>
    </message>
</context>
<context>
    <name>Media</name>
    <message>
        <location filename="../project/media.cpp" line="94"/>
        <source>New Folder</source>
        <translation>Nueva Carpeta</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="119"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="119"/>
        <source>Filename:</source>
        <translation>Nombre de Archivo:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="123"/>
        <source>Video Dimensions:</source>
        <translation>Dimensiones de Vídeo:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="133"/>
        <source>Frame Rate:</source>
        <translation>Fotogramas por Segundo:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="143"/>
        <source>%1 field(s) (%2 frame(s))</source>
        <translation>%1 Campo(s) (%2 Fotograma(s))</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="152"/>
        <source>Interlacing:</source>
        <translation>Entrelazado:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="164"/>
        <source>Audio Frequency:</source>
        <translation>Frecuencia del Audio:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="173"/>
        <source>Audio Channels:</source>
        <translation>Canales de Audio:</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="191"/>
        <source>Name: %1
Video Dimensions: %2x%3
Frame Rate: %4
Audio Frequency: %5
Audio Layout: %6</source>
        <translation>Nombre: %1
Dimensiones del Vídeo: %2x%3
Fotogramas por Segundo: %4
Frecuencia del Audio: %5
Audio: %6</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="322"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="324"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="328"/>
        <source>Rate</source>
        <translation>Velocidad</translation>
    </message>
</context>
<context>
    <name>MediaPropertiesDialog</name>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="44"/>
        <source>&quot;%1&quot; Properties</source>
        <translation>&quot;%1&quot; Propiedades</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="53"/>
        <source>Tracks:</source>
        <translation>Pistas:</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="61"/>
        <source>Video %1: %2x%3 %4FPS</source>
        <translation>Vídeo %1: %2x%3 %4FPS</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="77"/>
        <source>Audio %1: %2Hz %3</source>
        <translation>Audio %1: %2Hz %3</translation>
    </message>
    <message numerus="yes">
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="80"/>
        <source>%n channel(s)</source>
        <translation>
            <numerusform>%n canal</numerusform>
            <numerusform>%n canales</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="95"/>
        <source>Conform to Frame Rate:</source>
        <translation>Conforme a la velocidad de fotogramas:</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="105"/>
        <source>Alpha is Premultiplied</source>
        <translation>Canal Alfa Premultiplicado</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="114"/>
        <source>Auto (%1)</source>
        <translation>Automático (%1)</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="127"/>
        <source>Interlacing:</source>
        <translation>Entrelazado:</translation>
    </message>
    <message>
        <source>Color Space:</source>
        <translation type="vanished">Espacio de color:</translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="134"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
</context>
<context>
    <name>MenuHelper</name>
    <message>
        <location filename="../ui/menuhelper.cpp" line="164"/>
        <source>&amp;Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="165"/>
        <source>&amp;Project</source>
        <translation>&amp;Proyecto</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="166"/>
        <source>&amp;Sequence</source>
        <translation>&amp;Sequencia</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="167"/>
        <source>&amp;Folder</source>
        <translation>&amp;Carpeta</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="168"/>
        <source>Set In Point</source>
        <translation>Establecer punto de entrada</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="169"/>
        <source>Set Out Point</source>
        <translation>Establecer punto de salida</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="170"/>
        <source>Reset In Point</source>
        <translation>Resetear punto de entrada</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="171"/>
        <source>Reset Out Point</source>
        <translation>Resetear punto de salida</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="172"/>
        <source>Clear In/Out Point</source>
        <translation>Limpiar puntos de Entrada/Salida</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="173"/>
        <source>Add Default Transition</source>
        <translation>Añadir Transición predeterminada</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="174"/>
        <source>Link/Unlink</source>
        <translation>Unir/Separar clips seleccionados </translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="175"/>
        <source>Enable/Disable</source>
        <translation>Habilitar/Deshabilitar clips seleccionados</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="176"/>
        <source>Nest</source>
        <translation>Anidar selección en una secuencia</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="177"/>
        <source>Cu&amp;t</source>
        <translation>Cortar (&amp;x)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="178"/>
        <source>Cop&amp;y</source>
        <translation>&amp;Copiar</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="179"/>
        <location filename="../ui/menuhelper.cpp" line="274"/>
        <source>&amp;Paste</source>
        <translation>&amp;Pegar</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="180"/>
        <source>Paste Insert</source>
        <translation>Insertar (Pegar)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="181"/>
        <source>Duplicate</source>
        <translation>Duplicar</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="182"/>
        <source>Delete</source>
        <translation>Eliminar Selección (No elimina el hueco)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="183"/>
        <source>Ripple Delete</source>
        <translation>Extraer Selección (Elimina el hueco)</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="184"/>
        <source>Split</source>
        <translation>Dividir clips</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="227"/>
        <source>Invalid aspect ratio</source>
        <translation>Relación de aspecto no válida</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="227"/>
        <source>The aspect ratio &apos;%1&apos; is invalid. Please try again.</source>
        <translation>La relación de aspecto &apos;%1&apos; no es válida. Inténtalo de nuevo.</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="230"/>
        <source>Enter custom aspect ratio</source>
        <translation>Introduzca una relación de aspecto personalizada</translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="230"/>
        <source>Enter the aspect ratio to use for the title/action safe area (e.g. 16:9):</source>
        <translation>Ingrese la relación de aspecto a usar para el área segura de título/acción (por ejemplo, 16:9):</translation>
    </message>
</context>
<context>
    <name>NewSequenceDialog</name>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="62"/>
        <source>Editing &quot;%1&quot;</source>
        <translation>Edición &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="82"/>
        <source>New Sequence</source>
        <translation>Nueva Secuencia</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="209"/>
        <source>Preset:</source>
        <translation>Preestablecidos:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="213"/>
        <source>Film 4K</source>
        <translation>Película 4K</translation>
    </message>
    <message>
        <source>TV 4K (Ultra HD/2160p)</source>
        <translation type="vanished">TV 4K (Ultra HD/2160p)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="214"/>
        <source>1080p</source>
        <translation>1080p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="215"/>
        <source>V720p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="216"/>
        <source>720p</source>
        <translation>720p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="217"/>
        <source>480p</source>
        <translation>480p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="218"/>
        <source>360p</source>
        <translation>360p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="219"/>
        <source>240p</source>
        <translation>240p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="220"/>
        <source>144p</source>
        <translation>144p</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="221"/>
        <source>NTSC (480i)</source>
        <translation>NTSC (480i)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="222"/>
        <source>PAL (576i)</source>
        <translation>PAL (576i)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="223"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="231"/>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="235"/>
        <source>Width:</source>
        <translation>Ancho:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="241"/>
        <source>Height:</source>
        <translation>Alto:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="247"/>
        <source>Frame Rate:</source>
        <translation>Velocidad de Fotogramas (FPS):</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="267"/>
        <source>Pixel Aspect Ratio:</source>
        <translation>Relación de aspecto de píxeles:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="269"/>
        <source>Square Pixels (1.0)</source>
        <translation>Píxeles cuadrados (1.0)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="272"/>
        <source>Interlacing:</source>
        <translation>Entrelazado:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="274"/>
        <source>None (Progressive)</source>
        <translation>Ninguno (Progresivo)</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="282"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="286"/>
        <source>Sample Rate: </source>
        <translation>Frecuencia de muestreo:</translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="304"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
</context>
<context>
    <name>Node</name>
    <message>
        <source>Node</source>
        <translation type="vanished">Nodo</translation>
    </message>
</context>
<context>
    <name>NodeBlock</name>
    <message>
        <source>Previous</source>
        <translation type="vanished">Anterior</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="vanished">Siguiente</translation>
    </message>
    <message>
        <source>Block</source>
        <translation type="vanished">Bloquear</translation>
    </message>
</context>
<context>
    <name>NodeEditor</name>
    <message>
        <source>Node Editor</source>
        <translation type="vanished">Editor de Nodos</translation>
    </message>
</context>
<context>
    <name>NodeIO</name>
    <message>
        <source>Disable Keyframes</source>
        <translation type="vanished">Desconectar Fotogramas Clave</translation>
    </message>
    <message>
        <source>Disabling keyframes will delete all current keyframes. Are you sure you want to do this?</source>
        <translation type="vanished">¡Al desactivar los fotogramas clave se eliminarán todos los fotogramas clave actuales! ¿Seguro que quieres hacer esto?</translation>
    </message>
</context>
<context>
    <name>NodeMedia</name>
    <message>
        <source>Matrix</source>
        <translation type="vanished">Matriz</translation>
    </message>
    <message>
        <source>Texture</source>
        <translation type="vanished">Textura</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="vanished">Medios</translation>
    </message>
</context>
<context>
    <name>NodeTexturePassthru</name>
    <message>
        <source>Texture</source>
        <translation type="vanished">Textura</translation>
    </message>
    <message>
        <source>Image Output</source>
        <translation type="vanished">Salida de imagen</translation>
    </message>
</context>
<context>
    <name>NodeVideoClip</name>
    <message>
        <source>Texture</source>
        <translation type="vanished">Textura</translation>
    </message>
</context>
<context>
    <name>NodeView</name>
    <message>
        <source>Node Editor</source>
        <translation type="vanished">Editor de Nodos</translation>
    </message>
</context>
<context>
    <name>OldEffectNode</name>
    <message>
        <source>Save Effect Settings</source>
        <translation type="vanished">Guardar Ajustes del Efecto</translation>
    </message>
    <message>
        <source>Effect XML Settings %1</source>
        <translation type="vanished">Configuración de efectos XML %1</translation>
    </message>
    <message>
        <source>Save Settings Failed</source>
        <translation type="vanished">El guardado de los ajustes a fallado</translation>
    </message>
    <message>
        <source>Failed to open &quot;%1&quot; for writing.</source>
        <translation type="vanished">Error al abrir &quot;%1&quot; para escribir.</translation>
    </message>
    <message>
        <source>Load Effect Settings</source>
        <translation type="vanished">Cargar Ajustes del Efecto</translation>
    </message>
    <message>
        <source>Load Settings Failed</source>
        <translation type="vanished">La carga de los ajustes ha fallado</translation>
    </message>
    <message>
        <source>Failed to open &quot;%1&quot; for reading.</source>
        <translation type="vanished">Error al abrir &quot;%1&quot; para leer.</translation>
    </message>
    <message>
        <source>This settings file doesn&apos;t match this effect.</source>
        <translation type="vanished">Este archivo de configuración no es valido para este efecto.</translation>
    </message>
</context>
<context>
    <name>OliveGlobal</name>
    <message>
        <source>Olive Project %1</source>
        <translation type="vanished">Proyecto de Olive %1</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="96"/>
        <source>Auto-recovery</source>
        <translation>Recuperación Automática</translation>
    </message>
    <message>
        <source>Olive didn&apos;t close properly and an autorecovery file was detected. Would you like to open it?</source>
        <translation type="vanished">Olive no se cerró correctamente y se generó un archivo de recuperación automática. ¿Deseas abrirlo?</translation>
    </message>
    <message>
        <source>Effect already exists</source>
        <translation type="vanished">Effect already exists</translation>
    </message>
    <message>
        <source>Clip &apos;%1&apos; already contains a &apos;%2&apos; effect. Would you like to replace it with the pasted one or add it as a separate effect?</source>
        <translation type="vanished">El clip &apos;%1&apos; ya contiene un efecto &apos;%2&apos;. ¿Desea reemplazarlo con el que está pegando o agregarlo como un efecto separado?</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Añadir</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation type="vanished">Reemplazar</translation>
    </message>
    <message>
        <source>Skip</source>
        <translation type="vanished">Omitir</translation>
    </message>
    <message>
        <source>Do this for all conflicts found</source>
        <translation type="vanished">Haga esto para todos los conflictos encontrados</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="239"/>
        <source>Open Project...</source>
        <translation>Abrir Preyecto...</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="250"/>
        <source>Missing recent project</source>
        <translation>No se encuentra este proyecto reciente, si lo ha movido de su ubicación desde que lo guardo por última vez deberá abrirlo manualmente</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="251"/>
        <source>The project &apos;%1&apos; no longer exists. Would you like to remove it from the recent projects list?</source>
        <translation>El proyecto &apos;%1&apos; ya no existe. ¿Deseas eliminarlo de la lista de proyectos recientes?</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="262"/>
        <source>Save Project As...</source>
        <translation>Guardar Proyecto Como...</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="287"/>
        <source>Unsaved Project</source>
        <translation>Proyecto no guardado</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="288"/>
        <source>This project has changed since it was last saved. Would you like to save it before closing?</source>
        <translation>¡ADVERTENCIA! Se han realizado cambios desde la última vez que se guardó. ¿Deseas guardar éstos antes de cerrar?</translation>
    </message>
    <message>
        <source>Import media...</source>
        <translation type="vanished">Importar Medios...</translation>
    </message>
    <message>
        <source>All Files</source>
        <translation type="vanished">Todos los Archivos</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="382"/>
        <source>No active sequence</source>
        <translation>Ninguna Secuaencia Activa</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="383"/>
        <source>Please open the sequence to perform this action.</source>
        <translation>Por favor, abra la secuencia para realizar esta acción.</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="448"/>
        <source>No clips selected</source>
        <translation>Ningún Clip Seleccionado</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="449"/>
        <source>Select the clips you wish to auto-cut</source>
        <translation>Seleccione los clips que desea cortar automáticamente</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="319"/>
        <source>Missing Project File</source>
        <translation>No se encuentra el archivo de proyecto</translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="65"/>
        <source>KunGang (Jan 2020 | Alpha%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="68"/>
        <source>KunGang Project %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="97"/>
        <source>KunGang didn&apos;t close properly and an autorecovery file was detected. Would you like to open it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="320"/>
        <source>Specified project &apos;%1&apos; does not exist.</source>
        <translation>El proyecto especificado &apos;%1&apos; no existe.</translation>
    </message>
</context>
<context>
    <name>PanEffect</name>
    <message>
        <location filename="../effects/internal/paneffect.cpp" line="32"/>
        <source>Pan</source>
        <translation>Panorámica</translation>
    </message>
    <message>
        <source>Modifying the panning on a stereo audio clip.</source>
        <translation type="vanished">Modificar la panorámica en un clip de audio estéreo.</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="84"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="91"/>
        <source>Default Sequence</source>
        <translation>Secuencia Predeterminada</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="196"/>
        <source>Invalid CSS File</source>
        <translation>Archivo CSS NO válido</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="197"/>
        <source>CSS file &apos;%1&apos; does not exist.</source>
        <translation>El archivo CSS &apos;%1&apos; NO existe.</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="363"/>
        <source>Confirm Reset All Shortcuts</source>
        <translation>Confirmar restablecer todos los accesos directos</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="364"/>
        <source>Are you sure you wish to reset all keyboard shortcuts to their defaults?</source>
        <translation>¿Está seguro de que desea restablecer todos los métodos abreviados de teclado a sus valores predeterminados?</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="414"/>
        <source>Import Keyboard Shortcuts</source>
        <translation>Importar Accesos Rápidos de Teclado</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="438"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="462"/>
        <source>Error saving shortcuts</source>
        <translation>Error al guardar los Accesos Rápidos</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="439"/>
        <source>Failed to open file for reading</source>
        <translation>Error al abrir el archivo de lectura</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="446"/>
        <source>Export Keyboard Shortcuts</source>
        <translation>Exportar los Accesos Rápidos de Teclado</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="460"/>
        <source>Export Shortcuts</source>
        <translation>Exportar Accesos Rápidos</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="460"/>
        <source>Shortcuts exported successfully</source>
        <translation>Atajos exportados exitosamente</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="462"/>
        <source>Failed to open file for writing</source>
        <translation>Error al abrir el archivo para escribir</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="468"/>
        <source>Browse for CSS file</source>
        <translation>Buscar el archivo CSS</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="476"/>
        <source>Delete All Previews</source>
        <translation>Eliminar todas las vistas previas</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="477"/>
        <source>Are you sure you want to delete all previews?</source>
        <translation>¿Estás seguro de que deseas eliminar todas las vistas previas?</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="481"/>
        <source>Previews Deleted</source>
        <translation>Vistas previas eliminadas</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="506"/>
        <source>Language:</source>
        <translation>Idioma:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="582"/>
        <source>Default Sequence Settings</source>
        <translation>Configuración predeterminada
de las secuencias</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="594"/>
        <source>Add Default Effects to New Clips</source>
        <translation>Añadir efectos predeterminados a los nuevos clips</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="598"/>
        <source>Automatically Seek to the Beginning When Playing at the End of a Sequence</source>
        <translation>Ir al principio cuando iniciamos la reproducción
con el cursor al final de la secuencia</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="602"/>
        <source>Selecting Also Seeks</source>
        <translation>Al seleccionar un clip
poner el cursor en su inico</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="606"/>
        <source>Edit Tool Also Seeks</source>
        <translation>Poner el cursor de reproducción
al inicio de la selección</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="610"/>
        <source>Edit Tool Selects Links</source>
        <translation>La selección incluye
los clips vinculados</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="614"/>
        <source>Seek Also Selects</source>
        <translation>El cursor de reproducción
selecciona los clips que cruza</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="618"/>
        <source>Seek to the End of Pastes</source>
        <translation>Desplazar el cursor al final de lo pegado</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="622"/>
        <source>Scroll Wheel Zooms</source>
        <translation>Usar rueda del ratón para hacer zoom</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="623"/>
        <source>Hold CTRL to toggle this setting</source>
        <translation>Mantenga presionada la tecla CTRL para cambiar esta configuración</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="627"/>
        <source>Invert Timeline Scroll Axes</source>
        <translation>Rueda del ratón desplaza la línea de tiempo</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="631"/>
        <source>Enable Drag Files to Timeline</source>
        <translation>Habilitar poder arrastrar archivos a la línea de tiempo</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="635"/>
        <source>Auto-Scale By Default</source>
        <translation>Escala automática por defecto</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="639"/>
        <source>Auto-Seek to Imported Clips</source>
        <translation>Búsqueda automática de clips importados</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="643"/>
        <source>Audio Scrubbing</source>
        <translation>Limpiar o depurar Audio</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="647"/>
        <source>Drop Files on Media to Replace</source>
        <translation>Colocar archivos de medios para reemplazar</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="651"/>
        <source>Enable Hover Focus</source>
        <translation>Habilitar Enfoque flotante</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="655"/>
        <source>Ask For Name When Setting Marker</source>
        <translation>Preguntar por el nombre al insertar un marcador</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="661"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="668"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="671"/>
        <source>Olive Dark (Default)</source>
        <translation>Olive Oscuro (Por defecto)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="672"/>
        <source>Olive Light</source>
        <translation>Olive Claro</translation>
    </message>
    <message>
        <source>Native</source>
        <translation type="vanished">Nativo del sistema</translation>
    </message>
    <message>
        <source>Native (Light Icons)</source>
        <translation type="vanished">Nativo con Iconos Claros</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="683"/>
        <source>Use Native Menu Styling</source>
        <translation>Usar el estilo de menú nativo</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="691"/>
        <source>Custom CSS:</source>
        <translation>CSS Personalizado:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="697"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="542"/>
        <source>Image sequence formats:</source>
        <translation>Secuencia de imágenes.
Formatos:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="814"/>
        <source>Audio Recording:</source>
        <translation>Grabación de audio en:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="817"/>
        <source>Mono</source>
        <translation>Monoaural (1 canal)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="818"/>
        <source>Stereo</source>
        <translation>Estéreo (2 canales)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="704"/>
        <source>Effect Textbox Lines:</source>
        <translation>Efectos de inserción de Texto.
Líneas de los Cuadros de Texto:</translation>
    </message>
    <message>
        <source>(None)</source>
        <translation type="vanished">(Nada)</translation>
    </message>
    <message>
        <source>OpenColorIO Config Error</source>
        <translation type="vanished">Error de configuración de OpenColorIO</translation>
    </message>
    <message>
        <source>Failed to set OpenColorIO configuration: %1</source>
        <translation type="vanished">Error al establecer la configuración de OpenColorIO: %1</translation>
    </message>
    <message>
        <source>Invalid OpenColorIO Configuration File</source>
        <translation type="vanished">Archivo de configuración de OpenColorIO no válido</translation>
    </message>
    <message>
        <source>You must specify an OpenColorIO configuration file if color management is enabled.</source>
        <translation type="vanished">Debe especificar un archivo de configuración de OpenColorIO si la administración de color está habilitada.</translation>
    </message>
    <message>
        <source>OpenColorIO configuration file &apos;%1&apos; does not exist.</source>
        <translation type="vanished">El archivo de configuración de OpenColorIO &apos;%1&apos; no existe.</translation>
    </message>
    <message>
        <source>Browse for OpenColorIO configuration</source>
        <translation type="vanished">Buscar la configuración OpenColorIO</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="482"/>
        <source>All previews deleted successfully. You may have to re-open your current project for changes to take effect.</source>
        <translation>Todas las vistas previas eliminadas con éxito. Es posible que tenga que volver a abrir su proyecto actual para que los cambios surtan efecto.</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="552"/>
        <source>Thumbnail Resolution:</source>
        <translation>Resolución miniaturas:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="560"/>
        <source>Waveform Resolution:</source>
        <translation>Resolución Onda de Audio:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="568"/>
        <source>Delete Previews</source>
        <translation>Eliminar vistas previas</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="575"/>
        <source>Use Software Fallbacks When Possible</source>
        <translation>Use los recursos de software cuando sea posible</translation>
    </message>
    <message>
        <source>Don&apos;t Use Proxies When Exporting</source>
        <translation type="vanished">No use proxies al exportar</translation>
    </message>
    <message>
        <source>Use originals instead of proxies when exporting</source>
        <translation type="vanished">Use originales en lugar de proxies al exportar</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="586"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="590"/>
        <source>Behavior</source>
        <translation>Comportamiento</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="719"/>
        <source>Memory Usage</source>
        <translation>Uso de Memoria</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="721"/>
        <source>Upcoming Frame Queue:</source>
        <translation>Cargar cola de fotogramas en:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="726"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="735"/>
        <source>frames</source>
        <translation>Fotogramas</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="727"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="736"/>
        <source>seconds</source>
        <translation>segundos</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="730"/>
        <source>Previous Frame Queue:</source>
        <translation>Cola de fotogramas anteriores en:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="741"/>
        <source>Playback</source>
        <translation>Reproducir</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="752"/>
        <source>Output Device:</source>
        <translation>Dispositivo de Salida:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="755"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="778"/>
        <source>Default</source>
        <translation>Por defecto</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="775"/>
        <source>Input Device:</source>
        <translation>Dispositivo de Entrada:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="798"/>
        <source>Sample Rate:</source>
        <translation>Frecuencia de muestreo:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="824"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Enable Color Management</source>
        <translation type="vanished">Habilitar la gestión del color</translation>
    </message>
    <message>
        <source>OpenColorIO Config File:</source>
        <translation type="vanished">Archivo de configuración de OpenColorIO:</translation>
    </message>
    <message>
        <source>Default Input Color Space:</source>
        <translation type="vanished">Espacio de color de entrada predeterminado:</translation>
    </message>
    <message>
        <source>Display:</source>
        <translation type="vanished">Monitor:</translation>
    </message>
    <message>
        <source>View:</source>
        <translation type="vanished">Ver:</translation>
    </message>
    <message>
        <source>Look:</source>
        <translation type="vanished">Mira:</translation>
    </message>
    <message>
        <source>Bit Depth</source>
        <translation type="vanished">Profundidad de bits</translation>
    </message>
    <message>
        <source>Playback (Offline):</source>
        <translation type="vanished">Reproducción (sin conexión):</translation>
    </message>
    <message>
        <source>Export (Online):</source>
        <translation type="vanished">Exportar (sin conexión):</translation>
    </message>
    <message>
        <source>Color Management</source>
        <translation type="vanished">Manejo del color</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="832"/>
        <source>Search for action or shortcut</source>
        <translation>Buscar acción o atajo</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="839"/>
        <source>Action</source>
        <translation>Acción</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="840"/>
        <source>Shortcut</source>
        <translation>Atajo</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="845"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="849"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="855"/>
        <source>Reset Selected</source>
        <translation>Restablecer lo Seleccionado</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="859"/>
        <source>Reset All</source>
        <translation>Restablecer Todo</translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="865"/>
        <source>Keyboard</source>
        <translation>Atajos de Teclado</translation>
    </message>
</context>
<context>
    <name>PreviewGenerator</name>
    <message>
        <location filename="../project/previewgenerator.cpp" line="203"/>
        <source>Failed to find any valid video/audio streams</source>
        <translation>Error al encontrar cualquier transmisión de video/audio válida</translation>
    </message>
    <message>
        <location filename="../project/previewgenerator.cpp" line="559"/>
        <source>Could not open file - %1</source>
        <translation>No se pudo abrir el archivo -%1</translation>
    </message>
    <message>
        <location filename="../project/previewgenerator.cpp" line="566"/>
        <source>Could not find stream information - %1</source>
        <translation>No se pudo encontrar la información de la secuencia -%1</translation>
    </message>
</context>
<context>
    <name>Project</name>
    <message>
        <location filename="../panels/project.cpp" line="99"/>
        <source>New</source>
        <translation>Nuevo</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="105"/>
        <source>Open Project</source>
        <translation>Abrir Proyecto</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="111"/>
        <source>Save Project</source>
        <translation>Guardar Proyecto</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="117"/>
        <source>Undo</source>
        <translation>Deshacer los cambiós</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="123"/>
        <source>Redo</source>
        <translation>Reacer los cambios</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="134"/>
        <source>Tree View</source>
        <translation>Ver en árbol</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="140"/>
        <source>Icon View</source>
        <translation>Ver como iconos</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="146"/>
        <source>List View</source>
        <translation>Ver en modo lista</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="224"/>
        <source>Search media, markers, etc.</source>
        <translation>Buscar archivos multimedia, marcas, etc.</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="225"/>
        <source>Project</source>
        <translation>Proyecto</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="364"/>
        <location filename="../panels/project.cpp" line="1017"/>
        <source>No active sequence</source>
        <translation>Sin secuencia activa</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="365"/>
        <source>No sequence is active, please open the sequence you want to replace clips from.</source>
        <translation>Ninguna secuencia está activa, abra la secuencia desde la que desea reemplazar los clips.</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="373"/>
        <source>Active sequence selected</source>
        <translation>Secuencia activa seleccionada</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="374"/>
        <source>You cannot insert a sequence into itself, so no clips of this media would be in this sequence.</source>
        <translation>No puede insertar una secuencia en sí misma, por lo que no habrá clips de este medio en esta secuencia.</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="405"/>
        <source>Rename &apos;%1&apos;</source>
        <translation>Renombrar &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="406"/>
        <source>Enter new name:</source>
        <translation>Introduzca un nuevo nombre:</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="577"/>
        <source>Delete media in use?</source>
        <translation>¿Realmente quieres borrar este archivo que está en uso?</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="578"/>
        <source>The media &apos;%1&apos; is currently used in &apos;%2&apos;. Deleting it will remove all instances in the sequence. Are you sure you want to do this?</source>
        <translation>El medio &apos;%1&apos; se usa actualmente en &apos;%2&apos;. Al eliminarlo se eliminarán todas las instancias en la secuencia. ¿Seguro que quieres hacer esto?</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="581"/>
        <source>Skip</source>
        <translation>Saltar/Omitir</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="1018"/>
        <source>No sequence is active, please open the sequence you want to delete clips from.</source>
        <translation>Ninguna secuencia está activa, abra la secuencia de la que desea eliminar los clips.</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="229"/>
        <source>Sequence</source>
        <translation>Secuencia</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="351"/>
        <source>Replace &apos;%1&apos;</source>
        <translation>Reemplazar &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="353"/>
        <location filename="../panels/project.cpp" line="1005"/>
        <source>All Files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="746"/>
        <source>Import a Project</source>
        <translation>Importar un proyecto</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="747"/>
        <source>&quot;%1&quot; is an Olive project file. It will merge with this project. Do you wish to continue?</source>
        <translation>&quot;%1&quot; es un archivo de proyecto de Olive. Se fusionará con este proyecto. ¿Desea continuar?</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="850"/>
        <source>Image sequence detected</source>
        <translation>Secuencia de imágenes detectada</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="851"/>
        <source>The file &apos;%1&apos; appears to be part of an image sequence. Would you like to import it as such?</source>
        <translation>El archivo &apos;%1&apos; parece ser parte de una secuencia de imágenes. ¿Te gustaría importarlo como tal?</translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="1005"/>
        <source>Import media...</source>
        <translation>Importar Medios...</translation>
    </message>
</context>
<context>
    <name>ProjectModel</name>
    <message>
        <source>Sequence %1</source>
        <translation type="vanished">Secuencia %1</translation>
    </message>
    <message>
        <source>Import a Project</source>
        <translation type="vanished">Importar un proyecto</translation>
    </message>
    <message>
        <source>&quot;%1&quot; is an Olive project file. It will merge with this project. Do you wish to continue?</source>
        <translation type="vanished">&quot;%1&quot; es un archivo de proyecto de Olive. Se fusionará con este proyecto. ¿Desea continuar?</translation>
    </message>
    <message>
        <source>Image sequence detected</source>
        <translation type="vanished">Secuencia de Imágenes Detectada</translation>
    </message>
    <message>
        <source>The file &apos;%1&apos; appears to be part of an image sequence. Would you like to import it as such?</source>
        <translation type="vanished">El archivo &apos;%1&apos; parece ser parte de una secuencia de imágenes. ¿Te gustaría importarlo como tal?</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="41"/>
        <source>Create Proxy</source>
        <translation>Crear Proxy</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="44"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="50"/>
        <source>Dimensions:</source>
        <translation>Dimensiones:</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="53"/>
        <source>Same Size as Source</source>
        <translation>Mismo tamaño que la fuente</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="54"/>
        <source>Half Resolution (1/2)</source>
        <translation>Resolución a la mitad (1/2)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="55"/>
        <source>Quarter Resolution (1/4)</source>
        <translation>Resolución a un cuarto (1/4)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="56"/>
        <source>Eighth Resolution (1/8)</source>
        <translation>Resolución a un octavo (1/8)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="57"/>
        <source>Sixteenth Resolution (1/16)</source>
        <translation>Resolución a un dieciseisavo (1/16)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="61"/>
        <source>Format:</source>
        <translation>Formato:</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="64"/>
        <source>ProRes HQ</source>
        <translation>ProRes HQ</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="72"/>
        <source>Location:</source>
        <translation>Localización:</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="75"/>
        <source>Same as Source (in &quot;%1&quot; folder)</source>
        <translation>Igual que la fuente (en la carpeta &quot;%1&quot;)</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="128"/>
        <source>Proxy file exists</source>
        <translation>El archivo proxy existe</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="129"/>
        <source>The file &quot;%1&quot; already exists. Do you wish to replace it?</source>
        <translation>El archivo &quot;%1&quot; ya existe. ¿Desea reemplazarlo?</translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="182"/>
        <source>Custom Location</source>
        <translation>Ubicación Personalizada</translation>
    </message>
</context>
<context>
    <name>ProxyGenerator</name>
    <message>
        <location filename="../project/proxygenerator.cpp" line="332"/>
        <source>Finished generating proxy for &quot;%1&quot;</source>
        <translation>Terminado de generar el proxy para &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>ReplaceClipMediaDialog</name>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="37"/>
        <source>Replace clips using &quot;%1&quot;</source>
        <translation>Reemplazar clips usando &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="43"/>
        <source>Select which media you want to replace this media&apos;s clips with:</source>
        <translation>Seleccione el medio con el que desea reemplazar los clips de este medio por:</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="49"/>
        <source>Keep the same media in-points</source>
        <translation>Mantener los mismos puntos de entrada de medios</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="57"/>
        <source>Replace</source>
        <translation>Reemplazar</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="61"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="77"/>
        <source>No media selected</source>
        <translation>Ningún medio seleccionado</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="78"/>
        <source>Please select a media to replace with or click &apos;Cancel&apos;.</source>
        <translation>Seleccione un medio para reemplazar o haga clic en &quot;Cancelar&quot;.</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="86"/>
        <source>Same media selected</source>
        <translation>Mismo medio seleccionado</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="87"/>
        <source>You selected the same media that you&apos;re replacing. Please select a different one or click &apos;Cancel&apos;.</source>
        <translation>Seleccionó el mismo medio que está reemplazando. Por favor, seleccione uno diferente o haga clic en &apos;Cancelar&apos;.</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="93"/>
        <source>Folder selected</source>
        <translation>Carpeta Seleccionada</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="94"/>
        <source>You cannot replace footage with a folder.</source>
        <translation>No puedes reemplazar las imágenes con una carpeta.</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="101"/>
        <source>Active sequence selected</source>
        <translation>Secuencia activa seleccionada</translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="102"/>
        <source>You cannot insert a sequence into itself.</source>
        <translation>No puedes insertar una secuencia en sí misma.</translation>
    </message>
</context>
<context>
    <name>RichTextEffect</name>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="22"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="26"/>
        <source>Padding</source>
        <translation>Márgenes</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="30"/>
        <source>Position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="34"/>
        <source>Vertical Align:</source>
        <translation>Alineación Vertical:</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="36"/>
        <source>Top</source>
        <translation>Arriba</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="37"/>
        <source>Center</source>
        <translation>Centro</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="38"/>
        <source>Bottom</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="42"/>
        <source>Auto-Scroll</source>
        <translation>Desplazamiento automático</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="44"/>
        <source>Off</source>
        <translation>Apagado</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="45"/>
        <source>Up</source>
        <translation>Arriba</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="46"/>
        <source>Down</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="47"/>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="48"/>
        <source>Right</source>
        <translation>Derecha</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="51"/>
        <source>Shadow</source>
        <translation>Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="55"/>
        <source>Shadow Color</source>
        <translation>Color de la Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="59"/>
        <source>Shadow Angle</source>
        <translation>Angulo de la Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="63"/>
        <source>Shadow Distance</source>
        <translation>Distancia de la Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="68"/>
        <source>Shadow Softness</source>
        <translation>Suavizado de la Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="73"/>
        <source>Shadow Opacity</source>
        <translation>Opacidad de la Sombra</translation>
    </message>
    <message>
        <source>Rich Text</source>
        <translation type="vanished">Texto enriquecido</translation>
    </message>
    <message>
        <source>Render</source>
        <translation type="vanished">Renderizar (Calcular)</translation>
    </message>
    <message>
        <source>Render formatted rich text over a clip.</source>
        <translation type="vanished">Renderizar texto enriquecido formateado sobre un clip.</translation>
    </message>
</context>
<context>
    <name>Sequence</name>
    <message>
        <location filename="../timeline/sequence.cpp" line="41"/>
        <source>%1 (copy)</source>
        <translation>%1 (copiar)</translation>
    </message>
</context>
<context>
    <name>ShakeEffect</name>
    <message>
        <location filename="../effects/internal/shakeeffect.cpp" line="38"/>
        <source>Intensity</source>
        <translation>Intensidad</translation>
    </message>
    <message>
        <location filename="../effects/internal/shakeeffect.cpp" line="43"/>
        <source>Rotation</source>
        <translation>Rotación</translation>
    </message>
    <message>
        <location filename="../effects/internal/shakeeffect.cpp" line="48"/>
        <source>Frequency</source>
        <translation>Frecuencia</translation>
    </message>
    <message>
        <source>Shake</source>
        <translation type="vanished">Temblor/Movimiento</translation>
    </message>
    <message>
        <source>Distort</source>
        <translation type="vanished">Distorsionar</translation>
    </message>
    <message>
        <source>Simulate a camera shake movement.</source>
        <translation type="vanished">Simular movimiento de la cámara.</translation>
    </message>
</context>
<context>
    <name>SolidEffect</name>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="43"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="45"/>
        <source>Solid Color</source>
        <translation>Color Sólido</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="46"/>
        <source>SMPTE Bars</source>
        <translation>Barras SMPTE</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="47"/>
        <source>Checkerboard</source>
        <translation>Tablero de damas</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="49"/>
        <source>Opacity</source>
        <translation>Opacidad</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="55"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="59"/>
        <source>Checkerboard Size</source>
        <translation>Tamaño de los cuadros</translation>
    </message>
    <message>
        <source>Solid</source>
        <translation type="vanished">Sólido</translation>
    </message>
    <message>
        <source>Render</source>
        <translation type="vanished">Renderizar/Calcular</translation>
    </message>
    <message>
        <source>Render a solid color over this clip.</source>
        <translation type="vanished">Renderiza un color sólido sobre este clip.</translation>
    </message>
</context>
<context>
    <name>SourcesCommon</name>
    <message>
        <location filename="../project/sourcescommon.cpp" line="83"/>
        <source>Import...</source>
        <translation>Impotar...</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="86"/>
        <source>New</source>
        <translation>Nuevo</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="90"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="93"/>
        <source>Tree View</source>
        <translation>Vista en árbol</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="96"/>
        <source>Icon View</source>
        <translation>Vista de Icono</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="99"/>
        <source>Show Toolbar</source>
        <translation>Mostrar la barra de herramientas</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="104"/>
        <source>Show Sequences</source>
        <translation>Mostrar Secuencias</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="116"/>
        <source>Replace/Relink Media</source>
        <translation>Reemplazar/Revincular Medios</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="120"/>
        <source>Reveal in Explorer</source>
        <translation>Revelar en el explorador</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="122"/>
        <source>Reveal in Finder</source>
        <translation>Revelar en el buscador</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="124"/>
        <source>Reveal in File Manager</source>
        <translation>Revelar en el administrador de archivos</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="129"/>
        <source>Replace Clips Using This Media</source>
        <translation>Reemplazar clips utilizando este medio</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="152"/>
        <source>Create Sequence With This Media</source>
        <translation>Crear secuencia con este medio</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="158"/>
        <source>Duplicate</source>
        <translation>Duplicar</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="164"/>
        <source>Delete All Clips Using This Media</source>
        <translation>Eliminar todos los clips que utilizan este medio</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="167"/>
        <source>Proxy</source>
        <translation>Trabajar con Proxy</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="174"/>
        <source>Generating proxy: %1% complete</source>
        <translation>Generando proxy: %1% completado</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="198"/>
        <source>Create/Modify Proxy</source>
        <translation>Crear/Modificar Proxy</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="201"/>
        <source>Create Proxy</source>
        <translation>Crear Proxy</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="212"/>
        <source>Modify Proxy</source>
        <translation>Modificar Proxy</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="215"/>
        <source>Restore Original</source>
        <translation>Restaurar Original</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="221"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="228"/>
        <source>Preview in Media Viewer</source>
        <translation>Previsualizar en el visor de medios</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="234"/>
        <source>Properties...</source>
        <translation>Propiedades...</translation>
    </message>
    <message>
        <source>Replace &apos;%1&apos;</source>
        <translation type="vanished">Reemplazar &apos;%1&apos;</translation>
    </message>
    <message>
        <source>All Files</source>
        <translation type="vanished">Todos los archivos</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="291"/>
        <source>Replace Media</source>
        <translation>Reemplazar medios</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="292"/>
        <source>You dropped a file onto &apos;%1&apos;. Would you like to replace it with the dropped file?</source>
        <translation>Has colocado un archivo en &apos;%1&apos;. ¿Te gustaría reemplazarlo con el archivo caído?</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="421"/>
        <source>Delete proxy</source>
        <translation>Eliminar Proxy</translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="422"/>
        <source>Would you like to delete the proxy file &quot;%1&quot; as well?</source>
        <translation>¿Desea eliminar el archivo proxy &quot;%1&quot; también?</translation>
    </message>
</context>
<context>
    <name>SpeedDialog</name>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="40"/>
        <source>Speed/Duration</source>
        <translation>Velocidad/Duración</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="49"/>
        <source>Speed:</source>
        <translation>Velocidad:</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="56"/>
        <source>Frame Rate:</source>
        <translation>Velocidad
Fotogramas:</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="61"/>
        <source>Duration:</source>
        <translation>Duración:</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="69"/>
        <source>Reverse</source>
        <translation>Invertir Dirección</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="70"/>
        <source>Maintain Audio Pitch</source>
        <translation>Mantener el Tono del Audio</translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="71"/>
        <source>Ripple Changes</source>
        <translation>Desplazar clips contiguos</translation>
    </message>
</context>
<context>
    <name>TextEditDialog</name>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="35"/>
        <source>Edit Text</source>
        <translation>Editar texto</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="68"/>
        <source>Thin</source>
        <translation>Fino</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="69"/>
        <source>Extra Light</source>
        <translation>Extra Fino</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="70"/>
        <source>Light</source>
        <translation>Suave</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="71"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="72"/>
        <source>Medium</source>
        <translation>Medio</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="73"/>
        <source>Demi Bold</source>
        <translation>Semi Negrita</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="74"/>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="75"/>
        <source>Extra Bold</source>
        <translation>Extra Negrita</translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="76"/>
        <source>Black</source>
        <translation>Grueso</translation>
    </message>
</context>
<context>
    <name>TextEditEx</name>
    <message>
        <location filename="../ui/texteditex.cpp" line="43"/>
        <source>Edit Text</source>
        <translation>Editar Texto</translation>
    </message>
    <message>
        <location filename="../ui/texteditex.cpp" line="91"/>
        <source>&amp;Edit Text</source>
        <translation>&amp;Editar Texto</translation>
    </message>
</context>
<context>
    <name>TextEffect</name>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="51"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="55"/>
        <source>Font</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="59"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="64"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <source>Horizontal Alignment</source>
        <translation type="vanished">Alineación Horizontal</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="70"/>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="71"/>
        <location filename="../effects/internal/texteffect.cpp" line="77"/>
        <source>Center</source>
        <translation>Centrado</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="72"/>
        <source>Right</source>
        <translation>Derecha</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="73"/>
        <source>Justify</source>
        <translation>Justificado</translation>
    </message>
    <message>
        <source>Vertical Alignment</source>
        <translation type="vanished">Alineación Vertical</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="76"/>
        <source>Top</source>
        <translation>Arriba</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="78"/>
        <source>Bottom</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="68"/>
        <source>Alignment</source>
        <translation>Alineación</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="80"/>
        <source>Word Wrap</source>
        <translation>Ajuste de línea</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="84"/>
        <source>Padding</source>
        <translation>Márgenes</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="88"/>
        <source>Position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="92"/>
        <source>Outline</source>
        <translation>Contorno</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="96"/>
        <source>Outline Color</source>
        <translation>Color Contorno</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="100"/>
        <source>Outline Width</source>
        <translation>Ancho del Contorno</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="105"/>
        <source>Shadow</source>
        <translation>Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="109"/>
        <source>Shadow Color</source>
        <translation>Color Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="113"/>
        <source>Shadow Angle</source>
        <translation>Ángulo Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="117"/>
        <source>Shadow Distance</source>
        <translation>Distancia Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="122"/>
        <source>Shadow Softness</source>
        <translation>Suavidad Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="127"/>
        <source>Shadow Opacity</source>
        <translation>Opacidad Sombra</translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="134"/>
        <source>Sample Text</source>
        <translation>Texto de ejemplo</translation>
    </message>
    <message>
        <source>Render</source>
        <translation type="vanished">Renderizar</translation>
    </message>
    <message>
        <source>Generate simple text over this clip</source>
        <translation type="vanished">Generar texto simple sobre este clip</translation>
    </message>
</context>
<context>
    <name>TimecodeEffect</name>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="51"/>
        <source>Timecode</source>
        <translation>Código de Tiempo</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="53"/>
        <source>Sequence</source>
        <translation>Secuencia</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="54"/>
        <source>Media</source>
        <translation>Clip</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="57"/>
        <source>Scale</source>
        <translation>Escala</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="64"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="69"/>
        <source>Background Color</source>
        <translation>Color del Fondo</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="74"/>
        <source>Background Opacity</source>
        <translation>Opacidad del Fondo</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="81"/>
        <source>Offset</source>
        <translation>Compensar x-y</translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="85"/>
        <source>Prepend</source>
        <translation>Anteponer</translation>
    </message>
    <message>
        <source>Render</source>
        <translation type="vanished">Renderizar</translation>
    </message>
    <message>
        <source>Render the media or sequence timecode on this clip.</source>
        <translation type="vanished">Renderice el código de tiempo de los medios, o la secuencia, en este clip.</translation>
    </message>
</context>
<context>
    <name>Timeline</name>
    <message>
        <location filename="../panels/timeline.cpp" line="1939"/>
        <source>Timeline: </source>
        <translation>Línea de Tiempo:</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="487"/>
        <source>Nested Sequence</source>
        <translation>Secuencia Anidada</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1826"/>
        <source>Title...</source>
        <translation>Título...</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1831"/>
        <source>Solid Color...</source>
        <translation>Color Sólido...</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1836"/>
        <source>Bars...</source>
        <translation>Barras...</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1843"/>
        <source>Tone...</source>
        <translation>Tono...</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1848"/>
        <source>Noise...</source>
        <translation>Ruido...</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1871"/>
        <source>Unsaved Project</source>
        <translation>Proyecto sin guardar</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1872"/>
        <source>You must save this project before you can record audio in it.</source>
        <translation>Debe guardar este proyecto antes de poder grabar audio en él.</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1878"/>
        <source>Click on the timeline where you want to start recording (drag to limit the recording to a certain timeframe)</source>
        <translation>Haga clic en la línea de tiempo donde desea iniciar la grabación (arrastre para limitar la grabación a un determinado período de tiempo)</translation>
    </message>
    <message>
        <source>Video Transitions</source>
        <translation type="vanished">Transiciones de Vídeo</translation>
    </message>
    <message>
        <source>Audio Transitions</source>
        <translation type="vanished">Transiciones de Audio</translation>
    </message>
    <message>
        <source>Timeline: %1</source>
        <translation type="vanished">Linea de Tiempo: %1</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1941"/>
        <source>(none)</source>
        <translation>(Ninguno)</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="124"/>
        <source>Pointer Tool</source>
        <translation>Puntero de Selección/Edición/Mover Clips</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="125"/>
        <source>Edit Tool</source>
        <translation>Herramienta de Selección</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="126"/>
        <source>Ripple Tool</source>
        <translation>Herramienta para Enrrollar/Desenrrollar</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="127"/>
        <source>Razor Tool</source>
        <translation>Herramienta de corte</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="128"/>
        <source>Slip Tool</source>
        <translation>Deslizar clip sin desplazar</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="129"/>
        <source>Slide Tool</source>
        <translation>Desplazar clip afectando a los clips contiguos</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="130"/>
        <source>Hand Tool</source>
        <translation>Herramienta de Mano</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="131"/>
        <source>Transition Tool</source>
        <translation>Herramienta para Inserción de Transiciones</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="132"/>
        <source>Snapping</source>
        <translation>Imantar</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="133"/>
        <source>Zoom In</source>
        <translation>Acercar (Zoom)</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="134"/>
        <source>Zoom Out</source>
        <translation>Alejar (Zoom)</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="135"/>
        <source>Record audio</source>
        <translation>Grabar Audio</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="136"/>
        <source>Add title, solid, bars, etc.</source>
        <translation>Añadir clip de:.</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1353"/>
        <source>Effect already exists</source>
        <translation>El efecto ya existe</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1354"/>
        <source>Clip &apos;%1&apos; already contains a &apos;%2&apos; effect. Would you like to replace it with the pasted one or add it as a separate effect?</source>
        <translation>El clip &apos;%1&apos; ya contiene el efecto &apos;%2&apos;. ¿Desea reemplazarlo con el pegado o agregarlo como un efecto separado?</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1359"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1360"/>
        <source>Replace</source>
        <translation>Reemplazar</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1361"/>
        <source>Skip</source>
        <translation>Omitir</translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1363"/>
        <source>Do this for all conflicts found</source>
        <translation>Haga esto para todos los conflictos encontrados</translation>
    </message>
</context>
<context>
    <name>TimelineHeader</name>
    <message>
        <location filename="../ui/timelineheader.cpp" line="486"/>
        <source>Center Timecodes</source>
        <translation>Centrar Código de Tiempo</translation>
    </message>
</context>
<context>
    <name>TimelineLabel</name>
    <message>
        <source>Rename Track</source>
        <translation type="vanished">Renombrar Pista</translation>
    </message>
    <message>
        <source>Enter the new name for this track</source>
        <translation type="vanished">Introduzca el nuevo nombre para esta pista</translation>
    </message>
</context>
<context>
    <name>TimelineView</name>
    <message>
        <source>&amp;Undo</source>
        <translation type="vanished">&amp;Deshacer</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation type="vanished">&amp;Rehacer</translation>
    </message>
    <message>
        <source>R&amp;ipple Delete Empty Space</source>
        <translation type="vanished">&amp;Eliminar espacio vacío</translation>
    </message>
    <message>
        <source>Sequence Settings</source>
        <translation type="vanished">Ajustes de la Secuencia</translation>
    </message>
    <message>
        <source>&amp;Speed/Duration</source>
        <translation type="vanished">Cambiar &amp;Velocidad/Duración</translation>
    </message>
    <message>
        <source>Auto-Cut Silence</source>
        <translation type="vanished">Auto Cortar en los Silencios</translation>
    </message>
    <message>
        <source>Auto-S&amp;cale</source>
        <translation type="vanished">Escala Aut&amp;omática</translation>
    </message>
    <message>
        <source>&amp;Reveal in Project</source>
        <translation type="vanished">&amp;Revelar en Proyecto</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Propiedades</translation>
    </message>
    <message>
        <source>%1
Start: %2
End: %3
Duration: %4</source>
        <translation type="vanished">%1
Inicio: %2
Final: %3
Duración: %4</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Error</translation>
    </message>
    <message>
        <source>Couldn&apos;t locate media wrapper for sequence.</source>
        <translation type="vanished">No se pudo localizar el contenedor de medios para la secuencia.</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="vanished">Título</translation>
    </message>
    <message>
        <source>Solid Color</source>
        <translation type="vanished">Color Sólido</translation>
    </message>
    <message>
        <source>Bars</source>
        <translation type="vanished">Barras</translation>
    </message>
    <message>
        <source>Tone</source>
        <translation type="vanished">Tono</translation>
    </message>
    <message>
        <source>Noise</source>
        <translation type="vanished">Ruido</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation type="vanished">Duración:</translation>
    </message>
</context>
<context>
    <name>TimelineWidget</name>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="90"/>
        <source>&amp;Undo</source>
        <translation>&amp;Deshacer</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="91"/>
        <source>&amp;Redo</source>
        <translation>&amp;Rehacer</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="115"/>
        <source>Sequence Settings</source>
        <translation>Ajustes de la Secuencia</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="134"/>
        <source>&amp;Speed/Duration</source>
        <translation>Cambiar &amp;Velocidad/Duración</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="175"/>
        <source>&amp;Reveal in Project</source>
        <translation>&amp;Revelar en Proyecto</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="207"/>
        <source>%1
Start: %2
End: %3
Duration: %4</source>
        <translation>%1
Inicio: %2
Final: %3
Duración: %4</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="111"/>
        <source>R&amp;ipple Delete Empty Space</source>
        <translation>&amp;Eliminar espacio vacío</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="137"/>
        <source>Auto-Cut Silence</source>
        <translation>Auto Cortar en los Silencios</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="140"/>
        <source>Auto-S&amp;cale</source>
        <translation>Escala Aut&amp;omática</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="179"/>
        <source>Properties</source>
        <translation>Propiedades</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="233"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="233"/>
        <source>Couldn&apos;t locate media wrapper for sequence.</source>
        <translation>No se pudo localizar el contenedor de medios para la secuencia.</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="531"/>
        <source>New Sequence</source>
        <translation type="unfinished">Nueva Secuencia</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="532"/>
        <source>No sequence has been created yet. Would you like to make one based on this footage or set custom parameters?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="534"/>
        <source>Use Footage Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="535"/>
        <source>Custom Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1075"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1079"/>
        <source>Solid Color</source>
        <translation>Color Sólido</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1084"/>
        <source>Bars</source>
        <translation>Barras</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1095"/>
        <source>Tone</source>
        <translation>Tono</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1099"/>
        <source>Noise</source>
        <translation>Ruido</translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="2028"/>
        <source>Duration:</source>
        <translation>Duración:</translation>
    </message>
</context>
<context>
    <name>ToneEffect</name>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="31"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="33"/>
        <source>Sine</source>
        <translation>Sinusoidal</translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="35"/>
        <source>Frequency</source>
        <translation>Frecuencia</translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="41"/>
        <source>Amount</source>
        <translation>Cantidad</translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="47"/>
        <source>Mix</source>
        <translation>Mezclar</translation>
    </message>
    <message>
        <source>Tone</source>
        <translation type="vanished">Tono</translation>
    </message>
    <message>
        <source>Generate a sine wave tone to mix into this clip&apos;s audio.</source>
        <translation type="vanished">Genera un tono de onda sinusoidal para mezclarlo con el audio de este clip.</translation>
    </message>
</context>
<context>
    <name>Track</name>
    <message>
        <source>Video %1</source>
        <translation type="vanished">Vídeo %1</translation>
    </message>
    <message>
        <source>Audio %1</source>
        <translation type="vanished">Audio %1</translation>
    </message>
    <message>
        <source>Subtitle %1</source>
        <translation type="vanished">Subtítulo %1</translation>
    </message>
    <message>
        <source>Unknown %1</source>
        <translation type="vanished">Desconocido %1</translation>
    </message>
</context>
<context>
    <name>TransformEffect</name>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="49"/>
        <source>Position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="54"/>
        <source>Scale</source>
        <translation>Escala</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="64"/>
        <source>Uniform Scale</source>
        <translation>Escala Uniforme</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="68"/>
        <source>Rotation</source>
        <translation>Rotación</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="72"/>
        <source>Anchor Point</source>
        <translation>Punto de Ancla</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="77"/>
        <source>Opacity</source>
        <translation>Opacidad</translation>
    </message>
    <message>
        <source>Transform</source>
        <translation type="vanished">Transformación</translation>
    </message>
    <message>
        <source>Distort</source>
        <translation type="vanished">Distorsionar</translation>
    </message>
    <message>
        <source>Transform the position, scale, and rotation of this clip.</source>
        <translation type="vanished">Transformar la posición, escala y rotación de este clip.</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="84"/>
        <source>Blend Mode</source>
        <translation>Modo de Fusión</translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="89"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
</context>
<context>
    <name>Transition</name>
    <message>
        <location filename="../effects/transition.cpp" line="48"/>
        <source>Length</source>
        <translation>Longitud</translation>
    </message>
</context>
<context>
    <name>UpdateNotification</name>
    <message>
        <source>An update is available from the Olive website. Visit www.olivevideoeditor.org to download it.</source>
        <translation type="vanished">Una actualización está disponible en el sitio web de Olive. Visita www.olivevideoeditor.org para descargarla.</translation>
    </message>
    <message>
        <source>Invalid transition</source>
        <translation type="vanished">Transición No Válida</translation>
    </message>
    <message>
        <source>No candidate for transition &apos;%1&apos;. This transition may be corrupt. Try reinstalling it or Olive.</source>
        <translation type="vanished">Ningún candidato para la transición &apos;%1&apos;. Esta transición puede estar corrupta. Intenta volver a instalarla o reinstala Olive.</translation>
    </message>
    <message>
        <location filename="../ui/updatenotification.cpp" line="35"/>
        <source>An update is available from the Olive website. Visit http://www.sumoon.com/kungang to download it.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VSTHost</name>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="130"/>
        <location filename="../effects/internal/vsthost.cpp" line="146"/>
        <source>Error loading VST plugin</source>
        <translation>Error al cargar el Plugin VST</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="131"/>
        <source>Failed to load VST plugin &quot;%1&quot;: %2</source>
        <translation>Falló la carga del complemento VST &quot;%1&quot;:%2</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="147"/>
        <source>Failed to locate entry point for dynamic library.</source>
        <translation>Error al localizar el punto de entrada para la librería dinámica.</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="172"/>
        <source>VST Error</source>
        <translation>VST Error</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="172"/>
        <source>Plugin&apos;s magic number is invalid</source>
        <translation>El número mágico de Plugin no es válido</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="254"/>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="258"/>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="260"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <source>VST Plugin 2.x</source>
        <translation type="vanished">VST Plugin 2.x</translation>
    </message>
    <message>
        <source>Use a VST 2.x plugin on this clip&apos;s audio.</source>
        <translation type="vanished">Utilice un Plugin VST 2.x en el audio de este clip.</translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="228"/>
        <source>VST Plugin</source>
        <translation>Plugin VST</translation>
    </message>
</context>
<context>
    <name>Viewer</name>
    <message>
        <source>Viewer: %1</source>
        <translation type="vanished">Visionar: %1</translation>
    </message>
    <message>
        <source>Failed to import recorded file</source>
        <translation type="vanished">No se pudo importar el archivo grabado</translation>
    </message>
    <message>
        <source>An error occurred trying to import the recorded audio</source>
        <translation type="vanished">Se ha producido un error al intentar importar el audio grabado</translation>
    </message>
    <message>
        <location filename="../panels/viewer.cpp" line="601"/>
        <source>(none)</source>
        <translation>(ninguno)</translation>
    </message>
    <message>
        <location filename="../panels/viewer.cpp" line="737"/>
        <source>Drag video only</source>
        <translation>Sólo arrastrar Vídeo</translation>
    </message>
    <message>
        <location filename="../panels/viewer.cpp" line="744"/>
        <source>Drag audio only</source>
        <translation>Sólo arrastrar Audio</translation>
    </message>
    <message>
        <source>Sequence Viewer: %1</source>
        <translation type="vanished">Visor de secuencia:%1</translation>
    </message>
    <message>
        <source>Media Viewer: %1</source>
        <translation type="vanished">Visor de Medios: %1</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="936"/>
        <source>Sequence Viewer</source>
        <translation>Visor de Secuencias</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="937"/>
        <source>Media Viewer</source>
        <translation>Visor de Medios</translation>
    </message>
</context>
<context>
    <name>ViewerWidget</name>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="113"/>
        <source>Save Frame as Image...</source>
        <translation>Guardar fotograma como imagen...</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="116"/>
        <source>Show Fullscreen</source>
        <translation>Pantalla Completa</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="120"/>
        <source>Disable</source>
        <translation>Desconectar</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="123"/>
        <source>Screen %1: %2x%3</source>
        <translation>Pantalla %1: %2x%3</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="131"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="132"/>
        <source>Fit</source>
        <translation>Ajuste Automático</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="142"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="148"/>
        <source>Close Media</source>
        <translation>Cerrar Medios</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="158"/>
        <source>Save Frame</source>
        <translation>Guardar Fotograma</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="192"/>
        <source>Viewer Zoom</source>
        <translation>Visor de Zoom</translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="193"/>
        <source>Set Custom Zoom Value:</source>
        <translation>Establecer valor de zoom personalizado:</translation>
    </message>
</context>
<context>
    <name>ViewerWindow</name>
    <message>
        <location filename="../ui/viewerwindow.cpp" line="170"/>
        <source>Exit Fullscreen</source>
        <translation>Salir de la Pantalla Completa</translation>
    </message>
</context>
<context>
    <name>VoidEffect</name>
    <message>
        <location filename="../effects/internal/voideffect.cpp" line="33"/>
        <source>(unknown)</source>
        <translation>(Desconocido)</translation>
    </message>
    <message>
        <location filename="../effects/internal/voideffect.cpp" line="37"/>
        <source>Missing Effect</source>
        <translation>Efecto faltante</translation>
    </message>
</context>
<context>
    <name>VolumeEffect</name>
    <message>
        <location filename="../effects/internal/volumeeffect.cpp" line="32"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <source>Adjust the volume of this clip&apos;s audio</source>
        <translation type="vanished">Ajusta el volumen de los clips de audio</translation>
    </message>
</context>
<context>
    <name>bitdepths</name>
    <message>
        <source>8-bit</source>
        <translation type="vanished">8-bit</translation>
    </message>
    <message>
        <source>16-bit Integer</source>
        <translation type="vanished">16-bit Entero</translation>
    </message>
    <message>
        <source>Half-Float (16-bit)</source>
        <translation type="vanished">Medio-Coma-Flotante (16-bit)</translation>
    </message>
    <message>
        <source>Full-Float (32-bit)</source>
        <translation type="vanished">Máximo-Coma-Flotante (32-bit)</translation>
    </message>
</context>
<context>
    <name>transition</name>
    <message>
        <location filename="../effects/transition.cpp" line="117"/>
        <source>Invalid transition</source>
        <translation type="unfinished">Transición No Válida</translation>
    </message>
    <message>
        <location filename="../effects/transition.cpp" line="118"/>
        <source>No candidate for transition &apos;%1&apos;. This transition may be corrupt. Try reinstalling it or Olive.</source>
        <translation type="unfinished">Ningún candidato para la transición &apos;%1&apos;. Esta transición puede estar corrupta. Intenta volver a instalarla o reinstala Olive.</translation>
    </message>
</context>
</TS>
