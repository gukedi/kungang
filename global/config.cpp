/***

    Olive - Non-Linear Video Editor
    Copyright (C) 2019  Olive Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

***/

#include "config.h"

#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QLocale>
#include <QSettings>

#include "panels/project.h"
#include "panels/panels.h"

#include "debug.h"
#include "path.h"

Config olive::CurrentConfig;
RuntimeConfig olive::CurrentRuntimeConfig;

Config::Config()
  : show_track_lines(true),
    scroll_zooms(false),
    edit_tool_selects_links(false),
    edit_tool_also_seeks(false),
    select_also_seeks(false),
    paste_seeks(true),
    img_seq_formats("jpg|jpeg|bmp|tiff|tif|psd|png|tga|jp2|gif"),
    rectified_waveforms(false),
    default_transition_length(30),
    timecode_view(olive::kTimecodeNonDrop),
    show_title_safe_area(false),
    use_custom_title_safe_ratio(false),
    custom_title_safe_ratio(1),
    enable_drag_files_to_timeline(true),
    autoscale_by_default(false),
    recording_mode(2),
    enable_seek_to_import(false),
    enable_audio_scrubbing(true),
    drop_on_media_to_replace(true),
    autoscroll(olive::AUTOSCROLL_PAGE_SCROLL),
    audio_rate(48000),
    hover_focus(false),
    project_view_type(olive::PROJECT_VIEW_TREE),
    set_name_with_marker(true),
    show_project_toolbar(true),
    previous_queue_size(3),
    previous_queue_type(olive::FRAME_QUEUE_TYPE_FRAMES),
    upcoming_queue_size(0.5),
    upcoming_queue_type(olive::FRAME_QUEUE_TYPE_SECONDS),
    loop(false),
    seek_also_selects(false),
    auto_seek_to_beginning(true),
    effect_textbox_lines(3),
    use_software_fallback(false),
    center_timeline_timecodes(true),
    language_file("ts/kungang_zh-CN.qm"),
    locale_str("zh-CN"),
    waveform_resolution(64),
    thumbnail_resolution(120),
    add_default_effects_to_clips(true),
    invert_timeline_scroll_axes(true),
    style(olive::styling::kKunGangDark),
    use_native_menu_styling(true),
    default_sequence_width(720),
    default_sequence_height(1080),
    default_sequence_framerate(30.0),
    default_sequence_audio_frequency(48000),
    default_sequence_audio_channel_layout(3),
    locked_panels(false),
    image_frames(100)
{

}

void Config::load(QString path) {
   QString locale = QLocale::system().name();
   qInfo() << "local locale is " << locale << "\n";
  qInfo() << "load config file" << path << "\n";
  QFile f(path);
  if (f.exists() && f.open(QIODevice::ReadOnly)) {

    language_file = "ts/kungang_en-US.qm";
    locale_str = "en-US";
       
    QXmlStreamReader stream(&f);

    while (!stream.atEnd()) {
      stream.readNext();
      if (stream.isStartElement()) {
        if (stream.name() == "ShowTrackLines") {
          stream.readNext();
          show_track_lines = (stream.text() == "1");
        } else if (stream.name() == "ScrollZooms") {
          stream.readNext();
          scroll_zooms = (stream.text() == "1");
        } else if (stream.name() == "InvertTimelineScrollAxes") {
          stream.readNext();
          invert_timeline_scroll_axes = (stream.text() == "1");
        } else if (stream.name() == "EditToolSelectsLinks") {
          stream.readNext();
          edit_tool_selects_links = (stream.text() == "1");
        } else if (stream.name() == "EditToolAlsoSeeks") {
          stream.readNext();
          edit_tool_also_seeks = (stream.text() == "1");
        } else if (stream.name() == "SelectAlsoSeeks") {
          stream.readNext();
          select_also_seeks = (stream.text() == "1");
        } else if (stream.name() == "PasteSeeks") {
          stream.readNext();
          paste_seeks = (stream.text() == "1");
        } else if (stream.name() == "ImageSequenceFormats") {
          stream.readNext();
          img_seq_formats = stream.text().toString();
        } else if (stream.name() == "RectifiedWaveforms") {
          stream.readNext();
          rectified_waveforms = (stream.text() == "1");
        } else if (stream.name() == "DefaultTransitionLength") {
          stream.readNext();
          default_transition_length = stream.text().toInt();
        } else if (stream.name() == "TimecodeView") {
          stream.readNext();
          timecode_view = stream.text().toInt();
        } else if (stream.name() == "ShowTitleSafeArea") {
          stream.readNext();
          show_title_safe_area = (stream.text() == "1");
        } else if (stream.name() == "UseCustomTitleSafeRatio") {
          stream.readNext();
          use_custom_title_safe_ratio = (stream.text() == "1");
        } else if (stream.name() == "CustomTitleSafeRatio") {
          stream.readNext();
          custom_title_safe_ratio = stream.text().toDouble();
        } else if (stream.name() == "EnableDragFilesToTimeline") {
          stream.readNext();
          enable_drag_files_to_timeline = (stream.text() == "1");;
        } else if (stream.name() == "AutoscaleByDefault") {
          stream.readNext();
          autoscale_by_default = (stream.text() == "1");
        } else if (stream.name() == "RecordingMode") {
          stream.readNext();
          recording_mode = stream.text().toInt();
        } else if (stream.name() == "EnableSeekToImport") {
          stream.readNext();
          enable_seek_to_import = (stream.text() == "1");
        } else if (stream.name() == "AudioScrubbing") {
          stream.readNext();
          enable_audio_scrubbing = (stream.text() == "1");
        } else if (stream.name() == "DropFileOnMediaToReplace") {
          stream.readNext();
          drop_on_media_to_replace = (stream.text() == "1");
        } else if (stream.name() == "Autoscroll") {
          stream.readNext();
          autoscroll = stream.text().toInt();
        } else if (stream.name() == "AudioRate") {
          stream.readNext();
          audio_rate = stream.text().toInt();
        } else if (stream.name() == "HoverFocus") {
          stream.readNext();
          hover_focus = (stream.text() == "1");
        } else if (stream.name() == "ProjectViewType") {
          stream.readNext();
          project_view_type = stream.text().toInt();
        } else if (stream.name() == "SetNameWithMarker") {
          stream.readNext();
          set_name_with_marker = (stream.text() == "1");
        } else if (stream.name() == "ShowProjectToolbar") {
          stream.readNext();
          show_project_toolbar = (stream.text() == "1");
        } else if (stream.name() == "PreviousFrameQueueSize") {
          stream.readNext();
          previous_queue_size = stream.text().toDouble();
        } else if (stream.name() == "PreviousFrameQueueType") {
          stream.readNext();
          previous_queue_type = stream.text().toInt();
        } else if (stream.name() == "UpcomingFrameQueueSize") {
          stream.readNext();
          upcoming_queue_size = stream.text().toDouble();
        } else if (stream.name() == "UpcomingFrameQueueType") {
          stream.readNext();
          upcoming_queue_type = stream.text().toInt();
        } else if (stream.name() == "Loop") {
          stream.readNext();
          loop = (stream.text() == "1");
        } else if (stream.name() == "SeekAlsoSelects") {
          stream.readNext();
          seek_also_selects = (stream.text() == "1");
        } else if (stream.name() == "AutoSeekToBeginning") {
          stream.readNext();
          auto_seek_to_beginning = (stream.text() == "1");
        } else if (stream.name() == "CSSPath") {
          stream.readNext();
          css_path = stream.text().toString();
        } else if (stream.name() == "EffectTextboxLines") {
          stream.readNext();
          effect_textbox_lines = stream.text().toInt();
        } else if (stream.name() == "UseSoftwareFallback") {
          stream.readNext();
          use_software_fallback = (stream.text() == "1");
        } else if (stream.name() == "CenterTimelineTimecodes") {
          stream.readNext();
          center_timeline_timecodes =  (stream.text() == "1");
        } else if (stream.name() == "PreferredAudioOutput") {
          stream.readNext();
          preferred_audio_output = stream.text().toString();
        } else if (stream.name() == "PreferredAudioInput") {
          stream.readNext();
          preferred_audio_input = stream.text().toString();
        } else if (stream.name() == "LanguageFile") {
          stream.readNext();
          language_file = stream.text().toString();
          int si = language_file.lastIndexOf('_');
          if(si != -1)
          {
            int ei = language_file.lastIndexOf('.');
            locale_str = language_file.mid(si+1, ei-si-1);

            qInfo() << "locale str is :" << locale_str;
          }
        } else if (stream.name() == "ThumbnailResolution") {
          stream.readNext();
          thumbnail_resolution = stream.text().toInt();
        } else if (stream.name() == "WaveformResolution") {
          stream.readNext();
          waveform_resolution = stream.text().toInt();
        } else if (stream.name() == "AddDefaultEffectsToClips") {
          stream.readNext();
          add_default_effects_to_clips = (stream.text() == "1");
        } else if (stream.name() == "Style") {
          stream.readNext();
          style = static_cast<olive::styling::Style>(stream.text().toInt());
        } else if (stream.name() == "NativeMenuStyling") {
          stream.readNext();
          use_native_menu_styling = (stream.text() == "1");
        } else if (stream.name() == "DefaultSequenceWidth") {
          stream.readNext();
          default_sequence_width = stream.text().toInt();
        } else if (stream.name() == "DefaultSequenceHeight") {
          stream.readNext();
          default_sequence_height = stream.text().toInt();
        } else if (stream.name() == "DefaultSequenceFrameRate") {
          stream.readNext();
          default_sequence_framerate = stream.text().toDouble();
        } else if (stream.name() == "DefaultSequenceAudioFrequency") {
          stream.readNext();
          default_sequence_audio_frequency = stream.text().toInt();
        } else if (stream.name() == "DefaultSequenceAudioLayout") {
          stream.readNext();
          default_sequence_audio_channel_layout = stream.text().toInt();
        } else if (stream.name() == "LockedPanels") {
          stream.readNext();
          locked_panels = (stream.text() == "1");
        }else if (stream.name() == "PreferedProjectPath") {
          stream.readNext();
          prefered_project_path = stream.text().toString();
        }
        else if (stream.name() == "CurrentNoticeName") {
          stream.readNext();
          current_notice_file= stream.text().toString();
        }
        else if (stream.name() == "DefaultFramesForImage") {
          stream.readNext();
          image_frames = stream.text().toInt();
        }
      }
    }
    if (stream.hasError()) {
      qCritical() << "Error parsing config XML." << stream.errorString();
    }

    f.close();
  }
  loadIni4SavedEffects();
}

void Config::save(QString path) {
  QFile f(path);
  if (!f.open(QIODevice::WriteOnly)) {
    qCritical() << "Could not save configuration";
    return;
  }

  QXmlStreamWriter stream(&f);
  stream.setAutoFormatting(true);
  stream.writeStartDocument(); // doc
  stream.writeStartElement("Configuration"); // configuration

  stream.writeTextElement("Version", QString::number(olive::kSaveVersion));
  stream.writeTextElement("ShowTrackLines", QString::number(show_track_lines));
  stream.writeTextElement("ScrollZooms", QString::number(scroll_zooms));
  stream.writeTextElement("InvertTimelineScrollAxes", QString::number(invert_timeline_scroll_axes));
  stream.writeTextElement("EditToolSelectsLinks", QString::number(edit_tool_selects_links));
  stream.writeTextElement("EditToolAlsoSeeks", QString::number(edit_tool_also_seeks));
  stream.writeTextElement("SelectAlsoSeeks", QString::number(select_also_seeks));
  stream.writeTextElement("PasteSeeks", QString::number(paste_seeks));
  stream.writeTextElement("ImageSequenceFormats", img_seq_formats);
  stream.writeTextElement("RectifiedWaveforms", QString::number(rectified_waveforms));
  stream.writeTextElement("DefaultTransitionLength", QString::number(default_transition_length));
  stream.writeTextElement("TimecodeView", QString::number(timecode_view));
  stream.writeTextElement("ShowTitleSafeArea", QString::number(show_title_safe_area));
  stream.writeTextElement("UseCustomTitleSafeRatio", QString::number(use_custom_title_safe_ratio));
  stream.writeTextElement("CustomTitleSafeRatio", QString::number(custom_title_safe_ratio));
  stream.writeTextElement("EnableDragFilesToTimeline", QString::number(enable_drag_files_to_timeline));
  stream.writeTextElement("AutoscaleByDefault", QString::number(autoscale_by_default));
  stream.writeTextElement("RecordingMode", QString::number(recording_mode));
  stream.writeTextElement("EnableSeekToImport", QString::number(enable_seek_to_import));
  stream.writeTextElement("AudioScrubbing", QString::number(enable_audio_scrubbing));
  stream.writeTextElement("DropFileOnMediaToReplace", QString::number(drop_on_media_to_replace));
  stream.writeTextElement("Autoscroll", QString::number(autoscroll));
  stream.writeTextElement("AudioRate", QString::number(audio_rate));
  stream.writeTextElement("HoverFocus", QString::number(hover_focus));
  stream.writeTextElement("ProjectViewType", QString::number(project_view_type));
  stream.writeTextElement("SetNameWithMarker", QString::number(set_name_with_marker));
  stream.writeTextElement("ShowProjectToolbar", QString::number(panel_project->IsToolbarVisible()));
  stream.writeTextElement("PreviousFrameQueueSize", QString::number(previous_queue_size));
  stream.writeTextElement("PreviousFrameQueueType", QString::number(previous_queue_type));
  stream.writeTextElement("UpcomingFrameQueueSize", QString::number(upcoming_queue_size));
  stream.writeTextElement("UpcomingFrameQueueType", QString::number(upcoming_queue_type));
  stream.writeTextElement("Loop", QString::number(loop));
  stream.writeTextElement("SeekAlsoSelects", QString::number(seek_also_selects));
  stream.writeTextElement("AutoSeekToBeginning", QString::number(auto_seek_to_beginning));
  stream.writeTextElement("CSSPath", css_path);
  stream.writeTextElement("EffectTextboxLines", QString::number(effect_textbox_lines));
  stream.writeTextElement("UseSoftwareFallback", QString::number(use_software_fallback));
  stream.writeTextElement("CenterTimelineTimecodes", QString::number(center_timeline_timecodes));
  stream.writeTextElement("PreferredAudioOutput", preferred_audio_output);
  stream.writeTextElement("PreferredAudioInput", preferred_audio_input);
  stream.writeTextElement("LanguageFile", language_file);
  stream.writeTextElement("ThumbnailResolution", QString::number(thumbnail_resolution));
  stream.writeTextElement("WaveformResolution", QString::number(waveform_resolution));
  stream.writeTextElement("AddDefaultEffectsToClips", QString::number(add_default_effects_to_clips));
  stream.writeTextElement("Style", QString::number(style));
  stream.writeTextElement("NativeMenuStyling", QString::number(use_native_menu_styling));
  stream.writeTextElement("DefaultSequenceWidth", QString::number(default_sequence_width));
  stream.writeTextElement("DefaultSequenceHeight", QString::number(default_sequence_height));
  stream.writeTextElement("DefaultSequenceFrameRate", QString::number(default_sequence_framerate));
  stream.writeTextElement("DefaultSequenceAudioFrequency", QString::number(default_sequence_audio_frequency));
  stream.writeTextElement("DefaultSequenceAudioLayout", QString::number(default_sequence_audio_channel_layout));
  stream.writeTextElement("LockedPanels", QString::number(locked_panels));
  stream.writeTextElement("PreferedProjectPath", prefered_project_path);
  stream.writeTextElement("CurrentNoticeName", current_notice_file);
  stream.writeTextElement("DefaultFramesForImage", QString::number(image_frames));

  stream.writeEndElement(); // configuration
  stream.writeEndDocument(); // doc
  f.close();
}
bool Config::addSavedEffects(const QString& e_name, const QString& se_name)
{
   int i=1;
   while(true)
   {
       QString key = QString("%1_%2").arg(e_name, QString::number(i));
       if(!savedEffectsMap.contains(key))
       {
          savedEffectsMap.insert(key, se_name);
          saveIni4SavedEffects();
          return true;
       }
       else
        {
          if(savedEffectsMap[key] == se_name)
          {
          
            break;
          }
        }
       i++;
   }
   return false;

}
QVector<QString> Config::getSavedEffects(const QString& e_name)
{
   int i=1;
   QVector<QString>  seffects;
   while(true)
   {
       QString key = QString("%1_%2").arg(e_name, QString::number(i));
       if(!savedEffectsMap.contains(key))
       {
          break;
       }
       qWarning() << key ;
       qWarning() << savedEffectsMap[key];
       i++;
       seffects.append(savedEffectsMap[key]);
   };
   return seffects;
}
void Config::loadIni4SavedEffects()
{
  QDir app_dir = QDir(get_app_path());
  QDir destDir(app_dir.filePath("saved_effects"));
  
  QString new_name = destDir.absoluteFilePath("savedeffects.map");

  QFile file(new_name);
  file.open(QIODevice::ReadOnly);
  QDataStream out(&file);   // we will serialize the data into the file
  out >> savedEffectsMap;

}
void Config::saveIni4SavedEffects()
{
  QDir app_dir = QDir(get_app_path());
  QDir destDir(app_dir.filePath("saved_effects"));
  
  QString new_name = destDir.absoluteFilePath("savedeffects.map");

  QFile file(new_name);

  file.open(QIODevice::WriteOnly);
  QDataStream out(&file);   // we will serialize the data into the file
  out << savedEffectsMap;

}
void Config::buildI18nMap()
{
  i18nMap.insert("Radius", QString(QObject::tr("Radius")));
  i18nMap.insert("Horizontal", QString(QObject::tr("Horizontal")));
  i18nMap.insert("Vertical", QString(QObject::tr("Vertical")));
  i18nMap.insert("Amount", QString(QObject::tr("Amount")));
  i18nMap.insert("Center", QString(QObject::tr("Center")));
  i18nMap.insert("Mode", QString(QObject::tr("Mode")));
  i18nMap.insert("Key Color", QString(QObject::tr("Key Color")));
  i18nMap.insert("Lower Tolerance", QString(QObject::tr("Lower Tolerance")));
  i18nMap.insert("Upper Tolerance", QString(QObject::tr("Upper Tolerance")));
  i18nMap.insert("Red Amount", QString(QObject::tr("Red Amount")));
  i18nMap.insert("Green Amount", QString(QObject::tr("Green Amount")));
  i18nMap.insert("Blue Amount", QString(QObject::tr("Blue Amount")));
  i18nMap.insert("Temperature", QString(QObject::tr("Temperature")));
  i18nMap.insert("Tint", QString(QObject::tr("Tint")));
  i18nMap.insert("Exposure", QString(QObject::tr("Exposure")));
  i18nMap.insert("Contrast", QString(QObject::tr("Contrast")));
  i18nMap.insert("Highlights", QString(QObject::tr("Highlights")));
  i18nMap.insert("Shadows", QString(QObject::tr("Shadows")));
  i18nMap.insert("Whites (Brightness)", QString(QObject::tr("Whites (Brightness)")));
  i18nMap.insert("Blacks (Darkness)", QString(QObject::tr("Blacks (Darkness)")));
  i18nMap.insert("Saturation", QString(QObject::tr("Saturation")));
  i18nMap.insert("Find by", QString(QObject::tr("Find by")));
  i18nMap.insert("Lower Limit", QString(QObject::tr("Lower Limit")));
  i18nMap.insert("Upper Limit", QString(QObject::tr("Upper Limit")));
  i18nMap.insert("Invert", QString(QObject::tr("Invert")));
  i18nMap.insert("Left", QString(QObject::tr("Left")));
  i18nMap.insert("Top", QString(QObject::tr("Top")));
  i18nMap.insert("Right", QString(QObject::tr("Right")));
  i18nMap.insert("Bottom", QString(QObject::tr("Bottom")));
  i18nMap.insert("Feather", QString(QObject::tr("Feather")));
  i18nMap.insert("Invert", QString(QObject::tr("Invert")));
  i18nMap.insert("Size", QString(QObject::tr("Size")));
  i18nMap.insert("Invert", QString(QObject::tr("Invert")));
  i18nMap.insert("Channel", QString(QObject::tr("Channel")));
  i18nMap.insert("Factor", QString(QObject::tr("Factor")));
  i18nMap.insert("Balance", QString(QObject::tr("Balance")));
  i18nMap.insert("Length", QString(QObject::tr("Length")));
  i18nMap.insert("Angle", QString(QObject::tr("Angle")));
  i18nMap.insert("Contrast", QString(QObject::tr("Contrast")));
  i18nMap.insert("Resolution", QString(QObject::tr("Resolution")));
  i18nMap.insert("HSL mode", QString(QObject::tr("HSL mode")));
  i18nMap.insert("Invert", QString(QObject::tr("Invert")));
  i18nMap.insert("Size", QString(QObject::tr("Size")));
  i18nMap.insert("Horizontal", QString(QObject::tr("Horizontal")));
  i18nMap.insert("Vertical", QString(QObject::tr("Vertical")));
  i18nMap.insert("Radius", QString(QObject::tr("Radius")));
  i18nMap.insert("Sigma", QString(QObject::tr("Sigma")));
  i18nMap.insert("Hue", QString(QObject::tr("Hue")));
  i18nMap.insert("Saturation", QString(QObject::tr("Saturation")));
  i18nMap.insert("Brightness", QString(QObject::tr("Brightness")));
 
  i18nMap.insert("Lower Limit", QString(QObject::tr("Lower Limit")));
  i18nMap.insert("Upper Limit", QString(QObject::tr("Upper Limit")));
  i18nMap.insert("Invert", QString(QObject::tr("Invert")));
  i18nMap.insert("Color Noise", QString(QObject::tr("Color Noise")));
  i18nMap.insert("Blend", QString(QObject::tr("Blend")));
  i18nMap.insert("Horizontal Pixels", QString(QObject::tr("Horizontal Pixels")));
  i18nMap.insert("Vertical Pixels", QString(QObject::tr("Vertical Pixels")));
  i18nMap.insert("Bypass", QString(QObject::tr("Bypass")));
  i18nMap.insert("Colors", QString(QObject::tr("Colors")));
  i18nMap.insert("Gamma", QString(QObject::tr("Gamma")));
  i18nMap.insert("Center", QString(QObject::tr("Center")));
  i18nMap.insert("Speed", QString(QObject::tr("Speed")));
  i18nMap.insert("Intensity", QString(QObject::tr("Intensity")));
  i18nMap.insert("Frequency", QString(QObject::tr("Frequency")));
  i18nMap.insert("Reverse", QString(QObject::tr("Reverse")));
  i18nMap.insert("Stretch", QString(QObject::tr("Stretch")));

  i18nMap.insert("Center", QString(QObject::tr("Center")));
  i18nMap.insert("Tile", QString(QObject::tr("Tile")));
  i18nMap.insert("Hide Edges", QString(QObject::tr("Hide Edges")));
  i18nMap.insert("Stretch", QString(QObject::tr("Stretch")));
  i18nMap.insert("Angle", QString(QObject::tr("Angle")));
  i18nMap.insert("Scale", QString(QObject::tr("Scale")));
  i18nMap.insert("Center", QString(QObject::tr("Center")));
  i18nMap.insert("Mirror X", QString(QObject::tr("Mirror X")));
  i18nMap.insert("Mirror Y", QString(QObject::tr("Mirror Y")));
  i18nMap.insert("Colors", QString(QObject::tr("Colors")));
  i18nMap.insert("Saturation Levels", QString(QObject::tr("Saturation Levels")));
  i18nMap.insert("Brightness Levels", QString(QObject::tr("Brightness Levels")));
  i18nMap.insert("Edge Tolerance", QString(QObject::tr("Edge Tolerance")));
  i18nMap.insert("Edge Strength", QString(QObject::tr("Edge Strength")));
  i18nMap.insert("Size", QString(QObject::tr("Size")));
  i18nMap.insert("Softness", QString(QObject::tr("Softness")));
  i18nMap.insert("Circular", QString(QObject::tr("Circular")));
  i18nMap.insert("Invert", QString(QObject::tr("Invert")));
  i18nMap.insert("Frequency", QString(QObject::tr("Frequency")));
  i18nMap.insert("Intensity", QString(QObject::tr("Intensity")));
  i18nMap.insert("Evolution", QString(QObject::tr("Evolution")));
  i18nMap.insert("Vertical", QString(QObject::tr("Vertical")));
  i18nMap.insert("Composite", QString(QObject::tr("Composite")));
  i18nMap.insert("Alpha", QString(QObject::tr("Alpha")));
  i18nMap.insert("Original", QString(QObject::tr("Original")));
  i18nMap.insert("Luminance", QString(QObject::tr("Luminance")));
  i18nMap.insert("Value", QString(QObject::tr("Value")));
  i18nMap.insert("Hue", QString(QObject::tr("Hue")));
  i18nMap.insert("Saturation", QString(QObject::tr("Saturation")));
  i18nMap.insert("Red", QString(QObject::tr("Red")));
  i18nMap.insert("Green", QString(QObject::tr("Green")));
  i18nMap.insert("Blue", QString(QObject::tr("Blue")));

   
   
}
QString Config::findI18nStr(const QString& enstr)
{
  if(i18nMap.contains(enstr))
       {
          return i18nMap[enstr];
       }
  return enstr;
}

RuntimeConfig::RuntimeConfig() :
  shaders_are_enabled(true),
  disable_blending(false)
{}
