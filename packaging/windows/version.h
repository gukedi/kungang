#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             1,20,5011
#define VER_FILEVERSION_STR         "1.20.5011\0"

#define VER_PRODUCTVERSION          1,20,5011
#define VER_PRODUCTVERSION_STR      "1.20.5011\0"

#define VER_COMPANYNAME_STR         "sumoon.com/kungang"
#define VER_FILEDESCRIPTION_STR     "KunGang"
#define VER_INTERNALNAME_STR        "KunGang"
#define VER_LEGALCOPYRIGHT_STR      "Copyright@2020 KunGang Team"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "KunGang.exe"
#define VER_PRODUCTNAME_STR         "KunGang"

#define VER_COMPANYDOMAIN_STR       "www.sumoon.com/kungang"

#endif // VERSION_H
