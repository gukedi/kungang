!include "MUI.nsh"

!define MUI_ICON "kgang.ico"
!define MUI_UNICON "uninstall icon.ico"

!define APP_NAME "MYAPP_EN"
!define APP_TARGET "kungang"

!define MUI_FINISHPAGE_RUN "$INSTDIR\kungang.exe"

SetCompressor lzma

Name ${MYAPP_EN}


!ifdef X64
InstallDir "$PROGRAMFILES64\${APP_TARGET}"
!else
InstallDir "$PROGRAMFILES32\${APP_TARGET}"
!endif

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE LICENSE
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_FINISHPAGE_RUN_TEXT "Run ${MYAPP_EN}"
!define MUI_FINISHPAGE_RUN_FUNCTION "LaunchKungang"
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "SimpChinese"

Section "KunGang (required)"
	
	SectionIn RO

	SetOutPath $INSTDIR

	File /r kgang\*

	WriteUninstaller "$INSTDIR\uninstall.exe"

SectionEnd

Section "Create Desktop shortcut"
	CreateShortCut "$DESKTOP\${MYAPP_EN}.lnk" "$INSTDIR\${APP_TARGET}.exe"
SectionEnd

Section "Create Start Menu shortcut"
	CreateDirectory "$SMPROGRAMS\${MYAPP_EN}"
	CreateShortCut "$SMPROGRAMS\${MYAPP_EN}\${MYAPP_EN}.lnk" "$INSTDIR\${APP_TARGET}.exe"
	CreateShortCut "$SMPROGRAMS\${MYAPP_EN}\Uninstall ${MYAPP_EN}.lnk" "$INSTDIR\uninstall.exe"
SectionEnd

Section "Associate *.kgp files with KunGang"
	WriteRegStr HKCR ".kgp" "" "KunGang.KGPFile"
	WriteRegStr HKCR ".kgp" "Content Type" "application/vnd.kungang-project"
	WriteRegStr HKCR "KunGang.KGPFile" "" "KunGang project file"
	WriteRegStr HKCR "KunGang.KGPFile\DefaultIcon" "" "$INSTDIR\kungang.exe,1"
	WriteRegStr HKCR "KunGang.KGPFile\shell\open\command" "" "$\"$INSTDIR\kungang.exe$\" $\"%1$\""
	System::Call 'shell32.dll::SHChangeNotify(i, i, i, i) v (0x08000000, 0, 0, 0)'
SectionEnd

UninstPage uninstConfirm
UninstPage instfiles

Section "uninstall"

	rmdir /r "$INSTDIR"

	Delete "$DESKTOP\${MYAPP_EN}.lnk"
	rmdir /r "$SMPROGRAMS\${MYAPP_EN}"
	
	DeleteRegKey HKCR ".kgp"
	DeleteRegKey HKCR "KunGang.KGPFile"
	DeleteRegKey HKCR "KunGang.KGPFile\DefaultIcon" ""
	DeleteRegKey HKCR "KunGang.KGPFile\shell\open\command" ""
	System::Call 'shell32.dll::SHChangeNotify(i, i, i, i) v (0x08000000, 0, 0, 0)'
SectionEnd

Function LaunchKungang
	ExecShell "" "$INSTDIR\${APP_TARGET}.exe"
FunctionEnd