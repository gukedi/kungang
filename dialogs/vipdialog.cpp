/***

    Kungang - Non-Linear Video Editor
    Copyright (C) 2021 Kungang Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

***/
#include "vipdialog.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QDialogButtonBox>
#include <QDesktopServices>
#include <QUrl>
#include "global/global.h"

VipDialog::VipDialog(QWidget *parent) :
  QDialog(parent)
{
  setWindowTitle(tr("About KunGang"));

  QVBoxLayout* layout = new QVBoxLayout(this);
  layout->setSpacing(20);

  // Construct About text
  //connect(link,SIGNAL(linkActivated(QString)), this,SLOT(openUrl(QString)));
  QLabel* label =
      new QLabel(QString("<html><head/><body>"
                         "<p><b>%1</b></p>" // AppName (version identifier)
                         "<p>%2</p>" // First statement
                         "<p>%3</p>" // Second statement
                         "</body></html>").arg(olive::AppName+" by JunMing(javawolf@163.com)",
                                               tr("KunGang is a simple video editor. This software is free and "
                                                  "protected by the GNU GPL. Its was originally developed in Feb 2020, based on Olive(https://www.olivevideoeditor.org)"),
                                               tr("KunGang Team is obliged to inform users that KunGang source code is "
                                                  "available for download from its website.")), this);

  QLabel* linkicon = new QLabel("<p><a href=\"http://www.sumoon.com/kungang\"><img src=\":/icons/kgang-splash.png\"/></a></p>");
  connect(linkicon,SIGNAL(linkActivated(QString)), this,SLOT(openUrl(QString)));
  layout->addWidget(linkicon, 0,Qt::AlignHCenter);
  QLabel* iconAttr = new QLabel("Icons made by <a href=\"https://www.flaticon.com/authors/freepik\" title=\"Freepik\">Freepik</a> from <a href=\"https://www.flaticon.com/\" title=\"Flaticon\"> www.flaticon.com</a>");
  layout->addWidget(iconAttr, 0, Qt::AlignHCenter);
  connect(iconAttr,SIGNAL(linkActivated(QString)), this,SLOT(openUrl(QString)));

  QLabel* link = new QLabel("<a href=\"http://www.sumoon.com/kungang\">访问主页</a>.<a href=\"https://weibo.com/kgve\">微博</a>",this);
  // Set text formatting
  connect(link,SIGNAL(linkActivated(QString)), this,SLOT(openUrl(QString)));

  label->setAlignment(Qt::AlignCenter);
  label->setTextInteractionFlags(Qt::TextSelectableByMouse);
  //label->setCursor(Qt::IBeamCursor);
  label->setWordWrap(true);
  layout->addWidget(label);
  layout->addWidget(link, 0,Qt::AlignHCenter);
  QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok, this);
  buttons->setCenterButtons(true);
  layout->addWidget(buttons);

  this->setFixedWidth(400);

  connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
}
void VipDialog::openUrl(const QString& url)
{
  QDesktopServices::openUrl(QUrl(url));
}
