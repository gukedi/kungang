> 昆冈停止维护。。因为已经有更好的免费视频编辑软件可选。-- [剪映](https://lv.ulikecam.com/)  （2022/06/11)
# 昆冈-免费视频编辑

昆冈 是一款免费开源的视频编辑软件，基于[Olive](http://olivevideoeditor.org)开发。

![screen](https://gitee.com/gukedi/kungang/raw/master/snap/snapshot1.jpg)

[昆冈官方网站](http://www.sumoon.com/kungang/) 
[昆冈特效](http://www.sumoon.com/kgeffects) 

## 下载昆冈
windows版本[下载](http://www.sumoon.com/kungang/#download)**建议安装到非系统盘，否则你可能会遇到文件写入错误。**

Linux版本（ubuntu下测试通过）[下载](https://gitee.com/gukedi/kungang/blob/master/packaging/linux.deb)

## 使用昆冈
[昆冈使用指南]（http://www.sumoon.com/kungang/kghelp.html)
## 编译昆冈

1. 下载msys2
2. 安装依赖 QT ffmpeg
3. make 
4. 详细请参照build.md

## 已知问题
1. QLV(腾讯视频保存格式）不支持
2. Intel HD Graphic 1000/2000/3000 不支持(因为OPENGL版本太低！？）
3. 大视频导出，有时会退出（尤其是内存不是很富足的情况)
4. 其他问题等你来发现。

## 已完成改进计划
1. +启动项目导航
2. *特效面板优化
3. *项目面板优化
4. *片段面板优化
5. *导出对话框优化
6. +特效下载

## 下一步改进计划
1. 增强audio 特效
2. 扩展video 特效 --- 框架完成：[昆冈特效](http://www.sumoon.com/kungang/kgeffects)
3. 解决无响应/异常退出问题
4. UI 优化
   1. 增加toolbar
   2. 全屏预览
   3. 其他细节

## 引用
1. [Olive](https://github.com/olive-editor/olive)
2. [QuaZip](https://github.com/stachenov/quazip)