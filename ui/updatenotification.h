#ifndef UPDATENOTIFICATION_H
#define UPDATENOTIFICATION_H

#include <QNetworkReply>

class UpdateNotification : public QObject {
  Q_OBJECT
public:
  UpdateNotification();
  void check();
  
private slots:
  void finished_slot(QNetworkReply*);
  void finished_notice_slot(QNetworkReply*);
  void downloadNotice();
private:
  QString getNodeContent(const QString& response, const QString& snode, const QString& enode);
  //void downloadNotice(QString noticeUrl);
  void getNoticeNameFromUrl(const QString& noticeUrl);
  QString noticeName;
  QString noticeUrl;
  int noticeSize;
  
};

namespace olive {
  extern UpdateNotification update_notifier;
}

#endif // UPDATENOTIFICATION_H
